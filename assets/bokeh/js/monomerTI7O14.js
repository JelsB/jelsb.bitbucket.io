(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("b828e37b-3f35-476e-b3ca-dcb28bbb205d");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'b828e37b-3f35-476e-b3ca-dcb28bbb205d' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"ac4fbd06-144e-4de4-a768-c4337cae64a2":{"roots":{"references":[{"attributes":{"formatter":{"id":"c2b9ccff-7fd4-4ea2-92d4-d6ca0a8b53f8","type":"BasicTickFormatter"},"plot":{"id":"c182610d-8387-4e2b-bf3c-5b85e893521b","subtype":"Figure","type":"Plot"},"ticker":{"id":"fc11f267-fac3-440a-a24e-fe5a9ed97880","type":"BasicTicker"},"visible":false},"id":"0f1f2c82-59ac-4684-80e2-7e49fef5693c","type":"LinearAxis"},{"attributes":{"plot":{"id":"c182610d-8387-4e2b-bf3c-5b85e893521b","subtype":"Figure","type":"Plot"},"ticker":{"id":"44e6274a-f554-4ac2-a5e2-263676303c9b","type":"BasicTicker"},"visible":false},"id":"12630b81-582e-41b5-83a0-836ae5d6db98","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8fdf0bee-c8f6-4716-928c-7da257964eab","type":"ImageURL"},{"attributes":{"plot":null,"text":""},"id":"19d0ef08-a18a-4923-895d-105d48e61134","type":"Title"},{"attributes":{"source":{"id":"c5999a3f-329b-49d8-828c-ea3a69ad91b5","type":"ColumnDataSource"}},"id":"8fd1f476-9916-4f26-b821-263ac881ac16","type":"CDSView"},{"attributes":{},"id":"cf8e18e9-6e9d-48db-b498-8a4ce489a1fe","type":"Selection"},{"attributes":{"below":[{"id":"19112149-4eaa-4176-99b5-b95560dce203","type":"LinearAxis"}],"left":[{"id":"0f1f2c82-59ac-4684-80e2-7e49fef5693c","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"19112149-4eaa-4176-99b5-b95560dce203","type":"LinearAxis"},{"id":"12630b81-582e-41b5-83a0-836ae5d6db98","type":"Grid"},{"id":"0f1f2c82-59ac-4684-80e2-7e49fef5693c","type":"LinearAxis"},{"id":"65e57c00-d2ba-474e-9ad6-a4af9b669423","type":"Grid"},{"id":"65f51da3-6250-439e-9b3c-8ca6a49464dd","type":"GlyphRenderer"}],"title":{"id":"19d0ef08-a18a-4923-895d-105d48e61134","type":"Title"},"toolbar":{"id":"236a27ae-5f77-4e7a-b976-2484006b08ee","type":"Toolbar"},"x_range":{"id":"e9c695ef-2e7b-4e1a-99a9-f7fdc2f21e12","type":"Range1d"},"x_scale":{"id":"6a3e1a66-2fd7-4366-9966-3fbff5cbada3","type":"LinearScale"},"y_range":{"id":"cc5789bd-e6e5-4fab-8213-b146176a3bd9","type":"Range1d"},"y_scale":{"id":"6d98a80c-1965-4716-bf9c-5bfdceb376b9","type":"LinearScale"}},"id":"c182610d-8387-4e2b-bf3c-5b85e893521b","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c1127180-3ccb-4208-bdd5-8604df042385","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"c5999a3f-329b-49d8-828c-ea3a69ad91b5","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"1c592174-b454-40ad-8d36-5b2814c4346d","type":"CustomJS"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI7O14_000.png"],"url_num":["000"]},"selected":{"id":"cf8e18e9-6e9d-48db-b498-8a4ce489a1fe","type":"Selection"},"selection_policy":{"id":"f3f1b806-2ca9-474e-a11d-da0d5f187df6","type":"UnionRenderers"}},"id":"c5999a3f-329b-49d8-828c-ea3a69ad91b5","type":"ColumnDataSource"},{"attributes":{},"id":"fc11f267-fac3-440a-a24e-fe5a9ed97880","type":"BasicTicker"},{"attributes":{"formatter":{"id":"74bb3f45-50a3-4683-a406-dbbd2747fa8b","type":"BasicTickFormatter"},"plot":{"id":"c182610d-8387-4e2b-bf3c-5b85e893521b","subtype":"Figure","type":"Plot"},"ticker":{"id":"44e6274a-f554-4ac2-a5e2-263676303c9b","type":"BasicTicker"},"visible":false},"id":"19112149-4eaa-4176-99b5-b95560dce203","type":"LinearAxis"},{"attributes":{},"id":"c2b9ccff-7fd4-4ea2-92d4-d6ca0a8b53f8","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"e9c695ef-2e7b-4e1a-99a9-f7fdc2f21e12","type":"Range1d"},{"attributes":{"data_source":{"id":"c5999a3f-329b-49d8-828c-ea3a69ad91b5","type":"ColumnDataSource"},"glyph":{"id":"8fdf0bee-c8f6-4716-928c-7da257964eab","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c1127180-3ccb-4208-bdd5-8604df042385","type":"ImageURL"},"selection_glyph":null,"view":{"id":"8fd1f476-9916-4f26-b821-263ac881ac16","type":"CDSView"}},"id":"65f51da3-6250-439e-9b3c-8ca6a49464dd","type":"GlyphRenderer"},{"attributes":{"children":[{"id":"bb3874a2-c679-4ffe-8247-edfd5fb98bbe","type":"WidgetBox"},{"id":"c182610d-8387-4e2b-bf3c-5b85e893521b","subtype":"Figure","type":"Plot"}]},"id":"158cbbf2-829a-4b19-afbf-f3f15bc0505d","type":"Column"},{"attributes":{"children":[{"id":"a7b32a5e-7577-40bf-b68a-4297c02f0ca0","type":"Slider"}]},"id":"bb3874a2-c679-4ffe-8247-edfd5fb98bbe","type":"WidgetBox"},{"attributes":{},"id":"f3f1b806-2ca9-474e-a11d-da0d5f187df6","type":"UnionRenderers"},{"attributes":{"callback":{"id":"1c592174-b454-40ad-8d36-5b2814c4346d","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"a7b32a5e-7577-40bf-b68a-4297c02f0ca0","type":"Slider"},{"attributes":{},"id":"74bb3f45-50a3-4683-a406-dbbd2747fa8b","type":"BasicTickFormatter"},{"attributes":{},"id":"6a3e1a66-2fd7-4366-9966-3fbff5cbada3","type":"LinearScale"},{"attributes":{"callback":null},"id":"cc5789bd-e6e5-4fab-8213-b146176a3bd9","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"c182610d-8387-4e2b-bf3c-5b85e893521b","subtype":"Figure","type":"Plot"},"ticker":{"id":"fc11f267-fac3-440a-a24e-fe5a9ed97880","type":"BasicTicker"},"visible":false},"id":"65e57c00-d2ba-474e-9ad6-a4af9b669423","type":"Grid"},{"attributes":{},"id":"6d98a80c-1965-4716-bf9c-5bfdceb376b9","type":"LinearScale"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"236a27ae-5f77-4e7a-b976-2484006b08ee","type":"Toolbar"},{"attributes":{},"id":"44e6274a-f554-4ac2-a5e2-263676303c9b","type":"BasicTicker"}],"root_ids":["158cbbf2-829a-4b19-afbf-f3f15bc0505d"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"ac4fbd06-144e-4de4-a768-c4337cae64a2","roots":{"158cbbf2-829a-4b19-afbf-f3f15bc0505d":"b828e37b-3f35-476e-b3ca-dcb28bbb205d"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();