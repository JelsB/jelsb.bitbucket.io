(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("af4b8de5-da50-4d58-a05c-c9c17f118aae");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'af4b8de5-da50-4d58-a05c-c9c17f118aae' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"b6dbcdaf-5031-4c00-a2e1-97676154e162":{"roots":{"references":[{"attributes":{"callback":null},"id":"0546a84e-0cb8-4bdf-9d47-87dc7977bf80","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"143c8db7-b05b-4e4e-b0b3-01bb91d932b9","subtype":"Figure","type":"Plot"},"ticker":{"id":"92dea44c-5e8f-4102-a09e-7b91c294de5e","type":"BasicTicker"},"visible":false},"id":"a563d89a-d27c-453e-8e01-1a9ffc43d057","type":"Grid"},{"attributes":{},"id":"11eea022-0c5a-4c29-8301-daf00f0699a6","type":"LinearScale"},{"attributes":{"callback":null},"id":"bb030ca2-6aca-4368-8fdb-7efe968ca425","type":"Range1d"},{"attributes":{"children":[{"id":"99d6e91c-2a9a-46bd-bf00-88be0f268cd0","type":"WidgetBox"},{"id":"143c8db7-b05b-4e4e-b0b3-01bb91d932b9","subtype":"Figure","type":"Plot"}]},"id":"bf8b1ce5-a0bc-4208-8d78-014da9248ba2","type":"Column"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"44479b21-0890-46b2-965a-90e3e0fe00f6","type":"ImageURL"},{"attributes":{"data_source":{"id":"a302c9ad-f6c7-4e29-9a34-85c88ad1e4e1","type":"ColumnDataSource"},"glyph":{"id":"44479b21-0890-46b2-965a-90e3e0fe00f6","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"50701d7d-a509-44ac-96f2-c83a536080a2","type":"ImageURL"},"selection_glyph":null,"view":{"id":"8f88bda1-5db1-4bd7-aa61-ab5f19ebce04","type":"CDSView"}},"id":"29485935-900b-400d-911f-0e282e5ed30f","type":"GlyphRenderer"},{"attributes":{},"id":"fbb21cad-05fb-4785-9560-e5bbd318bb7d","type":"BasicTickFormatter"},{"attributes":{},"id":"48089441-63f0-48e9-bc6e-ce1848e90df1","type":"LinearScale"},{"attributes":{"plot":null,"text":""},"id":"ec042383-4ebf-4e6c-88ea-5b8738199524","type":"Title"},{"attributes":{"plot":{"id":"143c8db7-b05b-4e4e-b0b3-01bb91d932b9","subtype":"Figure","type":"Plot"},"ticker":{"id":"bc2a971a-f6a4-4080-abc4-0a70e2e2520c","type":"BasicTicker"},"visible":false},"id":"27d875bd-96bb-485c-8adf-972666240e99","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG2O2_000.png"],"url_num":["000"]},"selected":{"id":"5272b60c-a10e-4adf-81c5-fb6e018fd1ac","type":"Selection"},"selection_policy":{"id":"56483d50-1396-4f1f-ac46-ff8ec3146323","type":"UnionRenderers"}},"id":"a302c9ad-f6c7-4e29-9a34-85c88ad1e4e1","type":"ColumnDataSource"},{"attributes":{"formatter":{"id":"fbdb9773-4352-43e4-8979-b57451c92039","type":"BasicTickFormatter"},"plot":{"id":"143c8db7-b05b-4e4e-b0b3-01bb91d932b9","subtype":"Figure","type":"Plot"},"ticker":{"id":"92dea44c-5e8f-4102-a09e-7b91c294de5e","type":"BasicTicker"},"visible":false},"id":"89ed8832-f00d-4b24-971e-5be962d28da5","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"0ca5d7b1-0f79-42b2-b4c2-44d1b18f5b42","type":"Toolbar"},{"attributes":{},"id":"fbdb9773-4352-43e4-8979-b57451c92039","type":"BasicTickFormatter"},{"attributes":{"args":{"source":{"id":"a302c9ad-f6c7-4e29-9a34-85c88ad1e4e1","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"6a547137-7225-4102-a6e2-922473b95e45","type":"CustomJS"},{"attributes":{},"id":"92dea44c-5e8f-4102-a09e-7b91c294de5e","type":"BasicTicker"},{"attributes":{"children":[{"id":"9788dd9d-8432-44a9-bbc4-2a3768cb7a47","type":"Slider"}]},"id":"99d6e91c-2a9a-46bd-bf00-88be0f268cd0","type":"WidgetBox"},{"attributes":{"source":{"id":"a302c9ad-f6c7-4e29-9a34-85c88ad1e4e1","type":"ColumnDataSource"}},"id":"8f88bda1-5db1-4bd7-aa61-ab5f19ebce04","type":"CDSView"},{"attributes":{},"id":"5272b60c-a10e-4adf-81c5-fb6e018fd1ac","type":"Selection"},{"attributes":{"callback":{"id":"6a547137-7225-4102-a6e2-922473b95e45","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"9788dd9d-8432-44a9-bbc4-2a3768cb7a47","type":"Slider"},{"attributes":{},"id":"bc2a971a-f6a4-4080-abc4-0a70e2e2520c","type":"BasicTicker"},{"attributes":{"below":[{"id":"773feebd-9161-47bb-ba54-2def8ed22202","type":"LinearAxis"}],"left":[{"id":"89ed8832-f00d-4b24-971e-5be962d28da5","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"773feebd-9161-47bb-ba54-2def8ed22202","type":"LinearAxis"},{"id":"27d875bd-96bb-485c-8adf-972666240e99","type":"Grid"},{"id":"89ed8832-f00d-4b24-971e-5be962d28da5","type":"LinearAxis"},{"id":"a563d89a-d27c-453e-8e01-1a9ffc43d057","type":"Grid"},{"id":"29485935-900b-400d-911f-0e282e5ed30f","type":"GlyphRenderer"}],"title":{"id":"ec042383-4ebf-4e6c-88ea-5b8738199524","type":"Title"},"toolbar":{"id":"0ca5d7b1-0f79-42b2-b4c2-44d1b18f5b42","type":"Toolbar"},"x_range":{"id":"0546a84e-0cb8-4bdf-9d47-87dc7977bf80","type":"Range1d"},"x_scale":{"id":"11eea022-0c5a-4c29-8301-daf00f0699a6","type":"LinearScale"},"y_range":{"id":"bb030ca2-6aca-4368-8fdb-7efe968ca425","type":"Range1d"},"y_scale":{"id":"48089441-63f0-48e9-bc6e-ce1848e90df1","type":"LinearScale"}},"id":"143c8db7-b05b-4e4e-b0b3-01bb91d932b9","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"fbb21cad-05fb-4785-9560-e5bbd318bb7d","type":"BasicTickFormatter"},"plot":{"id":"143c8db7-b05b-4e4e-b0b3-01bb91d932b9","subtype":"Figure","type":"Plot"},"ticker":{"id":"bc2a971a-f6a4-4080-abc4-0a70e2e2520c","type":"BasicTicker"},"visible":false},"id":"773feebd-9161-47bb-ba54-2def8ed22202","type":"LinearAxis"},{"attributes":{},"id":"56483d50-1396-4f1f-ac46-ff8ec3146323","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"50701d7d-a509-44ac-96f2-c83a536080a2","type":"ImageURL"}],"root_ids":["bf8b1ce5-a0bc-4208-8d78-014da9248ba2"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"b6dbcdaf-5031-4c00-a2e1-97676154e162","roots":{"bf8b1ce5-a0bc-4208-8d78-014da9248ba2":"af4b8de5-da50-4d58-a05c-c9c17f118aae"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();