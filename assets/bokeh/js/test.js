(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("139ef998-a97e-4c1a-bd62-004a12fd17d8");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '139ef998-a97e-4c1a-bd62-004a12fd17d8' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"2788f190-4585-41f8-9939-10b9431de080":{"roots":{"references":[{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"1f623136-7629-49fd-9b24-bd322a0eb55f","type":"Toolbar"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"d9b86136-628a-4beb-a896-8e9752b6a591","type":"ImageURL"},{"attributes":{"source":{"id":"44071b91-3bfd-402c-b98f-ec81079fc171","type":"ColumnDataSource"}},"id":"d6842567-f4e0-4bec-ab58-b7da6a40611d","type":"CDSView"},{"attributes":{"children":[{"id":"ea32db26-cb5f-411a-83b5-87770e1aeef0","type":"Slider"}]},"id":"4c4bd62d-ff21-43c5-93d2-f5fa1dc5297b","type":"WidgetBox"},{"attributes":{"args":{"source":{"id":"44071b91-3bfd-402c-b98f-ec81079fc171","type":"ColumnDataSource"}},"code":"\\n    var data = source.data;\\n    var f = cb_obj.value\\n    var file_name = data[&#x27;url&#x27;]\\n    var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n    file_name[0] = file_name[0].replace(file_name[1], f_string)\\n    file_name[1] = f_string\\n    source.change.emit();\\n"},"id":"b01e00a0-8b2a-4428-8781-b51b8af0f34f","type":"CustomJS"},{"attributes":{"callback":null,"data":{"url":["/assets/bokeh/figs/TI2O4_000.png","000"]},"selected":{"id":"572dec94-0db7-4237-b72e-f68d03fff36b","type":"Selection"},"selection_policy":{"id":"6143d884-e614-4a67-862c-016e12203b21","type":"UnionRenderers"}},"id":"44071b91-3bfd-402c-b98f-ec81079fc171","type":"ColumnDataSource"},{"attributes":{"callback":{"id":"b01e00a0-8b2a-4428-8781-b51b8af0f34f","type":"CustomJS"},"end":73,"start":0,"title":"image number","value":1},"id":"ea32db26-cb5f-411a-83b5-87770e1aeef0","type":"Slider"},{"attributes":{"below":[{"id":"90608c5d-8ad7-44f5-9762-ca20593311b5","type":"LinearAxis"}],"left":[{"id":"b8091c30-6ebb-496f-b310-242e19c5a29b","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"90608c5d-8ad7-44f5-9762-ca20593311b5","type":"LinearAxis"},{"id":"501bf115-859f-41ea-9024-24db20810573","type":"Grid"},{"id":"b8091c30-6ebb-496f-b310-242e19c5a29b","type":"LinearAxis"},{"id":"50e94bce-46f6-4552-b054-482af59acf76","type":"Grid"},{"id":"940f90ff-a898-4203-9212-cd8610f54595","type":"GlyphRenderer"}],"title":{"id":"88bd9387-5acd-43ec-a100-e48cc75dbf74","type":"Title"},"toolbar":{"id":"1f623136-7629-49fd-9b24-bd322a0eb55f","type":"Toolbar"},"x_range":{"id":"ad35c622-5ac8-480e-bd55-5785b70a6f8d","type":"Range1d"},"x_scale":{"id":"16d8188b-07b9-40ea-9270-9ee9db50a4cd","type":"LinearScale"},"y_range":{"id":"4948ecc6-040f-453b-b198-83078bb54d5a","type":"Range1d"},"y_scale":{"id":"2db7dd9c-a906-4798-89ce-39f714dc61f7","type":"LinearScale"}},"id":"b68ce35d-d83d-4cb4-becc-0f5fa2644919","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"2db7dd9c-a906-4798-89ce-39f714dc61f7","type":"LinearScale"},{"attributes":{"formatter":{"id":"e435184f-1dbb-4583-b752-6852112b8f51","type":"BasicTickFormatter"},"plot":{"id":"b68ce35d-d83d-4cb4-becc-0f5fa2644919","subtype":"Figure","type":"Plot"},"ticker":{"id":"8a831257-5b81-4050-8566-2cddb118bbef","type":"BasicTicker"},"visible":false},"id":"90608c5d-8ad7-44f5-9762-ca20593311b5","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"88bd9387-5acd-43ec-a100-e48cc75dbf74","type":"Title"},{"attributes":{},"id":"8a831257-5b81-4050-8566-2cddb118bbef","type":"BasicTicker"},{"attributes":{"callback":null},"id":"ad35c622-5ac8-480e-bd55-5785b70a6f8d","type":"Range1d"},{"attributes":{"data_source":{"id":"44071b91-3bfd-402c-b98f-ec81079fc171","type":"ColumnDataSource"},"glyph":{"id":"d9b86136-628a-4beb-a896-8e9752b6a591","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"bbf63c41-a0c9-41ec-8302-a5b7b74c535c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"d6842567-f4e0-4bec-ab58-b7da6a40611d","type":"CDSView"}},"id":"940f90ff-a898-4203-9212-cd8610f54595","type":"GlyphRenderer"},{"attributes":{},"id":"e435184f-1dbb-4583-b752-6852112b8f51","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"b68ce35d-d83d-4cb4-becc-0f5fa2644919","subtype":"Figure","type":"Plot"},"ticker":{"id":"8a831257-5b81-4050-8566-2cddb118bbef","type":"BasicTicker"},"visible":false},"id":"501bf115-859f-41ea-9024-24db20810573","type":"Grid"},{"attributes":{},"id":"d3a445af-db69-4ca5-a2e6-601f682abdbe","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"d3a445af-db69-4ca5-a2e6-601f682abdbe","type":"BasicTickFormatter"},"plot":{"id":"b68ce35d-d83d-4cb4-becc-0f5fa2644919","subtype":"Figure","type":"Plot"},"ticker":{"id":"83cb6811-681a-4203-a417-541a700a4d6b","type":"BasicTicker"},"visible":false},"id":"b8091c30-6ebb-496f-b310-242e19c5a29b","type":"LinearAxis"},{"attributes":{},"id":"572dec94-0db7-4237-b72e-f68d03fff36b","type":"Selection"},{"attributes":{"callback":null},"id":"4948ecc6-040f-453b-b198-83078bb54d5a","type":"Range1d"},{"attributes":{},"id":"83cb6811-681a-4203-a417-541a700a4d6b","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"b68ce35d-d83d-4cb4-becc-0f5fa2644919","subtype":"Figure","type":"Plot"},"ticker":{"id":"83cb6811-681a-4203-a417-541a700a4d6b","type":"BasicTicker"},"visible":false},"id":"50e94bce-46f6-4552-b054-482af59acf76","type":"Grid"},{"attributes":{},"id":"6143d884-e614-4a67-862c-016e12203b21","type":"UnionRenderers"},{"attributes":{"children":[{"id":"4c4bd62d-ff21-43c5-93d2-f5fa1dc5297b","type":"WidgetBox"},{"id":"b68ce35d-d83d-4cb4-becc-0f5fa2644919","subtype":"Figure","type":"Plot"}]},"id":"296669ad-fb55-4457-b84f-a563989cff04","type":"Column"},{"attributes":{},"id":"16d8188b-07b9-40ea-9270-9ee9db50a4cd","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"bbf63c41-a0c9-41ec-8302-a5b7b74c535c","type":"ImageURL"}],"root_ids":["296669ad-fb55-4457-b84f-a563989cff04"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"2788f190-4585-41f8-9939-10b9431de080","roots":{"296669ad-fb55-4457-b84f-a563989cff04":"139ef998-a97e-4c1a-bd62-004a12fd17d8"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();