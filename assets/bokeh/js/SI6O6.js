(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("0fa34715-d08a-4df3-aa3d-37e24c104139");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '0fa34715-d08a-4df3-aa3d-37e24c104139' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"9108c4ce-a73c-4239-8071-0e35a63594bd":{"roots":{"references":[{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"b66ee633-7a2e-4222-8d56-78c8fb82d541","type":"ImageURL"},{"attributes":{},"id":"a727d6d0-e2c6-405f-98c2-97248eb6b172","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"8267834d-05de-40be-8d8a-4d2615cea3fa","type":"Range1d"},{"attributes":{"callback":{"id":"824aab07-4489-4cbb-82fe-70cf265fb17a","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"73628f7d-e10a-41aa-96de-717af6fba207","type":"Slider"},{"attributes":{"callback":null},"id":"173dc024-1609-49d0-b895-fc05064e54f6","type":"Range1d"},{"attributes":{},"id":"b1ad3f31-ed7f-4f8f-9ab5-124d33eb8dca","type":"BasicTickFormatter"},{"attributes":{"plot":null,"text":""},"id":"5fe9d8cb-b293-4795-8215-4bacb7eec40f","type":"Title"},{"attributes":{},"id":"401474b7-c9ce-497e-8db4-f0bac5870c17","type":"UnionRenderers"},{"attributes":{"children":[{"id":"73628f7d-e10a-41aa-96de-717af6fba207","type":"Slider"}]},"id":"f407306c-b1dd-4475-a25c-d831742ca681","type":"WidgetBox"},{"attributes":{},"id":"382b45f3-413b-477a-b7dd-a9a3462c2573","type":"LinearScale"},{"attributes":{},"id":"4d4ffcad-6aea-4ecc-9a0b-129a3ed8b548","type":"LinearScale"},{"attributes":{},"id":"dc53896c-9b92-424c-9b56-1cbd85a93ca8","type":"BasicTicker"},{"attributes":{"formatter":{"id":"a727d6d0-e2c6-405f-98c2-97248eb6b172","type":"BasicTickFormatter"},"plot":{"id":"6e0599e7-3197-44b5-bde6-7c0f11755569","subtype":"Figure","type":"Plot"},"ticker":{"id":"dc53896c-9b92-424c-9b56-1cbd85a93ca8","type":"BasicTicker"},"visible":false},"id":"d0363c89-c927-4bdf-9895-b7eff4eb5571","type":"LinearAxis"},{"attributes":{"plot":{"id":"6e0599e7-3197-44b5-bde6-7c0f11755569","subtype":"Figure","type":"Plot"},"ticker":{"id":"ef1f08cf-fe68-4ed5-9ff6-bf2f67b5c337","type":"BasicTicker"},"visible":false},"id":"a3b76f34-1485-4ac9-8da6-12fdd8041e53","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"b8e3046f-1435-432e-8a5d-3231cdfd3d3f","type":"ImageURL"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI6O6_000.png"],"url_num":["000"]},"selected":{"id":"55f11763-de03-4f85-a0d9-a7d2efc1db67","type":"Selection"},"selection_policy":{"id":"401474b7-c9ce-497e-8db4-f0bac5870c17","type":"UnionRenderers"}},"id":"587178e6-b7eb-4295-91d1-8793e6f6596b","type":"ColumnDataSource"},{"attributes":{},"id":"55f11763-de03-4f85-a0d9-a7d2efc1db67","type":"Selection"},{"attributes":{"args":{"source":{"id":"587178e6-b7eb-4295-91d1-8793e6f6596b","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"824aab07-4489-4cbb-82fe-70cf265fb17a","type":"CustomJS"},{"attributes":{"formatter":{"id":"b1ad3f31-ed7f-4f8f-9ab5-124d33eb8dca","type":"BasicTickFormatter"},"plot":{"id":"6e0599e7-3197-44b5-bde6-7c0f11755569","subtype":"Figure","type":"Plot"},"ticker":{"id":"ef1f08cf-fe68-4ed5-9ff6-bf2f67b5c337","type":"BasicTicker"},"visible":false},"id":"6804a85b-30e0-47c5-83d9-2d5ae6aa70de","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"0d98a5e7-4ac9-4993-94fa-81abea21ef38","type":"Toolbar"},{"attributes":{"dimension":1,"plot":{"id":"6e0599e7-3197-44b5-bde6-7c0f11755569","subtype":"Figure","type":"Plot"},"ticker":{"id":"dc53896c-9b92-424c-9b56-1cbd85a93ca8","type":"BasicTicker"},"visible":false},"id":"abf63261-adc2-42dd-a340-2a896a7238a4","type":"Grid"},{"attributes":{"data_source":{"id":"587178e6-b7eb-4295-91d1-8793e6f6596b","type":"ColumnDataSource"},"glyph":{"id":"b8e3046f-1435-432e-8a5d-3231cdfd3d3f","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"b66ee633-7a2e-4222-8d56-78c8fb82d541","type":"ImageURL"},"selection_glyph":null,"view":{"id":"3d684fd6-a327-448d-bbe3-34d8099184af","type":"CDSView"}},"id":"ac9c3be0-151b-4775-a516-4d3373203ab6","type":"GlyphRenderer"},{"attributes":{},"id":"ef1f08cf-fe68-4ed5-9ff6-bf2f67b5c337","type":"BasicTicker"},{"attributes":{"children":[{"id":"f407306c-b1dd-4475-a25c-d831742ca681","type":"WidgetBox"},{"id":"6e0599e7-3197-44b5-bde6-7c0f11755569","subtype":"Figure","type":"Plot"}]},"id":"96c9c54f-03c3-453b-98c3-382d87e0027a","type":"Column"},{"attributes":{"below":[{"id":"6804a85b-30e0-47c5-83d9-2d5ae6aa70de","type":"LinearAxis"}],"left":[{"id":"d0363c89-c927-4bdf-9895-b7eff4eb5571","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"6804a85b-30e0-47c5-83d9-2d5ae6aa70de","type":"LinearAxis"},{"id":"a3b76f34-1485-4ac9-8da6-12fdd8041e53","type":"Grid"},{"id":"d0363c89-c927-4bdf-9895-b7eff4eb5571","type":"LinearAxis"},{"id":"abf63261-adc2-42dd-a340-2a896a7238a4","type":"Grid"},{"id":"ac9c3be0-151b-4775-a516-4d3373203ab6","type":"GlyphRenderer"}],"title":{"id":"5fe9d8cb-b293-4795-8215-4bacb7eec40f","type":"Title"},"toolbar":{"id":"0d98a5e7-4ac9-4993-94fa-81abea21ef38","type":"Toolbar"},"x_range":{"id":"173dc024-1609-49d0-b895-fc05064e54f6","type":"Range1d"},"x_scale":{"id":"382b45f3-413b-477a-b7dd-a9a3462c2573","type":"LinearScale"},"y_range":{"id":"8267834d-05de-40be-8d8a-4d2615cea3fa","type":"Range1d"},"y_scale":{"id":"4d4ffcad-6aea-4ecc-9a0b-129a3ed8b548","type":"LinearScale"}},"id":"6e0599e7-3197-44b5-bde6-7c0f11755569","subtype":"Figure","type":"Plot"},{"attributes":{"source":{"id":"587178e6-b7eb-4295-91d1-8793e6f6596b","type":"ColumnDataSource"}},"id":"3d684fd6-a327-448d-bbe3-34d8099184af","type":"CDSView"}],"root_ids":["96c9c54f-03c3-453b-98c3-382d87e0027a"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"9108c4ce-a73c-4239-8071-0e35a63594bd","roots":{"96c9c54f-03c3-453b-98c3-382d87e0027a":"0fa34715-d08a-4df3-aa3d-37e24c104139"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();