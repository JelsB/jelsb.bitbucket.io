(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("51d2a67e-2440-44ad-9df2-14d29ecfd81a");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '51d2a67e-2440-44ad-9df2-14d29ecfd81a' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"858e4be4-1204-467a-b23c-14c365ff5c8d":{"roots":{"references":[{"attributes":{},"id":"1ce56d6d-45f9-4fab-a88c-904d4eb47c98","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"bb509689-de75-43fa-941d-58151d33da65","subtype":"Figure","type":"Plot"},"ticker":{"id":"a2afcae0-e6f9-492d-a2cb-53adeb452b76","type":"BasicTicker"},"visible":false},"id":"74b17ab9-ffc5-4074-890c-7250ff300167","type":"Grid"},{"attributes":{},"id":"441d1016-836c-43e5-86cb-27330483df4f","type":"BasicTickFormatter"},{"attributes":{},"id":"d13219a0-9107-48b6-8594-b7496a18c3db","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"5854c66d-b52d-4ccd-b0e2-3468d32ace9f","type":"BasicTickFormatter"},"plot":{"id":"bb509689-de75-43fa-941d-58151d33da65","subtype":"Figure","type":"Plot"},"ticker":{"id":"a2afcae0-e6f9-492d-a2cb-53adeb452b76","type":"BasicTicker"},"visible":false},"id":"4bd8d577-954a-4343-a181-c45a33307fef","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"cf4c3bea-0de9-4383-a23a-1713922d91c2","type":"Toolbar"},{"attributes":{},"id":"aa462093-e704-498a-8ab4-2923f2ac0189","type":"Selection"},{"attributes":{"plot":{"id":"bb509689-de75-43fa-941d-58151d33da65","subtype":"Figure","type":"Plot"},"ticker":{"id":"1ce56d6d-45f9-4fab-a88c-904d4eb47c98","type":"BasicTicker"},"visible":false},"id":"a058a502-ee66-46dc-b49e-e621d385e649","type":"Grid"},{"attributes":{},"id":"30a68cbf-a7f5-4246-a54b-c057feedfc86","type":"LinearScale"},{"attributes":{"source":{"id":"51807376-c177-4673-a831-2612de80c122","type":"ColumnDataSource"}},"id":"f4925fe5-240f-46a6-9077-fe97151f1a21","type":"CDSView"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"77d47332-16e0-4a0d-8663-2ba9e91e64b3","type":"ImageURL"},{"attributes":{"children":[{"id":"dfde9375-d8dd-42e7-b576-ca49c90baf12","type":"Slider"}]},"id":"fc4fb131-8f41-470a-bade-42bc2d3958f9","type":"WidgetBox"},{"attributes":{"args":{"source":{"id":"51807376-c177-4673-a831-2612de80c122","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"f22d9206-31d9-4fb0-abf6-1ae46e17a38d","type":"CustomJS"},{"attributes":{"callback":{"id":"f22d9206-31d9-4fb0-abf6-1ae46e17a38d","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"dfde9375-d8dd-42e7-b576-ca49c90baf12","type":"Slider"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL2O3_000.png"],"url_num":["000"]},"selected":{"id":"aa462093-e704-498a-8ab4-2923f2ac0189","type":"Selection"},"selection_policy":{"id":"d13219a0-9107-48b6-8594-b7496a18c3db","type":"UnionRenderers"}},"id":"51807376-c177-4673-a831-2612de80c122","type":"ColumnDataSource"},{"attributes":{"data_source":{"id":"51807376-c177-4673-a831-2612de80c122","type":"ColumnDataSource"},"glyph":{"id":"4fa741b0-226d-452e-bb9f-57a3b43f52d9","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"77d47332-16e0-4a0d-8663-2ba9e91e64b3","type":"ImageURL"},"selection_glyph":null,"view":{"id":"f4925fe5-240f-46a6-9077-fe97151f1a21","type":"CDSView"}},"id":"f07ccb37-9cc0-4770-b637-8a00cb8ac6bb","type":"GlyphRenderer"},{"attributes":{"plot":null,"text":""},"id":"ec17c91d-e210-4025-96fd-19f72835c17b","type":"Title"},{"attributes":{},"id":"5854c66d-b52d-4ccd-b0e2-3468d32ace9f","type":"BasicTickFormatter"},{"attributes":{},"id":"a2afcae0-e6f9-492d-a2cb-53adeb452b76","type":"BasicTicker"},{"attributes":{"callback":null},"id":"47b25841-395e-4f6c-a26b-4f7eef004ff9","type":"Range1d"},{"attributes":{"children":[{"id":"fc4fb131-8f41-470a-bade-42bc2d3958f9","type":"WidgetBox"},{"id":"bb509689-de75-43fa-941d-58151d33da65","subtype":"Figure","type":"Plot"}]},"id":"fb029411-2ffb-4477-aa26-ea13797022e8","type":"Column"},{"attributes":{"below":[{"id":"faef0006-4656-46c5-b38e-5372086defed","type":"LinearAxis"}],"left":[{"id":"4bd8d577-954a-4343-a181-c45a33307fef","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"faef0006-4656-46c5-b38e-5372086defed","type":"LinearAxis"},{"id":"a058a502-ee66-46dc-b49e-e621d385e649","type":"Grid"},{"id":"4bd8d577-954a-4343-a181-c45a33307fef","type":"LinearAxis"},{"id":"74b17ab9-ffc5-4074-890c-7250ff300167","type":"Grid"},{"id":"f07ccb37-9cc0-4770-b637-8a00cb8ac6bb","type":"GlyphRenderer"}],"title":{"id":"ec17c91d-e210-4025-96fd-19f72835c17b","type":"Title"},"toolbar":{"id":"cf4c3bea-0de9-4383-a23a-1713922d91c2","type":"Toolbar"},"x_range":{"id":"4511edb2-a063-4dd9-8548-10dc1cd018af","type":"Range1d"},"x_scale":{"id":"30a68cbf-a7f5-4246-a54b-c057feedfc86","type":"LinearScale"},"y_range":{"id":"47b25841-395e-4f6c-a26b-4f7eef004ff9","type":"Range1d"},"y_scale":{"id":"1caec17a-b5e9-48e7-a53f-28543e6bc048","type":"LinearScale"}},"id":"bb509689-de75-43fa-941d-58151d33da65","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"4fa741b0-226d-452e-bb9f-57a3b43f52d9","type":"ImageURL"},{"attributes":{"formatter":{"id":"441d1016-836c-43e5-86cb-27330483df4f","type":"BasicTickFormatter"},"plot":{"id":"bb509689-de75-43fa-941d-58151d33da65","subtype":"Figure","type":"Plot"},"ticker":{"id":"1ce56d6d-45f9-4fab-a88c-904d4eb47c98","type":"BasicTicker"},"visible":false},"id":"faef0006-4656-46c5-b38e-5372086defed","type":"LinearAxis"},{"attributes":{},"id":"1caec17a-b5e9-48e7-a53f-28543e6bc048","type":"LinearScale"},{"attributes":{"callback":null},"id":"4511edb2-a063-4dd9-8548-10dc1cd018af","type":"Range1d"}],"root_ids":["fb029411-2ffb-4477-aa26-ea13797022e8"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"858e4be4-1204-467a-b23c-14c365ff5c8d","roots":{"fb029411-2ffb-4477-aa26-ea13797022e8":"51d2a67e-2440-44ad-9df2-14d29ecfd81a"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();