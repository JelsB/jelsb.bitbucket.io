(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("d8a63af7-9aeb-4cb6-9a5b-6f4cb211e287");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'd8a63af7-9aeb-4cb6-9a5b-6f4cb211e287' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"e1890a3f-378c-43f6-931d-0ca7915a84cb":{"roots":{"references":[{"attributes":{"dimension":1,"plot":{"id":"0107ffbd-391e-45a2-b787-46330894bd92","subtype":"Figure","type":"Plot"},"ticker":{"id":"fa71a4ba-e92b-4470-8896-d906328eb9df","type":"BasicTicker"},"visible":false},"id":"cb77fb31-320f-4ec5-8ccf-489597379c3f","type":"Grid"},{"attributes":{"plot":{"id":"0107ffbd-391e-45a2-b787-46330894bd92","subtype":"Figure","type":"Plot"},"ticker":{"id":"db3861ec-4be7-4144-8588-ba3a1172fb94","type":"BasicTicker"},"visible":false},"id":"a3709c7d-08ca-447e-9a5f-cf3f19ff8f0f","type":"Grid"},{"attributes":{},"id":"fa71a4ba-e92b-4470-8896-d906328eb9df","type":"BasicTicker"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"a97d0c9d-53fd-4e78-b5ab-3a9b32ac5813","type":"Toolbar"},{"attributes":{},"id":"5871ee38-adc6-460e-aa8f-35f168887278","type":"LinearScale"},{"attributes":{"children":[{"id":"737d7de3-3c15-4a94-bf41-6e931c260870","type":"WidgetBox"},{"id":"0107ffbd-391e-45a2-b787-46330894bd92","subtype":"Figure","type":"Plot"}]},"id":"d2c9d740-f12b-44e5-99d4-1e45bab2f3d6","type":"Column"},{"attributes":{"formatter":{"id":"2390c32d-9919-4ee2-827f-f35ac386a9d6","type":"BasicTickFormatter"},"plot":{"id":"0107ffbd-391e-45a2-b787-46330894bd92","subtype":"Figure","type":"Plot"},"ticker":{"id":"db3861ec-4be7-4144-8588-ba3a1172fb94","type":"BasicTicker"},"visible":false},"id":"7034e851-0e24-4a2b-9fd6-ebefe5f3f509","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"13fe378a-1b75-44b2-a32e-44a4e4569913","type":"Title"},{"attributes":{"source":{"id":"5d408604-c82d-4ac6-b691-e173009137f8","type":"ColumnDataSource"}},"id":"e0c4c726-e2e9-4040-850f-b39b985d8328","type":"CDSView"},{"attributes":{},"id":"db3861ec-4be7-4144-8588-ba3a1172fb94","type":"BasicTicker"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"90c73758-d0d2-462e-ae30-485353d64b61","type":"ImageURL"},{"attributes":{},"id":"60cf32fe-1085-4269-9f27-e5950661d7a3","type":"Selection"},{"attributes":{},"id":"90f9c7b0-1042-4a74-becf-6be73b6da92b","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI3O3_000.png"],"url_num":["000"]},"selected":{"id":"60cf32fe-1085-4269-9f27-e5950661d7a3","type":"Selection"},"selection_policy":{"id":"d0ba07d3-4338-4481-bf26-50834a1799f0","type":"UnionRenderers"}},"id":"5d408604-c82d-4ac6-b691-e173009137f8","type":"ColumnDataSource"},{"attributes":{"callback":{"id":"50c7149b-365a-4da8-94a2-5d4f8cdb0937","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"50a122b0-2b55-4c49-8333-8c51d68a5dde","type":"Slider"},{"attributes":{"callback":null},"id":"7d09e4d9-08c9-4906-9d8f-6b8ec19bd5d6","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"afe6576a-ae5a-4c0e-b136-5de72eec9933","type":"ImageURL"},{"attributes":{"formatter":{"id":"58fc3d24-435c-42b9-8886-fbd14ad59d2f","type":"BasicTickFormatter"},"plot":{"id":"0107ffbd-391e-45a2-b787-46330894bd92","subtype":"Figure","type":"Plot"},"ticker":{"id":"fa71a4ba-e92b-4470-8896-d906328eb9df","type":"BasicTicker"},"visible":false},"id":"7d8afc50-c31c-41fc-9361-bc295309a02d","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"5d408604-c82d-4ac6-b691-e173009137f8","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"50c7149b-365a-4da8-94a2-5d4f8cdb0937","type":"CustomJS"},{"attributes":{},"id":"2390c32d-9919-4ee2-827f-f35ac386a9d6","type":"BasicTickFormatter"},{"attributes":{},"id":"58fc3d24-435c-42b9-8886-fbd14ad59d2f","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"6a7b44c8-83ab-4c34-9e19-6e0d947e4ae9","type":"Range1d"},{"attributes":{"children":[{"id":"50a122b0-2b55-4c49-8333-8c51d68a5dde","type":"Slider"}]},"id":"737d7de3-3c15-4a94-bf41-6e931c260870","type":"WidgetBox"},{"attributes":{},"id":"d0ba07d3-4338-4481-bf26-50834a1799f0","type":"UnionRenderers"},{"attributes":{"below":[{"id":"7034e851-0e24-4a2b-9fd6-ebefe5f3f509","type":"LinearAxis"}],"left":[{"id":"7d8afc50-c31c-41fc-9361-bc295309a02d","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"7034e851-0e24-4a2b-9fd6-ebefe5f3f509","type":"LinearAxis"},{"id":"a3709c7d-08ca-447e-9a5f-cf3f19ff8f0f","type":"Grid"},{"id":"7d8afc50-c31c-41fc-9361-bc295309a02d","type":"LinearAxis"},{"id":"cb77fb31-320f-4ec5-8ccf-489597379c3f","type":"Grid"},{"id":"6c9a08b9-1241-40db-8511-b1d29d208e5e","type":"GlyphRenderer"}],"title":{"id":"13fe378a-1b75-44b2-a32e-44a4e4569913","type":"Title"},"toolbar":{"id":"a97d0c9d-53fd-4e78-b5ab-3a9b32ac5813","type":"Toolbar"},"x_range":{"id":"7d09e4d9-08c9-4906-9d8f-6b8ec19bd5d6","type":"Range1d"},"x_scale":{"id":"5871ee38-adc6-460e-aa8f-35f168887278","type":"LinearScale"},"y_range":{"id":"6a7b44c8-83ab-4c34-9e19-6e0d947e4ae9","type":"Range1d"},"y_scale":{"id":"90f9c7b0-1042-4a74-becf-6be73b6da92b","type":"LinearScale"}},"id":"0107ffbd-391e-45a2-b787-46330894bd92","subtype":"Figure","type":"Plot"},{"attributes":{"data_source":{"id":"5d408604-c82d-4ac6-b691-e173009137f8","type":"ColumnDataSource"},"glyph":{"id":"afe6576a-ae5a-4c0e-b136-5de72eec9933","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"90c73758-d0d2-462e-ae30-485353d64b61","type":"ImageURL"},"selection_glyph":null,"view":{"id":"e0c4c726-e2e9-4040-850f-b39b985d8328","type":"CDSView"}},"id":"6c9a08b9-1241-40db-8511-b1d29d208e5e","type":"GlyphRenderer"}],"root_ids":["d2c9d740-f12b-44e5-99d4-1e45bab2f3d6"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"e1890a3f-378c-43f6-931d-0ca7915a84cb","roots":{"d2c9d740-f12b-44e5-99d4-1e45bab2f3d6":"d8a63af7-9aeb-4cb6-9a5b-6f4cb211e287"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();