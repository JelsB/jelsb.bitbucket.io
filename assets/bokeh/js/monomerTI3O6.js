(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("7617d456-7d9c-4c4f-915f-f9eb667fdd3f");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '7617d456-7d9c-4c4f-915f-f9eb667fdd3f' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"5f18e094-53f2-4d31-9d1b-a59848581443":{"roots":{"references":[{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"844e1bc2-f0f1-423f-97ad-c571b916d6f7","type":"ImageURL"},{"attributes":{},"id":"68d6c876-8505-4ade-9767-f796ee1a4138","type":"LinearScale"},{"attributes":{},"id":"5d7ace79-d2fc-42e4-adb0-7783f8180d76","type":"LinearScale"},{"attributes":{"callback":{"id":"440d8289-7efb-4bae-9848-d732c3da716b","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"dac6ca69-ea1c-4053-b63a-d2ab3df83367","type":"Slider"},{"attributes":{"callback":null},"id":"ac08b2be-157a-49f6-b91e-8a84eeaf12d9","type":"Range1d"},{"attributes":{"args":{"source":{"id":"c3587ef0-d78f-48ca-902c-c14606771230","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"440d8289-7efb-4bae-9848-d732c3da716b","type":"CustomJS"},{"attributes":{},"id":"c00791f4-cdd3-4cca-a493-3f27b143f197","type":"BasicTicker"},{"attributes":{"source":{"id":"c3587ef0-d78f-48ca-902c-c14606771230","type":"ColumnDataSource"}},"id":"d1103593-a19d-4d72-bb3b-45e443939df2","type":"CDSView"},{"attributes":{"callback":null},"id":"49c5172a-d8c5-4e21-add9-6fb52419e152","type":"Range1d"},{"attributes":{},"id":"c1e2262d-f9bf-48c2-afca-cdfff01941b9","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"0c5030ed-2077-44d0-9d69-436ca36d8429","type":"ImageURL"},{"attributes":{"plot":null,"text":""},"id":"ab07314e-8d06-4bde-9fd2-a40fe067e95c","type":"Title"},{"attributes":{"plot":{"id":"deb4489e-2645-4e3f-8cfe-95044f2de492","subtype":"Figure","type":"Plot"},"ticker":{"id":"1f07fcf2-bc87-4b62-b2b9-927909c2163d","type":"BasicTicker"},"visible":false},"id":"ab2ca169-ba7b-4250-b71b-d05ae9650010","type":"Grid"},{"attributes":{},"id":"ba4e9840-adcc-4e02-b0b0-59c7647999c7","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"c1e2262d-f9bf-48c2-afca-cdfff01941b9","type":"BasicTickFormatter"},"plot":{"id":"deb4489e-2645-4e3f-8cfe-95044f2de492","subtype":"Figure","type":"Plot"},"ticker":{"id":"1f07fcf2-bc87-4b62-b2b9-927909c2163d","type":"BasicTicker"},"visible":false},"id":"7ec05f22-4209-4e1d-a416-c3318a11d585","type":"LinearAxis"},{"attributes":{},"id":"1f07fcf2-bc87-4b62-b2b9-927909c2163d","type":"BasicTicker"},{"attributes":{"data_source":{"id":"c3587ef0-d78f-48ca-902c-c14606771230","type":"ColumnDataSource"},"glyph":{"id":"0c5030ed-2077-44d0-9d69-436ca36d8429","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"844e1bc2-f0f1-423f-97ad-c571b916d6f7","type":"ImageURL"},"selection_glyph":null,"view":{"id":"d1103593-a19d-4d72-bb3b-45e443939df2","type":"CDSView"}},"id":"340c6ea7-0f7c-4ea4-bec6-9f08f5a4b645","type":"GlyphRenderer"},{"attributes":{},"id":"bc173d42-a1a3-4656-8459-d824a5c039e3","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"bc173d42-a1a3-4656-8459-d824a5c039e3","type":"BasicTickFormatter"},"plot":{"id":"deb4489e-2645-4e3f-8cfe-95044f2de492","subtype":"Figure","type":"Plot"},"ticker":{"id":"c00791f4-cdd3-4cca-a493-3f27b143f197","type":"BasicTicker"},"visible":false},"id":"f27717c7-8015-488b-97b1-f951b5b35939","type":"LinearAxis"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI3O6_000.png"],"url_num":["000"]},"selected":{"id":"c23b66a9-32ee-48b5-83d3-c1efa1b4fcc5","type":"Selection"},"selection_policy":{"id":"ba4e9840-adcc-4e02-b0b0-59c7647999c7","type":"UnionRenderers"}},"id":"c3587ef0-d78f-48ca-902c-c14606771230","type":"ColumnDataSource"},{"attributes":{"dimension":1,"plot":{"id":"deb4489e-2645-4e3f-8cfe-95044f2de492","subtype":"Figure","type":"Plot"},"ticker":{"id":"c00791f4-cdd3-4cca-a493-3f27b143f197","type":"BasicTicker"},"visible":false},"id":"092c52c2-9511-4484-9c44-e25ac551d127","type":"Grid"},{"attributes":{},"id":"c23b66a9-32ee-48b5-83d3-c1efa1b4fcc5","type":"Selection"},{"attributes":{"children":[{"id":"773e1570-47c9-49f2-99f5-d8b10a037261","type":"WidgetBox"},{"id":"deb4489e-2645-4e3f-8cfe-95044f2de492","subtype":"Figure","type":"Plot"}]},"id":"01a837f7-02be-446a-8d80-3b7ee9ed428c","type":"Column"},{"attributes":{"below":[{"id":"7ec05f22-4209-4e1d-a416-c3318a11d585","type":"LinearAxis"}],"left":[{"id":"f27717c7-8015-488b-97b1-f951b5b35939","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"7ec05f22-4209-4e1d-a416-c3318a11d585","type":"LinearAxis"},{"id":"ab2ca169-ba7b-4250-b71b-d05ae9650010","type":"Grid"},{"id":"f27717c7-8015-488b-97b1-f951b5b35939","type":"LinearAxis"},{"id":"092c52c2-9511-4484-9c44-e25ac551d127","type":"Grid"},{"id":"340c6ea7-0f7c-4ea4-bec6-9f08f5a4b645","type":"GlyphRenderer"}],"title":{"id":"ab07314e-8d06-4bde-9fd2-a40fe067e95c","type":"Title"},"toolbar":{"id":"f0b538e9-cf18-4c0f-875d-5bbcedba5f79","type":"Toolbar"},"x_range":{"id":"49c5172a-d8c5-4e21-add9-6fb52419e152","type":"Range1d"},"x_scale":{"id":"68d6c876-8505-4ade-9767-f796ee1a4138","type":"LinearScale"},"y_range":{"id":"ac08b2be-157a-49f6-b91e-8a84eeaf12d9","type":"Range1d"},"y_scale":{"id":"5d7ace79-d2fc-42e4-adb0-7783f8180d76","type":"LinearScale"}},"id":"deb4489e-2645-4e3f-8cfe-95044f2de492","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"dac6ca69-ea1c-4053-b63a-d2ab3df83367","type":"Slider"}]},"id":"773e1570-47c9-49f2-99f5-d8b10a037261","type":"WidgetBox"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"f0b538e9-cf18-4c0f-875d-5bbcedba5f79","type":"Toolbar"}],"root_ids":["01a837f7-02be-446a-8d80-3b7ee9ed428c"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"5f18e094-53f2-4d31-9d1b-a59848581443","roots":{"01a837f7-02be-446a-8d80-3b7ee9ed428c":"7617d456-7d9c-4c4f-915f-f9eb667fdd3f"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();