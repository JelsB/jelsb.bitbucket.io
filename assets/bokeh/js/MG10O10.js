(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("f1d2ea2b-7ae6-4f1e-83d3-61e438a9a78f");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'f1d2ea2b-7ae6-4f1e-83d3-61e438a9a78f' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"8869ba55-4b16-4779-ade1-aa69791ef962":{"roots":{"references":[{"attributes":{"plot":null,"text":""},"id":"8526214e-a6af-4e35-91d7-45712652bb84","type":"Title"},{"attributes":{},"id":"f1518724-2fa3-45ed-9ef6-c23f682a6c1f","type":"BasicTicker"},{"attributes":{"args":{"source":{"id":"48041a0c-4684-43c0-82e3-121d15c7b6fa","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"1eaf2ca2-1e6a-43c9-9ad2-a4e1b747c0b0","type":"CustomJS"},{"attributes":{"children":[{"id":"96ed9fcf-bbb8-452c-a3ff-d7e88798a18f","type":"Slider"}]},"id":"1f7d9a00-40d7-4a40-9e53-e8e4eb98981e","type":"WidgetBox"},{"attributes":{"data_source":{"id":"48041a0c-4684-43c0-82e3-121d15c7b6fa","type":"ColumnDataSource"},"glyph":{"id":"62df6646-b6b0-4ab2-b762-2472578aef36","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c21f6dfe-7bfd-4740-b87e-b9eb892528ee","type":"ImageURL"},"selection_glyph":null,"view":{"id":"112380a9-7d22-47f7-8c5c-6e2547617b9a","type":"CDSView"}},"id":"31dfea8d-d344-44fa-89c4-92ffd3a3504f","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"870d72b5-8844-4f94-b1bb-ca2a1eb6e814","type":"Toolbar"},{"attributes":{},"id":"fe2b85b1-8806-47d7-b3f7-ea9af671ef0f","type":"BasicTicker"},{"attributes":{"formatter":{"id":"d5817a99-da8f-4d3c-9d0c-b4258c26fc6a","type":"BasicTickFormatter"},"plot":{"id":"43a9cfea-7330-408d-9926-44226ece5759","subtype":"Figure","type":"Plot"},"ticker":{"id":"f1518724-2fa3-45ed-9ef6-c23f682a6c1f","type":"BasicTicker"},"visible":false},"id":"5de7ecb1-a769-4acc-9677-d6954fb3a01a","type":"LinearAxis"},{"attributes":{"formatter":{"id":"257d7de7-df3f-46c2-89dd-f0b61a6cbe99","type":"BasicTickFormatter"},"plot":{"id":"43a9cfea-7330-408d-9926-44226ece5759","subtype":"Figure","type":"Plot"},"ticker":{"id":"fe2b85b1-8806-47d7-b3f7-ea9af671ef0f","type":"BasicTicker"},"visible":false},"id":"65a00119-a13c-48a6-86e3-a876a2c19224","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c21f6dfe-7bfd-4740-b87e-b9eb892528ee","type":"ImageURL"},{"attributes":{"dimension":1,"plot":{"id":"43a9cfea-7330-408d-9926-44226ece5759","subtype":"Figure","type":"Plot"},"ticker":{"id":"fe2b85b1-8806-47d7-b3f7-ea9af671ef0f","type":"BasicTicker"},"visible":false},"id":"3f24161a-7742-4200-8f0e-59bf2d7442e3","type":"Grid"},{"attributes":{"source":{"id":"48041a0c-4684-43c0-82e3-121d15c7b6fa","type":"ColumnDataSource"}},"id":"112380a9-7d22-47f7-8c5c-6e2547617b9a","type":"CDSView"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG10O10_000.png"],"url_num":["000"]},"selected":{"id":"aeaddbf0-1647-4a2f-a66f-642d03086dec","type":"Selection"},"selection_policy":{"id":"9fc510aa-9629-426a-9f79-a122b9428bb7","type":"UnionRenderers"}},"id":"48041a0c-4684-43c0-82e3-121d15c7b6fa","type":"ColumnDataSource"},{"attributes":{},"id":"257d7de7-df3f-46c2-89dd-f0b61a6cbe99","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"1f7d9a00-40d7-4a40-9e53-e8e4eb98981e","type":"WidgetBox"},{"id":"43a9cfea-7330-408d-9926-44226ece5759","subtype":"Figure","type":"Plot"}]},"id":"fd18a7c3-626f-43c9-8de1-18b816f2f824","type":"Column"},{"attributes":{"callback":null},"id":"a486b0ed-1442-43c4-9325-1bef42bc8476","type":"Range1d"},{"attributes":{},"id":"b511adc4-c33b-4a67-bcd2-f60e05c45ced","type":"LinearScale"},{"attributes":{},"id":"aeaddbf0-1647-4a2f-a66f-642d03086dec","type":"Selection"},{"attributes":{"callback":{"id":"1eaf2ca2-1e6a-43c9-9ad2-a4e1b747c0b0","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"96ed9fcf-bbb8-452c-a3ff-d7e88798a18f","type":"Slider"},{"attributes":{"plot":{"id":"43a9cfea-7330-408d-9926-44226ece5759","subtype":"Figure","type":"Plot"},"ticker":{"id":"f1518724-2fa3-45ed-9ef6-c23f682a6c1f","type":"BasicTicker"},"visible":false},"id":"531d07e4-fdf6-4e9a-8ec8-94a1dc9f9804","type":"Grid"},{"attributes":{"below":[{"id":"5de7ecb1-a769-4acc-9677-d6954fb3a01a","type":"LinearAxis"}],"left":[{"id":"65a00119-a13c-48a6-86e3-a876a2c19224","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"5de7ecb1-a769-4acc-9677-d6954fb3a01a","type":"LinearAxis"},{"id":"531d07e4-fdf6-4e9a-8ec8-94a1dc9f9804","type":"Grid"},{"id":"65a00119-a13c-48a6-86e3-a876a2c19224","type":"LinearAxis"},{"id":"3f24161a-7742-4200-8f0e-59bf2d7442e3","type":"Grid"},{"id":"31dfea8d-d344-44fa-89c4-92ffd3a3504f","type":"GlyphRenderer"}],"title":{"id":"8526214e-a6af-4e35-91d7-45712652bb84","type":"Title"},"toolbar":{"id":"870d72b5-8844-4f94-b1bb-ca2a1eb6e814","type":"Toolbar"},"x_range":{"id":"a486b0ed-1442-43c4-9325-1bef42bc8476","type":"Range1d"},"x_scale":{"id":"b511adc4-c33b-4a67-bcd2-f60e05c45ced","type":"LinearScale"},"y_range":{"id":"84818831-75fd-4559-9487-db180de9c822","type":"Range1d"},"y_scale":{"id":"02e37df2-441b-427f-a3c6-4cf736beb8f0","type":"LinearScale"}},"id":"43a9cfea-7330-408d-9926-44226ece5759","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"02e37df2-441b-427f-a3c6-4cf736beb8f0","type":"LinearScale"},{"attributes":{"callback":null},"id":"84818831-75fd-4559-9487-db180de9c822","type":"Range1d"},{"attributes":{},"id":"d5817a99-da8f-4d3c-9d0c-b4258c26fc6a","type":"BasicTickFormatter"},{"attributes":{},"id":"9fc510aa-9629-426a-9f79-a122b9428bb7","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"62df6646-b6b0-4ab2-b762-2472578aef36","type":"ImageURL"}],"root_ids":["fd18a7c3-626f-43c9-8de1-18b816f2f824"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"8869ba55-4b16-4779-ade1-aa69791ef962","roots":{"fd18a7c3-626f-43c9-8de1-18b816f2f824":"f1d2ea2b-7ae6-4f1e-83d3-61e438a9a78f"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();