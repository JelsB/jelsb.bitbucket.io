(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("3a2e3620-ea2a-4bc1-8e70-9e5397f22c6a");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '3a2e3620-ea2a-4bc1-8e70-9e5397f22c6a' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"6d685225-a945-4c40-9410-6a1a41308246":{"roots":{"references":[{"attributes":{"plot":{"id":"5f31b61d-60ee-4b21-b7c7-9598f88802bf","subtype":"Figure","type":"Plot"},"ticker":{"id":"c58783b9-3a47-445a-b1e8-be0da2864418","type":"BasicTicker"},"visible":false},"id":"f082ff42-0875-482f-84ce-40499ab18154","type":"Grid"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"e29cb046-bc32-4208-ad54-fd090d0a0a9f","type":"Toolbar"},{"attributes":{"plot":null,"text":""},"id":"d4b04fc6-1563-445c-924e-475c29123115","type":"Title"},{"attributes":{},"id":"392715c2-68ab-4885-90b1-2b6828b3c301","type":"BasicTickFormatter"},{"attributes":{},"id":"6e4e8f49-658f-443d-9361-e55b26b7d82a","type":"BasicTickFormatter"},{"attributes":{"dimension":1,"plot":{"id":"5f31b61d-60ee-4b21-b7c7-9598f88802bf","subtype":"Figure","type":"Plot"},"ticker":{"id":"f37469cd-0cfb-4984-b28c-41e57a0830d5","type":"BasicTicker"},"visible":false},"id":"b142af61-ba5d-4c35-b9cc-fd874586f95e","type":"Grid"},{"attributes":{},"id":"3c7945b0-5a9c-456a-bb46-b3d2c7d4c885","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL10O15_000.png"],"url_num":["000"]},"selected":{"id":"89309c01-6bf0-446a-b208-b718dacd795f","type":"Selection"},"selection_policy":{"id":"ca74ba66-4b7b-4bfc-97cc-1c7fb1606b29","type":"UnionRenderers"}},"id":"0d80e5ef-2655-46c0-a5b5-9f288c03f076","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"dc08624e-8278-42fe-a52d-ba758ed32444","type":"LinearAxis"}],"left":[{"id":"115fab3d-b048-4b3f-b9ec-e7aa405bcc3f","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"dc08624e-8278-42fe-a52d-ba758ed32444","type":"LinearAxis"},{"id":"f082ff42-0875-482f-84ce-40499ab18154","type":"Grid"},{"id":"115fab3d-b048-4b3f-b9ec-e7aa405bcc3f","type":"LinearAxis"},{"id":"b142af61-ba5d-4c35-b9cc-fd874586f95e","type":"Grid"},{"id":"95b31c21-bd17-4227-808d-cd19cc33044e","type":"GlyphRenderer"}],"title":{"id":"d4b04fc6-1563-445c-924e-475c29123115","type":"Title"},"toolbar":{"id":"e29cb046-bc32-4208-ad54-fd090d0a0a9f","type":"Toolbar"},"x_range":{"id":"3c0c8bb1-cf06-40a0-bf15-69ebabe53bda","type":"Range1d"},"x_scale":{"id":"3c7945b0-5a9c-456a-bb46-b3d2c7d4c885","type":"LinearScale"},"y_range":{"id":"6f4c347f-c530-4542-9e0c-390545106ff9","type":"Range1d"},"y_scale":{"id":"18245800-c0a0-48db-8375-f730fc5539ae","type":"LinearScale"}},"id":"5f31b61d-60ee-4b21-b7c7-9598f88802bf","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"ca74ba66-4b7b-4bfc-97cc-1c7fb1606b29","type":"UnionRenderers"},{"attributes":{},"id":"f37469cd-0cfb-4984-b28c-41e57a0830d5","type":"BasicTicker"},{"attributes":{},"id":"c58783b9-3a47-445a-b1e8-be0da2864418","type":"BasicTicker"},{"attributes":{"data_source":{"id":"0d80e5ef-2655-46c0-a5b5-9f288c03f076","type":"ColumnDataSource"},"glyph":{"id":"27aa6422-de32-4ee9-982a-c6f9661f061e","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c7a938f7-a12b-49d2-9c98-f307fe74a16a","type":"ImageURL"},"selection_glyph":null,"view":{"id":"23714089-3d11-47cf-8bed-efadd1d6c028","type":"CDSView"}},"id":"95b31c21-bd17-4227-808d-cd19cc33044e","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c7a938f7-a12b-49d2-9c98-f307fe74a16a","type":"ImageURL"},{"attributes":{"children":[{"id":"0c24fe24-f609-49cd-87c0-df2c3908ae80","type":"WidgetBox"},{"id":"5f31b61d-60ee-4b21-b7c7-9598f88802bf","subtype":"Figure","type":"Plot"}]},"id":"3875a859-2673-4837-aacc-cf5c848eb4cc","type":"Column"},{"attributes":{"callback":null},"id":"3c0c8bb1-cf06-40a0-bf15-69ebabe53bda","type":"Range1d"},{"attributes":{"callback":null},"id":"6f4c347f-c530-4542-9e0c-390545106ff9","type":"Range1d"},{"attributes":{"children":[{"id":"aaaa8ffe-0b6d-4da2-9625-a6f74b5b37e6","type":"Slider"}]},"id":"0c24fe24-f609-49cd-87c0-df2c3908ae80","type":"WidgetBox"},{"attributes":{"source":{"id":"0d80e5ef-2655-46c0-a5b5-9f288c03f076","type":"ColumnDataSource"}},"id":"23714089-3d11-47cf-8bed-efadd1d6c028","type":"CDSView"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"27aa6422-de32-4ee9-982a-c6f9661f061e","type":"ImageURL"},{"attributes":{},"id":"89309c01-6bf0-446a-b208-b718dacd795f","type":"Selection"},{"attributes":{},"id":"18245800-c0a0-48db-8375-f730fc5539ae","type":"LinearScale"},{"attributes":{"args":{"source":{"id":"0d80e5ef-2655-46c0-a5b5-9f288c03f076","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"89f94e6d-7579-476c-9201-e94e5d9c6c8d","type":"CustomJS"},{"attributes":{"formatter":{"id":"392715c2-68ab-4885-90b1-2b6828b3c301","type":"BasicTickFormatter"},"plot":{"id":"5f31b61d-60ee-4b21-b7c7-9598f88802bf","subtype":"Figure","type":"Plot"},"ticker":{"id":"f37469cd-0cfb-4984-b28c-41e57a0830d5","type":"BasicTicker"},"visible":false},"id":"115fab3d-b048-4b3f-b9ec-e7aa405bcc3f","type":"LinearAxis"},{"attributes":{"formatter":{"id":"6e4e8f49-658f-443d-9361-e55b26b7d82a","type":"BasicTickFormatter"},"plot":{"id":"5f31b61d-60ee-4b21-b7c7-9598f88802bf","subtype":"Figure","type":"Plot"},"ticker":{"id":"c58783b9-3a47-445a-b1e8-be0da2864418","type":"BasicTicker"},"visible":false},"id":"dc08624e-8278-42fe-a52d-ba758ed32444","type":"LinearAxis"},{"attributes":{"callback":{"id":"89f94e6d-7579-476c-9201-e94e5d9c6c8d","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"aaaa8ffe-0b6d-4da2-9625-a6f74b5b37e6","type":"Slider"}],"root_ids":["3875a859-2673-4837-aacc-cf5c848eb4cc"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"6d685225-a945-4c40-9410-6a1a41308246","roots":{"3875a859-2673-4837-aacc-cf5c848eb4cc":"3a2e3620-ea2a-4bc1-8e70-9e5397f22c6a"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();