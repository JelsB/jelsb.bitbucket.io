(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("491bed0c-353e-4644-9f5f-7f3612e3459e");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '491bed0c-353e-4644-9f5f-7f3612e3459e' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"b5624de9-4bd9-4805-9048-b612ee4a658e":{"roots":{"references":[{"attributes":{"data_source":{"id":"27b73cf2-0f95-48bc-af92-ecd7cc9a92e2","type":"ColumnDataSource"},"glyph":{"id":"33844604-bf3b-457d-ba23-14550187ff15","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"6ff08b60-679c-4e5b-a457-ee1a0bfe4d19","type":"ImageURL"},"selection_glyph":null,"view":{"id":"7d132f01-b1e8-4fbc-9927-4fdaab20abf5","type":"CDSView"}},"id":"e7088cc2-52c4-43af-a178-40984f6f626a","type":"GlyphRenderer"},{"attributes":{},"id":"ba56a7df-8aa3-4e54-b421-7d9bc108f2a9","type":"BasicTicker"},{"attributes":{},"id":"6255cc28-9343-4281-a55a-bbb6f0179f01","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"195fed35-29c0-467e-91a9-9607989f349f","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"6efc3b0e-6abd-4e63-a6ff-029929c41c02","subtype":"Figure","type":"Plot"},"ticker":{"id":"df92e84b-2c3f-4463-ba63-98016d833cf1","type":"BasicTicker"},"visible":false},"id":"b3d76995-8ffc-47b9-9c3f-67bec45435e6","type":"Grid"},{"attributes":{"callback":{"id":"7d491855-f9ea-4d10-a7ad-e2ec89c8fa6e","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c394e938-227a-4599-9d92-e429d67356d4","type":"Slider"},{"attributes":{"children":[{"id":"10ca9b99-5392-4e52-a3aa-a35ee77517a1","type":"WidgetBox"},{"id":"6efc3b0e-6abd-4e63-a6ff-029929c41c02","subtype":"Figure","type":"Plot"}]},"id":"0a1e8f15-58b9-4b9b-9c81-1e2c6f35a9aa","type":"Column"},{"attributes":{},"id":"7f0caf04-cdae-4e2a-8622-9ed53a6fbb54","type":"LinearScale"},{"attributes":{},"id":"da245b66-a24a-4dc8-b705-0d5377e984e4","type":"LinearScale"},{"attributes":{},"id":"322ea8e5-a772-4340-860d-d4752897277c","type":"Selection"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL8O12_000.png"],"url_num":["000"]},"selected":{"id":"322ea8e5-a772-4340-860d-d4752897277c","type":"Selection"},"selection_policy":{"id":"b82b677b-6c9a-491e-a9f3-4ce18305141e","type":"UnionRenderers"}},"id":"27b73cf2-0f95-48bc-af92-ecd7cc9a92e2","type":"ColumnDataSource"},{"attributes":{"plot":null,"text":""},"id":"6ad97947-b264-49c2-a8bb-1ab946e192f5","type":"Title"},{"attributes":{"children":[{"id":"c394e938-227a-4599-9d92-e429d67356d4","type":"Slider"}]},"id":"10ca9b99-5392-4e52-a3aa-a35ee77517a1","type":"WidgetBox"},{"attributes":{"plot":{"id":"6efc3b0e-6abd-4e63-a6ff-029929c41c02","subtype":"Figure","type":"Plot"},"ticker":{"id":"ba56a7df-8aa3-4e54-b421-7d9bc108f2a9","type":"BasicTicker"},"visible":false},"id":"d1a6f878-b949-45a9-80ee-b56063ff503d","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"33844604-bf3b-457d-ba23-14550187ff15","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"6ff08b60-679c-4e5b-a457-ee1a0bfe4d19","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"27b73cf2-0f95-48bc-af92-ecd7cc9a92e2","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"7d491855-f9ea-4d10-a7ad-e2ec89c8fa6e","type":"CustomJS"},{"attributes":{"formatter":{"id":"6255cc28-9343-4281-a55a-bbb6f0179f01","type":"BasicTickFormatter"},"plot":{"id":"6efc3b0e-6abd-4e63-a6ff-029929c41c02","subtype":"Figure","type":"Plot"},"ticker":{"id":"ba56a7df-8aa3-4e54-b421-7d9bc108f2a9","type":"BasicTicker"},"visible":false},"id":"991afd39-d9fc-436d-9ea1-ae458e860598","type":"LinearAxis"},{"attributes":{},"id":"ba788882-d6de-40b6-adca-c130e6cf3c33","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"b8a8813a-1e49-44fb-8785-606334afd0d0","type":"Range1d"},{"attributes":{},"id":"df92e84b-2c3f-4463-ba63-98016d833cf1","type":"BasicTicker"},{"attributes":{"formatter":{"id":"ba788882-d6de-40b6-adca-c130e6cf3c33","type":"BasicTickFormatter"},"plot":{"id":"6efc3b0e-6abd-4e63-a6ff-029929c41c02","subtype":"Figure","type":"Plot"},"ticker":{"id":"df92e84b-2c3f-4463-ba63-98016d833cf1","type":"BasicTicker"},"visible":false},"id":"5b628e9f-518f-4f2d-a89e-b72a6c062f96","type":"LinearAxis"},{"attributes":{},"id":"b82b677b-6c9a-491e-a9f3-4ce18305141e","type":"UnionRenderers"},{"attributes":{"source":{"id":"27b73cf2-0f95-48bc-af92-ecd7cc9a92e2","type":"ColumnDataSource"}},"id":"7d132f01-b1e8-4fbc-9927-4fdaab20abf5","type":"CDSView"},{"attributes":{"below":[{"id":"991afd39-d9fc-436d-9ea1-ae458e860598","type":"LinearAxis"}],"left":[{"id":"5b628e9f-518f-4f2d-a89e-b72a6c062f96","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"991afd39-d9fc-436d-9ea1-ae458e860598","type":"LinearAxis"},{"id":"d1a6f878-b949-45a9-80ee-b56063ff503d","type":"Grid"},{"id":"5b628e9f-518f-4f2d-a89e-b72a6c062f96","type":"LinearAxis"},{"id":"b3d76995-8ffc-47b9-9c3f-67bec45435e6","type":"Grid"},{"id":"e7088cc2-52c4-43af-a178-40984f6f626a","type":"GlyphRenderer"}],"title":{"id":"6ad97947-b264-49c2-a8bb-1ab946e192f5","type":"Title"},"toolbar":{"id":"3c430910-1476-4b38-a3c0-5d90766ba44d","type":"Toolbar"},"x_range":{"id":"b8a8813a-1e49-44fb-8785-606334afd0d0","type":"Range1d"},"x_scale":{"id":"7f0caf04-cdae-4e2a-8622-9ed53a6fbb54","type":"LinearScale"},"y_range":{"id":"195fed35-29c0-467e-91a9-9607989f349f","type":"Range1d"},"y_scale":{"id":"da245b66-a24a-4dc8-b705-0d5377e984e4","type":"LinearScale"}},"id":"6efc3b0e-6abd-4e63-a6ff-029929c41c02","subtype":"Figure","type":"Plot"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"3c430910-1476-4b38-a3c0-5d90766ba44d","type":"Toolbar"}],"root_ids":["0a1e8f15-58b9-4b9b-9c81-1e2c6f35a9aa"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"b5624de9-4bd9-4805-9048-b612ee4a658e","roots":{"0a1e8f15-58b9-4b9b-9c81-1e2c6f35a9aa":"491bed0c-353e-4644-9f5f-7f3612e3459e"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();