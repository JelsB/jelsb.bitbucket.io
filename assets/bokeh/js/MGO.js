(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("643385d8-5e50-4aac-abef-a22282ccbcad");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '643385d8-5e50-4aac-abef-a22282ccbcad' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"2db3841c-0982-4a88-bbb2-2f51571e3f80":{"roots":{"references":[{"attributes":{"plot":null,"text":""},"id":"a8c2475f-a844-49d3-9e5c-6d4b8e20ad71","type":"Title"},{"attributes":{"formatter":{"id":"b50b8112-ab65-44ac-9bc4-fad063d70687","type":"BasicTickFormatter"},"plot":{"id":"a1e0051e-0b99-4e60-9b19-b83f60fc7808","subtype":"Figure","type":"Plot"},"ticker":{"id":"023a7579-87a6-46e8-9aaf-155cbdeb6707","type":"BasicTicker"},"visible":false},"id":"d5b13bea-0d41-43f0-8d71-fd0fc7ea1801","type":"LinearAxis"},{"attributes":{"plot":{"id":"a1e0051e-0b99-4e60-9b19-b83f60fc7808","subtype":"Figure","type":"Plot"},"ticker":{"id":"bfc188f4-6196-4c8e-bf0d-096986f54134","type":"BasicTicker"},"visible":false},"id":"dcd4ba8c-0930-4866-94d2-7dabf774be8a","type":"Grid"},{"attributes":{"children":[{"id":"f5d67c7f-c695-4d63-9789-e1a3ee3459d3","type":"Slider"}]},"id":"00f34193-be1b-4877-9ed7-bdb2ba676434","type":"WidgetBox"},{"attributes":{"formatter":{"id":"7d2be353-aace-4a42-bb9b-23a11641f18e","type":"BasicTickFormatter"},"plot":{"id":"a1e0051e-0b99-4e60-9b19-b83f60fc7808","subtype":"Figure","type":"Plot"},"ticker":{"id":"bfc188f4-6196-4c8e-bf0d-096986f54134","type":"BasicTicker"},"visible":false},"id":"e025af4b-6769-4076-928f-b1c491fbaa67","type":"LinearAxis"},{"attributes":{},"id":"7d2be353-aace-4a42-bb9b-23a11641f18e","type":"BasicTickFormatter"},{"attributes":{},"id":"21ce669e-3bd1-42fe-a187-3e0543bdf6cd","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MGO_000.png"],"url_num":["000"]},"selected":{"id":"f58007b5-553a-458f-9198-c50782592bbb","type":"Selection"},"selection_policy":{"id":"ec02658c-fb42-4597-bea1-14e403de5fbf","type":"UnionRenderers"}},"id":"3d0a85b7-46cf-424a-ae0b-de59bb6c51b0","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"34b6c103-8408-42d0-b709-ea711c5f83e3","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"3d0a85b7-46cf-424a-ae0b-de59bb6c51b0","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"479139cb-8be4-4707-8e13-70038c240fd6","type":"CustomJS"},{"attributes":{},"id":"df2d5156-5bc5-4e55-b823-84b1a392a076","type":"LinearScale"},{"attributes":{"callback":null},"id":"4489493f-1ac4-4b1d-91d4-5a6aa67ea84c","type":"Range1d"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"d8673756-8960-4d6b-a823-1fe2aa838624","type":"Toolbar"},{"attributes":{},"id":"ec02658c-fb42-4597-bea1-14e403de5fbf","type":"UnionRenderers"},{"attributes":{"dimension":1,"plot":{"id":"a1e0051e-0b99-4e60-9b19-b83f60fc7808","subtype":"Figure","type":"Plot"},"ticker":{"id":"023a7579-87a6-46e8-9aaf-155cbdeb6707","type":"BasicTicker"},"visible":false},"id":"cde1a1e1-624b-4e97-8788-cdaea58e1a45","type":"Grid"},{"attributes":{"callback":null},"id":"789dda07-9a2a-46d1-8375-d3aa791b60ed","type":"Range1d"},{"attributes":{"source":{"id":"3d0a85b7-46cf-424a-ae0b-de59bb6c51b0","type":"ColumnDataSource"}},"id":"cef5b5ba-527f-45ac-8071-7dd732603565","type":"CDSView"},{"attributes":{},"id":"f58007b5-553a-458f-9198-c50782592bbb","type":"Selection"},{"attributes":{"data_source":{"id":"3d0a85b7-46cf-424a-ae0b-de59bb6c51b0","type":"ColumnDataSource"},"glyph":{"id":"34b6c103-8408-42d0-b709-ea711c5f83e3","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"9f4605bc-a9b9-43aa-bf50-a2a303d8317c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"cef5b5ba-527f-45ac-8071-7dd732603565","type":"CDSView"}},"id":"15268739-2147-440b-bc28-370f12176860","type":"GlyphRenderer"},{"attributes":{},"id":"bfc188f4-6196-4c8e-bf0d-096986f54134","type":"BasicTicker"},{"attributes":{},"id":"b50b8112-ab65-44ac-9bc4-fad063d70687","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"9f4605bc-a9b9-43aa-bf50-a2a303d8317c","type":"ImageURL"},{"attributes":{"below":[{"id":"e025af4b-6769-4076-928f-b1c491fbaa67","type":"LinearAxis"}],"left":[{"id":"d5b13bea-0d41-43f0-8d71-fd0fc7ea1801","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"e025af4b-6769-4076-928f-b1c491fbaa67","type":"LinearAxis"},{"id":"dcd4ba8c-0930-4866-94d2-7dabf774be8a","type":"Grid"},{"id":"d5b13bea-0d41-43f0-8d71-fd0fc7ea1801","type":"LinearAxis"},{"id":"cde1a1e1-624b-4e97-8788-cdaea58e1a45","type":"Grid"},{"id":"15268739-2147-440b-bc28-370f12176860","type":"GlyphRenderer"}],"title":{"id":"a8c2475f-a844-49d3-9e5c-6d4b8e20ad71","type":"Title"},"toolbar":{"id":"d8673756-8960-4d6b-a823-1fe2aa838624","type":"Toolbar"},"x_range":{"id":"789dda07-9a2a-46d1-8375-d3aa791b60ed","type":"Range1d"},"x_scale":{"id":"21ce669e-3bd1-42fe-a187-3e0543bdf6cd","type":"LinearScale"},"y_range":{"id":"4489493f-1ac4-4b1d-91d4-5a6aa67ea84c","type":"Range1d"},"y_scale":{"id":"df2d5156-5bc5-4e55-b823-84b1a392a076","type":"LinearScale"}},"id":"a1e0051e-0b99-4e60-9b19-b83f60fc7808","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"00f34193-be1b-4877-9ed7-bdb2ba676434","type":"WidgetBox"},{"id":"a1e0051e-0b99-4e60-9b19-b83f60fc7808","subtype":"Figure","type":"Plot"}]},"id":"7015a626-85dc-4b7f-a453-53096c08ec6b","type":"Column"},{"attributes":{"callback":{"id":"479139cb-8be4-4707-8e13-70038c240fd6","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"f5d67c7f-c695-4d63-9789-e1a3ee3459d3","type":"Slider"},{"attributes":{},"id":"023a7579-87a6-46e8-9aaf-155cbdeb6707","type":"BasicTicker"}],"root_ids":["7015a626-85dc-4b7f-a453-53096c08ec6b"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"2db3841c-0982-4a88-bbb2-2f51571e3f80","roots":{"7015a626-85dc-4b7f-a453-53096c08ec6b":"643385d8-5e50-4aac-abef-a22282ccbcad"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();