(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("32ed46fa-5ea7-4375-9961-ad69502a8f0e");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '32ed46fa-5ea7-4375-9961-ad69502a8f0e' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"cc70ac30-3807-47a1-a71b-dcb923baf72b":{"roots":{"references":[{"attributes":{"source":{"id":"ccb23d56-609a-4996-b652-addf577ac43d","type":"ColumnDataSource"}},"id":"ebf74d34-b177-4cb2-9d56-3d2a178de528","type":"CDSView"},{"attributes":{"callback":{"id":"85d63ac7-32fb-4399-87b7-ac085cede970","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"2214f546-aa03-4a28-b42f-4c189a14ecab","type":"Slider"},{"attributes":{"formatter":{"id":"8e7de16a-487e-4d0c-b05d-2e78de6b098e","type":"BasicTickFormatter"},"plot":{"id":"6fd454b1-ac14-408f-a549-1f099e9d3bb2","subtype":"Figure","type":"Plot"},"ticker":{"id":"7ede14e8-65bc-4c67-9515-2173ca867c36","type":"BasicTicker"},"visible":false},"id":"81ba4e39-a3e3-44e7-a6ab-09c9a7d74c4b","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"ccb23d56-609a-4996-b652-addf577ac43d","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"85d63ac7-32fb-4399-87b7-ac085cede970","type":"CustomJS"},{"attributes":{"formatter":{"id":"53776667-fb0c-4b44-8b93-463ffa475a08","type":"BasicTickFormatter"},"plot":{"id":"6fd454b1-ac14-408f-a549-1f099e9d3bb2","subtype":"Figure","type":"Plot"},"ticker":{"id":"e982d5c9-e7b1-47f7-8d8f-51d36ae23288","type":"BasicTicker"},"visible":false},"id":"9b97a9cd-1647-40a9-863c-8f2977e01109","type":"LinearAxis"},{"attributes":{"plot":{"id":"6fd454b1-ac14-408f-a549-1f099e9d3bb2","subtype":"Figure","type":"Plot"},"ticker":{"id":"e982d5c9-e7b1-47f7-8d8f-51d36ae23288","type":"BasicTicker"},"visible":false},"id":"faf3ac6a-4230-4213-9a91-cc902ade7187","type":"Grid"},{"attributes":{"data_source":{"id":"ccb23d56-609a-4996-b652-addf577ac43d","type":"ColumnDataSource"},"glyph":{"id":"6ab8f735-4cc6-43c0-a754-c0c4617f63ab","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"f48b5f66-93be-4133-81af-320ad0f8282d","type":"ImageURL"},"selection_glyph":null,"view":{"id":"ebf74d34-b177-4cb2-9d56-3d2a178de528","type":"CDSView"}},"id":"7c4d0cc1-818d-402f-9aa3-5a852f2ca55f","type":"GlyphRenderer"},{"attributes":{"callback":null},"id":"012cc031-8abf-4efc-b8f2-a24b5f91223c","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"6fd454b1-ac14-408f-a549-1f099e9d3bb2","subtype":"Figure","type":"Plot"},"ticker":{"id":"7ede14e8-65bc-4c67-9515-2173ca867c36","type":"BasicTicker"},"visible":false},"id":"6d8a94c8-6f5b-466a-849c-99566ab0054a","type":"Grid"},{"attributes":{},"id":"27b2ce8f-6b7d-42fc-90ec-c725077294a3","type":"LinearScale"},{"attributes":{},"id":"85a3667b-4b2f-4eef-9218-c3b035b16713","type":"LinearScale"},{"attributes":{},"id":"53776667-fb0c-4b44-8b93-463ffa475a08","type":"BasicTickFormatter"},{"attributes":{},"id":"8e7de16a-487e-4d0c-b05d-2e78de6b098e","type":"BasicTickFormatter"},{"attributes":{},"id":"e982d5c9-e7b1-47f7-8d8f-51d36ae23288","type":"BasicTicker"},{"attributes":{},"id":"6116400d-83e1-4832-8a6d-1b68f8c1424d","type":"Selection"},{"attributes":{},"id":"e48618ee-89bb-4a04-8744-29a648eff838","type":"UnionRenderers"},{"attributes":{"plot":null,"text":""},"id":"908dfba1-399c-4352-ace6-6ce203e71bf4","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"39761852-3720-430f-9b80-9592eb6e6d55","type":"Toolbar"},{"attributes":{"children":[{"id":"2214f546-aa03-4a28-b42f-4c189a14ecab","type":"Slider"}]},"id":"43b7f541-a9cd-4d7a-97f8-f576186ba71c","type":"WidgetBox"},{"attributes":{},"id":"7ede14e8-65bc-4c67-9515-2173ca867c36","type":"BasicTicker"},{"attributes":{"callback":null},"id":"6b58d258-3347-40c1-ad83-871d01a77155","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"6ab8f735-4cc6-43c0-a754-c0c4617f63ab","type":"ImageURL"},{"attributes":{"children":[{"id":"43b7f541-a9cd-4d7a-97f8-f576186ba71c","type":"WidgetBox"},{"id":"6fd454b1-ac14-408f-a549-1f099e9d3bb2","subtype":"Figure","type":"Plot"}]},"id":"349def24-b502-4f38-afbd-4526d8a297e4","type":"Column"},{"attributes":{"below":[{"id":"9b97a9cd-1647-40a9-863c-8f2977e01109","type":"LinearAxis"}],"left":[{"id":"81ba4e39-a3e3-44e7-a6ab-09c9a7d74c4b","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"9b97a9cd-1647-40a9-863c-8f2977e01109","type":"LinearAxis"},{"id":"faf3ac6a-4230-4213-9a91-cc902ade7187","type":"Grid"},{"id":"81ba4e39-a3e3-44e7-a6ab-09c9a7d74c4b","type":"LinearAxis"},{"id":"6d8a94c8-6f5b-466a-849c-99566ab0054a","type":"Grid"},{"id":"7c4d0cc1-818d-402f-9aa3-5a852f2ca55f","type":"GlyphRenderer"}],"title":{"id":"908dfba1-399c-4352-ace6-6ce203e71bf4","type":"Title"},"toolbar":{"id":"39761852-3720-430f-9b80-9592eb6e6d55","type":"Toolbar"},"x_range":{"id":"6b58d258-3347-40c1-ad83-871d01a77155","type":"Range1d"},"x_scale":{"id":"27b2ce8f-6b7d-42fc-90ec-c725077294a3","type":"LinearScale"},"y_range":{"id":"012cc031-8abf-4efc-b8f2-a24b5f91223c","type":"Range1d"},"y_scale":{"id":"85a3667b-4b2f-4eef-9218-c3b035b16713","type":"LinearScale"}},"id":"6fd454b1-ac14-408f-a549-1f099e9d3bb2","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"f48b5f66-93be-4133-81af-320ad0f8282d","type":"ImageURL"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL6O9_000.png"],"url_num":["000"]},"selected":{"id":"6116400d-83e1-4832-8a6d-1b68f8c1424d","type":"Selection"},"selection_policy":{"id":"e48618ee-89bb-4a04-8744-29a648eff838","type":"UnionRenderers"}},"id":"ccb23d56-609a-4996-b652-addf577ac43d","type":"ColumnDataSource"}],"root_ids":["349def24-b502-4f38-afbd-4526d8a297e4"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"cc70ac30-3807-47a1-a71b-dcb923baf72b","roots":{"349def24-b502-4f38-afbd-4526d8a297e4":"32ed46fa-5ea7-4375-9961-ad69502a8f0e"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();