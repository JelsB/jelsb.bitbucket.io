(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("4cf444cf-a3c4-42b2-af77-89d01f1d65e7");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '4cf444cf-a3c4-42b2-af77-89d01f1d65e7' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"316b50cd-0ec2-4069-aed4-aacda8b0428c":{"roots":{"references":[{"attributes":{"args":{"source":{"id":"52e59387-d016-45a7-87dc-641501db318f","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"95ee538a-d22a-44c3-9958-43b3c569db32","type":"CustomJS"},{"attributes":{},"id":"a1484624-70b3-4d2c-b272-074d77eee12f","type":"LinearScale"},{"attributes":{},"id":"684dda66-4e5b-4606-9c21-09ede57acaaf","type":"BasicTickFormatter"},{"attributes":{"plot":null,"text":""},"id":"ae7d05d0-4526-4227-aa74-8640a5743deb","type":"Title"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"ec7aa9eb-9754-4caf-b20f-19b5986612cf","type":"ImageURL"},{"attributes":{},"id":"b840e031-5c6e-48e5-86b9-959d2bad352b","type":"BasicTicker"},{"attributes":{},"id":"30d74d17-5cad-411b-aedd-6cee186c3384","type":"Selection"},{"attributes":{"callback":{"id":"95ee538a-d22a-44c3-9958-43b3c569db32","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"5df9ee2d-1ee8-421a-9a77-45c2abb47454","type":"Slider"},{"attributes":{"formatter":{"id":"684dda66-4e5b-4606-9c21-09ede57acaaf","type":"BasicTickFormatter"},"plot":{"id":"17184b5e-459a-4776-bff5-7b3cddf23f65","subtype":"Figure","type":"Plot"},"ticker":{"id":"870f033c-4d93-4bf3-ad48-3ea0fb21b3b4","type":"BasicTicker"},"visible":false},"id":"0cd8ac66-1216-46e7-9f3d-a527aebe36fb","type":"LinearAxis"},{"attributes":{},"id":"870f033c-4d93-4bf3-ad48-3ea0fb21b3b4","type":"BasicTicker"},{"attributes":{"children":[{"id":"5df9ee2d-1ee8-421a-9a77-45c2abb47454","type":"Slider"}]},"id":"2ff126be-51cd-4ebc-8769-0fc8d17ed67e","type":"WidgetBox"},{"attributes":{"data_source":{"id":"52e59387-d016-45a7-87dc-641501db318f","type":"ColumnDataSource"},"glyph":{"id":"ec7aa9eb-9754-4caf-b20f-19b5986612cf","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c516c775-1413-4b63-9b25-e3ad145ba28c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"27138593-0416-4b2b-b7f4-196c1fd020b8","type":"CDSView"}},"id":"4b62507d-79c7-45cf-9316-41842435576d","type":"GlyphRenderer"},{"attributes":{"callback":null},"id":"4c36d984-f39a-4b8c-a903-0b2020ae3b4b","type":"Range1d"},{"attributes":{},"id":"a724b530-b5f3-4348-8348-cb67ac670177","type":"UnionRenderers"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI7O14_000.png"],"url_num":["000"]},"selected":{"id":"30d74d17-5cad-411b-aedd-6cee186c3384","type":"Selection"},"selection_policy":{"id":"a724b530-b5f3-4348-8348-cb67ac670177","type":"UnionRenderers"}},"id":"52e59387-d016-45a7-87dc-641501db318f","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"0cd8ac66-1216-46e7-9f3d-a527aebe36fb","type":"LinearAxis"}],"left":[{"id":"c12c7abd-4dd3-43d3-b37d-48a964438ca8","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"0cd8ac66-1216-46e7-9f3d-a527aebe36fb","type":"LinearAxis"},{"id":"efb55921-d9cb-4546-8343-3e1ea34ae466","type":"Grid"},{"id":"c12c7abd-4dd3-43d3-b37d-48a964438ca8","type":"LinearAxis"},{"id":"4c337730-d8d6-4adc-9566-6a84c071436f","type":"Grid"},{"id":"4b62507d-79c7-45cf-9316-41842435576d","type":"GlyphRenderer"}],"title":{"id":"ae7d05d0-4526-4227-aa74-8640a5743deb","type":"Title"},"toolbar":{"id":"c9d705f8-3dfd-4287-af76-2532f0427df6","type":"Toolbar"},"x_range":{"id":"4c36d984-f39a-4b8c-a903-0b2020ae3b4b","type":"Range1d"},"x_scale":{"id":"1f63b417-a221-4838-b8df-eab9429014ab","type":"LinearScale"},"y_range":{"id":"9763c856-c208-439e-8162-685e01683f86","type":"Range1d"},"y_scale":{"id":"a1484624-70b3-4d2c-b272-074d77eee12f","type":"LinearScale"}},"id":"17184b5e-459a-4776-bff5-7b3cddf23f65","subtype":"Figure","type":"Plot"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"c9d705f8-3dfd-4287-af76-2532f0427df6","type":"Toolbar"},{"attributes":{"callback":null},"id":"9763c856-c208-439e-8162-685e01683f86","type":"Range1d"},{"attributes":{"plot":{"id":"17184b5e-459a-4776-bff5-7b3cddf23f65","subtype":"Figure","type":"Plot"},"ticker":{"id":"870f033c-4d93-4bf3-ad48-3ea0fb21b3b4","type":"BasicTicker"},"visible":false},"id":"efb55921-d9cb-4546-8343-3e1ea34ae466","type":"Grid"},{"attributes":{"children":[{"id":"2ff126be-51cd-4ebc-8769-0fc8d17ed67e","type":"WidgetBox"},{"id":"17184b5e-459a-4776-bff5-7b3cddf23f65","subtype":"Figure","type":"Plot"}]},"id":"280588a4-bb95-4a30-805e-ce91bd02edae","type":"Column"},{"attributes":{"dimension":1,"plot":{"id":"17184b5e-459a-4776-bff5-7b3cddf23f65","subtype":"Figure","type":"Plot"},"ticker":{"id":"b840e031-5c6e-48e5-86b9-959d2bad352b","type":"BasicTicker"},"visible":false},"id":"4c337730-d8d6-4adc-9566-6a84c071436f","type":"Grid"},{"attributes":{},"id":"e225ee39-8a98-4002-a181-6d976316fea3","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"e225ee39-8a98-4002-a181-6d976316fea3","type":"BasicTickFormatter"},"plot":{"id":"17184b5e-459a-4776-bff5-7b3cddf23f65","subtype":"Figure","type":"Plot"},"ticker":{"id":"b840e031-5c6e-48e5-86b9-959d2bad352b","type":"BasicTicker"},"visible":false},"id":"c12c7abd-4dd3-43d3-b37d-48a964438ca8","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c516c775-1413-4b63-9b25-e3ad145ba28c","type":"ImageURL"},{"attributes":{"source":{"id":"52e59387-d016-45a7-87dc-641501db318f","type":"ColumnDataSource"}},"id":"27138593-0416-4b2b-b7f4-196c1fd020b8","type":"CDSView"},{"attributes":{},"id":"1f63b417-a221-4838-b8df-eab9429014ab","type":"LinearScale"}],"root_ids":["280588a4-bb95-4a30-805e-ce91bd02edae"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"316b50cd-0ec2-4069-aed4-aacda8b0428c","roots":{"280588a4-bb95-4a30-805e-ce91bd02edae":"4cf444cf-a3c4-42b2-af77-89d01f1d65e7"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();