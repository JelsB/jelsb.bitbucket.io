(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("32d77bff-2549-4482-a8c0-8060095e7728");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '32d77bff-2549-4482-a8c0-8060095e7728' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"0881af42-c542-4c06-aab0-80ccfee10248":{"roots":{"references":[{"attributes":{"callback":null},"id":"61c7878f-3528-43e9-8e29-63b3a5c63a18","type":"Range1d"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"88ef274a-db5e-4022-ad30-69b300364c66","type":"ImageURL"},{"attributes":{"children":[{"id":"bdab7f1c-3d73-40d4-923a-fcf9be416aa8","type":"WidgetBox"},{"id":"79fcd52a-45c2-46e6-b620-0ef0234efd50","subtype":"Figure","type":"Plot"}]},"id":"a73eadff-ae6a-478e-b979-83cb7ddb153f","type":"Column"},{"attributes":{"children":[{"id":"48d21b22-fce2-4d39-bc10-62d27ea4a673","type":"Slider"}]},"id":"bdab7f1c-3d73-40d4-923a-fcf9be416aa8","type":"WidgetBox"},{"attributes":{"callback":null},"id":"f9f8144f-27fe-41fe-b530-8c47b3218061","type":"Range1d"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TIO2_000.png"],"url_num":["000"]},"selected":{"id":"05607926-520b-49df-9af8-77b37a0d0d11","type":"Selection"},"selection_policy":{"id":"77114c07-122e-445d-a0d0-d4ebe06f245c","type":"UnionRenderers"}},"id":"b2e121cc-28e6-47bb-adaf-9f38e3227128","type":"ColumnDataSource"},{"attributes":{},"id":"ca8a086a-f3cb-44d6-9846-398f29792770","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"a5f0ae4b-0ba1-485c-9ca3-077e2910ba61","type":"ImageURL"},{"attributes":{},"id":"3ffc105e-5ea6-462b-898d-090d888c280b","type":"LinearScale"},{"attributes":{"formatter":{"id":"35ff89dd-8899-4ca4-b984-41e0761606cf","type":"BasicTickFormatter"},"plot":{"id":"79fcd52a-45c2-46e6-b620-0ef0234efd50","subtype":"Figure","type":"Plot"},"ticker":{"id":"b007b467-03be-4b52-b235-65ab00979abf","type":"BasicTicker"},"visible":false},"id":"962023f2-f3a8-45d5-8fcc-d1920585fbb7","type":"LinearAxis"},{"attributes":{"below":[{"id":"962023f2-f3a8-45d5-8fcc-d1920585fbb7","type":"LinearAxis"}],"left":[{"id":"a9c5c215-a3d6-4be1-b94f-5fee273fbf91","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"962023f2-f3a8-45d5-8fcc-d1920585fbb7","type":"LinearAxis"},{"id":"97a503b5-e743-4efc-95c6-6c6015dcadac","type":"Grid"},{"id":"a9c5c215-a3d6-4be1-b94f-5fee273fbf91","type":"LinearAxis"},{"id":"00cc690b-ed96-4adf-afe7-62bf9424ddc5","type":"Grid"},{"id":"64937826-6734-4137-ab79-e0d6ca7e74ac","type":"GlyphRenderer"}],"title":{"id":"420a2f92-6c59-4090-993d-d53e1375291f","type":"Title"},"toolbar":{"id":"4d1d6e1a-52b9-4dc5-b3f8-ee39451ecb7c","type":"Toolbar"},"x_range":{"id":"61c7878f-3528-43e9-8e29-63b3a5c63a18","type":"Range1d"},"x_scale":{"id":"ca8a086a-f3cb-44d6-9846-398f29792770","type":"LinearScale"},"y_range":{"id":"f9f8144f-27fe-41fe-b530-8c47b3218061","type":"Range1d"},"y_scale":{"id":"3ffc105e-5ea6-462b-898d-090d888c280b","type":"LinearScale"}},"id":"79fcd52a-45c2-46e6-b620-0ef0234efd50","subtype":"Figure","type":"Plot"},{"attributes":{"plot":null,"text":""},"id":"420a2f92-6c59-4090-993d-d53e1375291f","type":"Title"},{"attributes":{},"id":"b007b467-03be-4b52-b235-65ab00979abf","type":"BasicTicker"},{"attributes":{},"id":"521753f5-4740-472c-8970-7ccd26df46fd","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"79fcd52a-45c2-46e6-b620-0ef0234efd50","subtype":"Figure","type":"Plot"},"ticker":{"id":"b007b467-03be-4b52-b235-65ab00979abf","type":"BasicTicker"},"visible":false},"id":"97a503b5-e743-4efc-95c6-6c6015dcadac","type":"Grid"},{"attributes":{"callback":{"id":"ca3ccb1f-75c0-4557-b87b-40ccc4c1c17b","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"48d21b22-fce2-4d39-bc10-62d27ea4a673","type":"Slider"},{"attributes":{},"id":"35ff89dd-8899-4ca4-b984-41e0761606cf","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"521753f5-4740-472c-8970-7ccd26df46fd","type":"BasicTickFormatter"},"plot":{"id":"79fcd52a-45c2-46e6-b620-0ef0234efd50","subtype":"Figure","type":"Plot"},"ticker":{"id":"0be1262a-49eb-4cda-8971-139059d2a998","type":"BasicTicker"},"visible":false},"id":"a9c5c215-a3d6-4be1-b94f-5fee273fbf91","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"4d1d6e1a-52b9-4dc5-b3f8-ee39451ecb7c","type":"Toolbar"},{"attributes":{},"id":"77114c07-122e-445d-a0d0-d4ebe06f245c","type":"UnionRenderers"},{"attributes":{},"id":"0be1262a-49eb-4cda-8971-139059d2a998","type":"BasicTicker"},{"attributes":{"source":{"id":"b2e121cc-28e6-47bb-adaf-9f38e3227128","type":"ColumnDataSource"}},"id":"a5c51a11-67a7-43a3-bb26-dd227f150741","type":"CDSView"},{"attributes":{"dimension":1,"plot":{"id":"79fcd52a-45c2-46e6-b620-0ef0234efd50","subtype":"Figure","type":"Plot"},"ticker":{"id":"0be1262a-49eb-4cda-8971-139059d2a998","type":"BasicTicker"},"visible":false},"id":"00cc690b-ed96-4adf-afe7-62bf9424ddc5","type":"Grid"},{"attributes":{},"id":"05607926-520b-49df-9af8-77b37a0d0d11","type":"Selection"},{"attributes":{"data_source":{"id":"b2e121cc-28e6-47bb-adaf-9f38e3227128","type":"ColumnDataSource"},"glyph":{"id":"a5f0ae4b-0ba1-485c-9ca3-077e2910ba61","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"88ef274a-db5e-4022-ad30-69b300364c66","type":"ImageURL"},"selection_glyph":null,"view":{"id":"a5c51a11-67a7-43a3-bb26-dd227f150741","type":"CDSView"}},"id":"64937826-6734-4137-ab79-e0d6ca7e74ac","type":"GlyphRenderer"},{"attributes":{"args":{"source":{"id":"b2e121cc-28e6-47bb-adaf-9f38e3227128","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"ca3ccb1f-75c0-4557-b87b-40ccc4c1c17b","type":"CustomJS"}],"root_ids":["a73eadff-ae6a-478e-b979-83cb7ddb153f"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"0881af42-c542-4c06-aab0-80ccfee10248","roots":{"a73eadff-ae6a-478e-b979-83cb7ddb153f":"32d77bff-2549-4482-a8c0-8060095e7728"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();