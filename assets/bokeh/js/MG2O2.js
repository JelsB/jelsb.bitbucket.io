(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("6f84a0fd-4e96-42cd-b468-22e43e32ac03");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '6f84a0fd-4e96-42cd-b468-22e43e32ac03' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"8941e6ba-7f9a-4215-8904-c01812275b45":{"roots":{"references":[{"attributes":{},"id":"2a888cb8-9319-4b73-982e-e46156c7166d","type":"BasicTickFormatter"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG2O2_000.png"],"url_num":["000"]},"selected":{"id":"f9bc5363-d669-479c-8fa0-9e4ca0890e9e","type":"Selection"},"selection_policy":{"id":"a2f5aadc-7428-4054-91e0-8a3d440524e6","type":"UnionRenderers"}},"id":"ee92ff4c-e27c-481a-9565-18b65486397e","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"6e025740-1c29-40d7-ac14-788b72e01d1e","type":"LinearAxis"}],"left":[{"id":"f8c675d7-2f42-4f31-9a2a-e1cacb337a4f","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"6e025740-1c29-40d7-ac14-788b72e01d1e","type":"LinearAxis"},{"id":"b5a9e099-02cc-44b3-84dd-9d714ccd6712","type":"Grid"},{"id":"f8c675d7-2f42-4f31-9a2a-e1cacb337a4f","type":"LinearAxis"},{"id":"26f9b244-470c-4c14-9645-99d92ba976a4","type":"Grid"},{"id":"e16d563c-7163-4390-b3aa-61d3d35c7522","type":"GlyphRenderer"}],"title":{"id":"e7ce88d9-44c2-466f-a0e7-e3548a61fdff","type":"Title"},"toolbar":{"id":"c886a2f8-e0a7-43d0-8e06-6f901b0ce3ef","type":"Toolbar"},"x_range":{"id":"0861f748-2081-4de7-a5d7-46e80bb4d0b7","type":"Range1d"},"x_scale":{"id":"e41d4cc6-c070-4928-8a74-5b4b2d160b09","type":"LinearScale"},"y_range":{"id":"a3061588-9bdd-4608-a252-63a983734824","type":"Range1d"},"y_scale":{"id":"28963aa2-da4a-45e7-a43b-2f32d4106af3","type":"LinearScale"}},"id":"451e7631-bcfb-4363-a481-6348ebdfec73","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"0861f748-2081-4de7-a5d7-46e80bb4d0b7","type":"Range1d"},{"attributes":{},"id":"e41d4cc6-c070-4928-8a74-5b4b2d160b09","type":"LinearScale"},{"attributes":{},"id":"a2f5aadc-7428-4054-91e0-8a3d440524e6","type":"UnionRenderers"},{"attributes":{"plot":{"id":"451e7631-bcfb-4363-a481-6348ebdfec73","subtype":"Figure","type":"Plot"},"ticker":{"id":"80f875d7-4ba4-4881-82fc-af79753f0d47","type":"BasicTicker"},"visible":false},"id":"b5a9e099-02cc-44b3-84dd-9d714ccd6712","type":"Grid"},{"attributes":{"formatter":{"id":"2a888cb8-9319-4b73-982e-e46156c7166d","type":"BasicTickFormatter"},"plot":{"id":"451e7631-bcfb-4363-a481-6348ebdfec73","subtype":"Figure","type":"Plot"},"ticker":{"id":"80f875d7-4ba4-4881-82fc-af79753f0d47","type":"BasicTicker"},"visible":false},"id":"6e025740-1c29-40d7-ac14-788b72e01d1e","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"46c84d14-6559-4bdd-86e4-266fb2b923cc","type":"ImageURL"},{"attributes":{"dimension":1,"plot":{"id":"451e7631-bcfb-4363-a481-6348ebdfec73","subtype":"Figure","type":"Plot"},"ticker":{"id":"0fa9d52c-4c97-4ce2-828c-6daadc752487","type":"BasicTicker"},"visible":false},"id":"26f9b244-470c-4c14-9645-99d92ba976a4","type":"Grid"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"c886a2f8-e0a7-43d0-8e06-6f901b0ce3ef","type":"Toolbar"},{"attributes":{},"id":"f9bc5363-d669-479c-8fa0-9e4ca0890e9e","type":"Selection"},{"attributes":{},"id":"66866ec4-d62d-4dd8-b234-3b851b1afb39","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"a3061588-9bdd-4608-a252-63a983734824","type":"Range1d"},{"attributes":{"source":{"id":"ee92ff4c-e27c-481a-9565-18b65486397e","type":"ColumnDataSource"}},"id":"ae06833a-3ac0-47b6-a6cd-3b1ee0b57cad","type":"CDSView"},{"attributes":{"callback":{"id":"f4774a4b-fa90-4052-931a-b22b5bd78ab5","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"a3b6faa0-f376-4999-b825-2b7dc7fc8a0a","type":"Slider"},{"attributes":{"children":[{"id":"cfe606bf-55b5-4636-ab0c-6610310d9151","type":"WidgetBox"},{"id":"451e7631-bcfb-4363-a481-6348ebdfec73","subtype":"Figure","type":"Plot"}]},"id":"8c4eff94-c907-4887-96fd-34a96331ef50","type":"Column"},{"attributes":{},"id":"28963aa2-da4a-45e7-a43b-2f32d4106af3","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"cf1a7fa0-1313-4220-90a6-28e4785a6378","type":"ImageURL"},{"attributes":{"plot":null,"text":""},"id":"e7ce88d9-44c2-466f-a0e7-e3548a61fdff","type":"Title"},{"attributes":{},"id":"80f875d7-4ba4-4881-82fc-af79753f0d47","type":"BasicTicker"},{"attributes":{"formatter":{"id":"66866ec4-d62d-4dd8-b234-3b851b1afb39","type":"BasicTickFormatter"},"plot":{"id":"451e7631-bcfb-4363-a481-6348ebdfec73","subtype":"Figure","type":"Plot"},"ticker":{"id":"0fa9d52c-4c97-4ce2-828c-6daadc752487","type":"BasicTicker"},"visible":false},"id":"f8c675d7-2f42-4f31-9a2a-e1cacb337a4f","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"ee92ff4c-e27c-481a-9565-18b65486397e","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"f4774a4b-fa90-4052-931a-b22b5bd78ab5","type":"CustomJS"},{"attributes":{},"id":"0fa9d52c-4c97-4ce2-828c-6daadc752487","type":"BasicTicker"},{"attributes":{"data_source":{"id":"ee92ff4c-e27c-481a-9565-18b65486397e","type":"ColumnDataSource"},"glyph":{"id":"cf1a7fa0-1313-4220-90a6-28e4785a6378","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"46c84d14-6559-4bdd-86e4-266fb2b923cc","type":"ImageURL"},"selection_glyph":null,"view":{"id":"ae06833a-3ac0-47b6-a6cd-3b1ee0b57cad","type":"CDSView"}},"id":"e16d563c-7163-4390-b3aa-61d3d35c7522","type":"GlyphRenderer"},{"attributes":{"children":[{"id":"a3b6faa0-f376-4999-b825-2b7dc7fc8a0a","type":"Slider"}]},"id":"cfe606bf-55b5-4636-ab0c-6610310d9151","type":"WidgetBox"}],"root_ids":["8c4eff94-c907-4887-96fd-34a96331ef50"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"8941e6ba-7f9a-4215-8904-c01812275b45","roots":{"8c4eff94-c907-4887-96fd-34a96331ef50":"6f84a0fd-4e96-42cd-b468-22e43e32ac03"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();