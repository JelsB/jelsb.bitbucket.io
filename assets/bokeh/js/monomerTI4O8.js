(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("7ac19b2e-5d05-4a05-bf42-fb8a092e5465");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '7ac19b2e-5d05-4a05-bf42-fb8a092e5465' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"9025d8bc-e745-49cd-bd9e-6a4e1a3bfa0e":{"roots":{"references":[{"attributes":{},"id":"6dbbd002-ff11-47d7-8285-c93bac78c37d","type":"UnionRenderers"},{"attributes":{},"id":"72c7b195-8484-4d74-9244-28f04b457d44","type":"BasicTickFormatter"},{"attributes":{"source":{"id":"095559c8-6f34-4657-8fd9-4ad1759cdcaf","type":"ColumnDataSource"}},"id":"62bc2b1e-f938-46eb-b514-ee3bb9d59f97","type":"CDSView"},{"attributes":{"plot":null,"text":""},"id":"b51776b0-d85d-47ed-9366-9b429a9bf87d","type":"Title"},{"attributes":{},"id":"3f590b0a-70ab-4ab4-8ba4-a5918eaeeabd","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"71807e62-6aed-4f26-b063-e0e1163f0df5","type":"Range1d"},{"attributes":{},"id":"93df4579-503f-4e94-85ae-43bf90d0e993","type":"LinearScale"},{"attributes":{"args":{"source":{"id":"095559c8-6f34-4657-8fd9-4ad1759cdcaf","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"96d23e3c-e2d1-4579-a336-d3ad23b47295","type":"CustomJS"},{"attributes":{},"id":"2323ccc9-ee3d-4683-82cf-a666ffb78189","type":"BasicTicker"},{"attributes":{"data_source":{"id":"095559c8-6f34-4657-8fd9-4ad1759cdcaf","type":"ColumnDataSource"},"glyph":{"id":"b87f3a52-5dce-4e89-82e4-5eba02713a7c","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"967035bd-e6d6-4f7a-9ecd-f261a63bd099","type":"ImageURL"},"selection_glyph":null,"view":{"id":"62bc2b1e-f938-46eb-b514-ee3bb9d59f97","type":"CDSView"}},"id":"6083b167-b01d-4216-b41a-718b9ac64173","type":"GlyphRenderer"},{"attributes":{"formatter":{"id":"72c7b195-8484-4d74-9244-28f04b457d44","type":"BasicTickFormatter"},"plot":{"id":"50fc428d-1724-4ca4-b8a8-2cd9a92ede75","subtype":"Figure","type":"Plot"},"ticker":{"id":"2323ccc9-ee3d-4683-82cf-a666ffb78189","type":"BasicTicker"},"visible":false},"id":"6b8986ce-79f8-4e1d-931f-757afe3964e7","type":"LinearAxis"},{"attributes":{"plot":{"id":"50fc428d-1724-4ca4-b8a8-2cd9a92ede75","subtype":"Figure","type":"Plot"},"ticker":{"id":"777b945f-1fec-4565-b42b-2b764eb901da","type":"BasicTicker"},"visible":false},"id":"eab877a5-54ae-45cf-8de9-5ee1e18d894f","type":"Grid"},{"attributes":{},"id":"259e051e-e88c-49cb-aeec-812fabe54c62","type":"Selection"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI4O8_000.png"],"url_num":["000"]},"selected":{"id":"259e051e-e88c-49cb-aeec-812fabe54c62","type":"Selection"},"selection_policy":{"id":"6dbbd002-ff11-47d7-8285-c93bac78c37d","type":"UnionRenderers"}},"id":"095559c8-6f34-4657-8fd9-4ad1759cdcaf","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"b87f3a52-5dce-4e89-82e4-5eba02713a7c","type":"ImageURL"},{"attributes":{"callback":null},"id":"6e493c9a-6ef9-4118-a375-2a08f793362c","type":"Range1d"},{"attributes":{"callback":{"id":"96d23e3c-e2d1-4579-a336-d3ad23b47295","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"0725019b-2d58-498b-b77b-840578af3e52","type":"Slider"},{"attributes":{},"id":"418086ea-bbcf-4989-bf56-793077d27934","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"967035bd-e6d6-4f7a-9ecd-f261a63bd099","type":"ImageURL"},{"attributes":{"children":[{"id":"5b899e73-c183-4b98-bf4b-8e7552f0302f","type":"WidgetBox"},{"id":"50fc428d-1724-4ca4-b8a8-2cd9a92ede75","subtype":"Figure","type":"Plot"}]},"id":"7b8738b0-0669-4df4-b957-c2316ac594ae","type":"Column"},{"attributes":{},"id":"777b945f-1fec-4565-b42b-2b764eb901da","type":"BasicTicker"},{"attributes":{"below":[{"id":"2cec34b1-11cd-4129-b170-a99aabdb637e","type":"LinearAxis"}],"left":[{"id":"6b8986ce-79f8-4e1d-931f-757afe3964e7","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"2cec34b1-11cd-4129-b170-a99aabdb637e","type":"LinearAxis"},{"id":"eab877a5-54ae-45cf-8de9-5ee1e18d894f","type":"Grid"},{"id":"6b8986ce-79f8-4e1d-931f-757afe3964e7","type":"LinearAxis"},{"id":"77f67f82-e10c-441e-90ea-6924769a88c8","type":"Grid"},{"id":"6083b167-b01d-4216-b41a-718b9ac64173","type":"GlyphRenderer"}],"title":{"id":"b51776b0-d85d-47ed-9366-9b429a9bf87d","type":"Title"},"toolbar":{"id":"86eb709b-2ec7-4acd-8278-4a35d4818574","type":"Toolbar"},"x_range":{"id":"71807e62-6aed-4f26-b063-e0e1163f0df5","type":"Range1d"},"x_scale":{"id":"418086ea-bbcf-4989-bf56-793077d27934","type":"LinearScale"},"y_range":{"id":"6e493c9a-6ef9-4118-a375-2a08f793362c","type":"Range1d"},"y_scale":{"id":"93df4579-503f-4e94-85ae-43bf90d0e993","type":"LinearScale"}},"id":"50fc428d-1724-4ca4-b8a8-2cd9a92ede75","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"3f590b0a-70ab-4ab4-8ba4-a5918eaeeabd","type":"BasicTickFormatter"},"plot":{"id":"50fc428d-1724-4ca4-b8a8-2cd9a92ede75","subtype":"Figure","type":"Plot"},"ticker":{"id":"777b945f-1fec-4565-b42b-2b764eb901da","type":"BasicTicker"},"visible":false},"id":"2cec34b1-11cd-4129-b170-a99aabdb637e","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"86eb709b-2ec7-4acd-8278-4a35d4818574","type":"Toolbar"},{"attributes":{"children":[{"id":"0725019b-2d58-498b-b77b-840578af3e52","type":"Slider"}]},"id":"5b899e73-c183-4b98-bf4b-8e7552f0302f","type":"WidgetBox"},{"attributes":{"dimension":1,"plot":{"id":"50fc428d-1724-4ca4-b8a8-2cd9a92ede75","subtype":"Figure","type":"Plot"},"ticker":{"id":"2323ccc9-ee3d-4683-82cf-a666ffb78189","type":"BasicTicker"},"visible":false},"id":"77f67f82-e10c-441e-90ea-6924769a88c8","type":"Grid"}],"root_ids":["7b8738b0-0669-4df4-b957-c2316ac594ae"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"9025d8bc-e745-49cd-bd9e-6a4e1a3bfa0e","roots":{"7b8738b0-0669-4df4-b957-c2316ac594ae":"7ac19b2e-5d05-4a05-bf42-fb8a092e5465"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();