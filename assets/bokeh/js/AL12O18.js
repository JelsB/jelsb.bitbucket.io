(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("02e388af-9817-4637-a467-ddc6eafc8342");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '02e388af-9817-4637-a467-ddc6eafc8342' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"36062490-928c-4fb1-9e68-44abb80857c5":{"roots":{"references":[{"attributes":{},"id":"f85f9f3d-224e-4631-ac72-65001980a5a3","type":"BasicTicker"},{"attributes":{"args":{"source":{"id":"e5dff90d-2d43-4f26-9429-03456cec97be","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"dc77cd46-fa13-4ee5-8b46-0c4191e4caca","type":"CustomJS"},{"attributes":{"data_source":{"id":"e5dff90d-2d43-4f26-9429-03456cec97be","type":"ColumnDataSource"},"glyph":{"id":"660ca285-700e-41c1-8fed-b0edc90c669c","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"63651b76-38a7-46a2-b8ca-03196662df11","type":"ImageURL"},"selection_glyph":null,"view":{"id":"1e84e4b8-3d0c-4760-9d4f-4551a2f63031","type":"CDSView"}},"id":"4a620723-ca75-40d0-a081-ad59f5c680e7","type":"GlyphRenderer"},{"attributes":{"source":{"id":"e5dff90d-2d43-4f26-9429-03456cec97be","type":"ColumnDataSource"}},"id":"1e84e4b8-3d0c-4760-9d4f-4551a2f63031","type":"CDSView"},{"attributes":{},"id":"e50da510-f41f-4d2c-927c-22e9fa822833","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"6b681efa-af81-4ca3-a4c1-7693b372c943","type":"Slider"}]},"id":"d5bf1a58-6075-4bc6-9b5a-9ff392cea8d8","type":"WidgetBox"},{"attributes":{"below":[{"id":"87a7fbbf-43c8-4aaa-986e-0eb5e9865cd1","type":"LinearAxis"}],"left":[{"id":"aada50b6-d5a4-4151-b585-00d89e7e6499","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"87a7fbbf-43c8-4aaa-986e-0eb5e9865cd1","type":"LinearAxis"},{"id":"dd4d164d-02c4-4e63-99d6-f4d884be9cd0","type":"Grid"},{"id":"aada50b6-d5a4-4151-b585-00d89e7e6499","type":"LinearAxis"},{"id":"63285bea-bbd5-450d-983d-194e48cf2844","type":"Grid"},{"id":"4a620723-ca75-40d0-a081-ad59f5c680e7","type":"GlyphRenderer"}],"title":{"id":"399fee0c-173d-472b-b994-b1066039e8bf","type":"Title"},"toolbar":{"id":"d35686bc-be4d-482c-886d-caaa576db1f8","type":"Toolbar"},"x_range":{"id":"510b24fc-b6b1-40a0-970c-a65ed524e7af","type":"Range1d"},"x_scale":{"id":"a97340d9-c0f2-41e7-8ff4-093238b5df2b","type":"LinearScale"},"y_range":{"id":"038b006b-3c8e-4abd-9e71-7eac12596453","type":"Range1d"},"y_scale":{"id":"fa23c0a1-3242-4019-824e-0d100ef4a066","type":"LinearScale"}},"id":"265104d2-3ad3-425c-8bba-ef68e3402185","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"510b24fc-b6b1-40a0-970c-a65ed524e7af","type":"Range1d"},{"attributes":{},"id":"c6d232dc-4abf-4b96-8b32-530f6a215cc7","type":"Selection"},{"attributes":{"plot":null,"text":""},"id":"399fee0c-173d-472b-b994-b1066039e8bf","type":"Title"},{"attributes":{},"id":"82279a7d-8192-4f4a-a915-4caf98bf9d8b","type":"BasicTickFormatter"},{"attributes":{},"id":"ca5aa5b1-5f31-4809-8e62-bd97c0b3ef42","type":"BasicTicker"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/AL12O18_000.png"],"url_num":["000"]},"selected":{"id":"c6d232dc-4abf-4b96-8b32-530f6a215cc7","type":"Selection"},"selection_policy":{"id":"f3b26eeb-40f9-46a4-b0c7-c4019f43d4a0","type":"UnionRenderers"}},"id":"e5dff90d-2d43-4f26-9429-03456cec97be","type":"ColumnDataSource"},{"attributes":{"dimension":1,"plot":{"id":"265104d2-3ad3-425c-8bba-ef68e3402185","subtype":"Figure","type":"Plot"},"ticker":{"id":"f85f9f3d-224e-4631-ac72-65001980a5a3","type":"BasicTicker"},"visible":false},"id":"63285bea-bbd5-450d-983d-194e48cf2844","type":"Grid"},{"attributes":{},"id":"f3b26eeb-40f9-46a4-b0c7-c4019f43d4a0","type":"UnionRenderers"},{"attributes":{"callback":null},"id":"038b006b-3c8e-4abd-9e71-7eac12596453","type":"Range1d"},{"attributes":{"plot":{"id":"265104d2-3ad3-425c-8bba-ef68e3402185","subtype":"Figure","type":"Plot"},"ticker":{"id":"ca5aa5b1-5f31-4809-8e62-bd97c0b3ef42","type":"BasicTicker"},"visible":false},"id":"dd4d164d-02c4-4e63-99d6-f4d884be9cd0","type":"Grid"},{"attributes":{},"id":"a97340d9-c0f2-41e7-8ff4-093238b5df2b","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"63651b76-38a7-46a2-b8ca-03196662df11","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"660ca285-700e-41c1-8fed-b0edc90c669c","type":"ImageURL"},{"attributes":{"formatter":{"id":"82279a7d-8192-4f4a-a915-4caf98bf9d8b","type":"BasicTickFormatter"},"plot":{"id":"265104d2-3ad3-425c-8bba-ef68e3402185","subtype":"Figure","type":"Plot"},"ticker":{"id":"f85f9f3d-224e-4631-ac72-65001980a5a3","type":"BasicTicker"},"visible":false},"id":"aada50b6-d5a4-4151-b585-00d89e7e6499","type":"LinearAxis"},{"attributes":{"callback":{"id":"dc77cd46-fa13-4ee5-8b46-0c4191e4caca","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"6b681efa-af81-4ca3-a4c1-7693b372c943","type":"Slider"},{"attributes":{"formatter":{"id":"e50da510-f41f-4d2c-927c-22e9fa822833","type":"BasicTickFormatter"},"plot":{"id":"265104d2-3ad3-425c-8bba-ef68e3402185","subtype":"Figure","type":"Plot"},"ticker":{"id":"ca5aa5b1-5f31-4809-8e62-bd97c0b3ef42","type":"BasicTicker"},"visible":false},"id":"87a7fbbf-43c8-4aaa-986e-0eb5e9865cd1","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"d35686bc-be4d-482c-886d-caaa576db1f8","type":"Toolbar"},{"attributes":{"children":[{"id":"d5bf1a58-6075-4bc6-9b5a-9ff392cea8d8","type":"WidgetBox"},{"id":"265104d2-3ad3-425c-8bba-ef68e3402185","subtype":"Figure","type":"Plot"}]},"id":"1c61be66-0edb-4a18-8273-1140b9508c35","type":"Column"},{"attributes":{},"id":"fa23c0a1-3242-4019-824e-0d100ef4a066","type":"LinearScale"}],"root_ids":["1c61be66-0edb-4a18-8273-1140b9508c35"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"36062490-928c-4fb1-9e68-44abb80857c5","roots":{"1c61be66-0edb-4a18-8273-1140b9508c35":"02e388af-9817-4637-a467-ddc6eafc8342"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();