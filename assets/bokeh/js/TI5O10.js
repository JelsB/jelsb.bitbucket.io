(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("0fe0a90e-a47b-4ed6-bb98-a6007ab01ddc");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '0fe0a90e-a47b-4ed6-bb98-a6007ab01ddc' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"b636fa09-c19e-4a80-adbd-c0f29953cb9f":{"roots":{"references":[{"attributes":{"args":{"source":{"id":"0dcaf1eb-3e91-4ec2-9011-a6bc8e27f1a6","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"4b9f2bf6-be27-4c85-8d90-ac20e6e26d53","type":"CustomJS"},{"attributes":{"source":{"id":"0dcaf1eb-3e91-4ec2-9011-a6bc8e27f1a6","type":"ColumnDataSource"}},"id":"fa3b7cc2-25d9-4659-9b5c-1238bca9e0ca","type":"CDSView"},{"attributes":{},"id":"94645d54-4a0c-4bab-988b-addd8705b825","type":"UnionRenderers"},{"attributes":{"formatter":{"id":"e99899d9-0837-4067-93e1-3d4fd01d3480","type":"BasicTickFormatter"},"plot":{"id":"6ddf2f79-2902-4c6d-9e49-52da5151c1a3","subtype":"Figure","type":"Plot"},"ticker":{"id":"02d0af5a-02cf-4b8a-921e-c642a9d13d31","type":"BasicTicker"},"visible":false},"id":"d6c01272-e8e6-42d3-903c-99d937c90353","type":"LinearAxis"},{"attributes":{"children":[{"id":"1393ed30-fd8f-41f9-b887-1c064a36e787","type":"Slider"}]},"id":"330af19f-5a2d-48a2-a5b4-f98d485c22ef","type":"WidgetBox"},{"attributes":{"callback":null},"id":"5e744013-7993-4eab-a98b-7f1365421433","type":"Range1d"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI5O10_000.png"],"url_num":["000"]},"selected":{"id":"6c73c9b3-d5cd-40ad-be47-019e97a8c183","type":"Selection"},"selection_policy":{"id":"94645d54-4a0c-4bab-988b-addd8705b825","type":"UnionRenderers"}},"id":"0dcaf1eb-3e91-4ec2-9011-a6bc8e27f1a6","type":"ColumnDataSource"},{"attributes":{},"id":"ccd3cf84-eac0-47c4-b9f3-c7e85a4bb879","type":"LinearScale"},{"attributes":{},"id":"6c73c9b3-d5cd-40ad-be47-019e97a8c183","type":"Selection"},{"attributes":{"dimension":1,"plot":{"id":"6ddf2f79-2902-4c6d-9e49-52da5151c1a3","subtype":"Figure","type":"Plot"},"ticker":{"id":"02d0af5a-02cf-4b8a-921e-c642a9d13d31","type":"BasicTicker"},"visible":false},"id":"70f0d37b-4480-42d6-a406-e9570b1ede9a","type":"Grid"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"b0b794e2-17f0-4b62-aaeb-caab45c0ea3f","type":"Toolbar"},{"attributes":{"callback":null},"id":"8aec7db9-9ded-418e-bbd6-289dd9209c20","type":"Range1d"},{"attributes":{"plot":{"id":"6ddf2f79-2902-4c6d-9e49-52da5151c1a3","subtype":"Figure","type":"Plot"},"ticker":{"id":"7f9634c9-deae-431c-aa88-4c078b31826b","type":"BasicTicker"},"visible":false},"id":"6b1789ed-40bf-4122-a5da-074a208de853","type":"Grid"},{"attributes":{},"id":"7f9634c9-deae-431c-aa88-4c078b31826b","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"f2967e0f-b3b7-4af6-a2e8-671b118b6a1f","type":"Title"},{"attributes":{"data_source":{"id":"0dcaf1eb-3e91-4ec2-9011-a6bc8e27f1a6","type":"ColumnDataSource"},"glyph":{"id":"86933ca0-7e6e-4f2f-b30c-1feb603692da","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"a892b2bf-4561-4818-b533-8f6cfd3e9302","type":"ImageURL"},"selection_glyph":null,"view":{"id":"fa3b7cc2-25d9-4659-9b5c-1238bca9e0ca","type":"CDSView"}},"id":"e125620d-a72a-4b48-9291-1bacb54dbedc","type":"GlyphRenderer"},{"attributes":{},"id":"a55bd003-e60f-46e9-aed6-656ee8cd276f","type":"LinearScale"},{"attributes":{"below":[{"id":"d2b2dfcd-5c7c-4d4f-abc3-0fd0b1ad42c7","type":"LinearAxis"}],"left":[{"id":"d6c01272-e8e6-42d3-903c-99d937c90353","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"d2b2dfcd-5c7c-4d4f-abc3-0fd0b1ad42c7","type":"LinearAxis"},{"id":"6b1789ed-40bf-4122-a5da-074a208de853","type":"Grid"},{"id":"d6c01272-e8e6-42d3-903c-99d937c90353","type":"LinearAxis"},{"id":"70f0d37b-4480-42d6-a406-e9570b1ede9a","type":"Grid"},{"id":"e125620d-a72a-4b48-9291-1bacb54dbedc","type":"GlyphRenderer"}],"title":{"id":"f2967e0f-b3b7-4af6-a2e8-671b118b6a1f","type":"Title"},"toolbar":{"id":"b0b794e2-17f0-4b62-aaeb-caab45c0ea3f","type":"Toolbar"},"x_range":{"id":"5e744013-7993-4eab-a98b-7f1365421433","type":"Range1d"},"x_scale":{"id":"ccd3cf84-eac0-47c4-b9f3-c7e85a4bb879","type":"LinearScale"},"y_range":{"id":"8aec7db9-9ded-418e-bbd6-289dd9209c20","type":"Range1d"},"y_scale":{"id":"a55bd003-e60f-46e9-aed6-656ee8cd276f","type":"LinearScale"}},"id":"6ddf2f79-2902-4c6d-9e49-52da5151c1a3","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"330af19f-5a2d-48a2-a5b4-f98d485c22ef","type":"WidgetBox"},{"id":"6ddf2f79-2902-4c6d-9e49-52da5151c1a3","subtype":"Figure","type":"Plot"}]},"id":"f065e9f7-2651-4f23-b86f-642acd3516d3","type":"Column"},{"attributes":{},"id":"8d4af399-68a7-4ef8-9bba-947296af9d54","type":"BasicTickFormatter"},{"attributes":{},"id":"e99899d9-0837-4067-93e1-3d4fd01d3480","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"8d4af399-68a7-4ef8-9bba-947296af9d54","type":"BasicTickFormatter"},"plot":{"id":"6ddf2f79-2902-4c6d-9e49-52da5151c1a3","subtype":"Figure","type":"Plot"},"ticker":{"id":"7f9634c9-deae-431c-aa88-4c078b31826b","type":"BasicTicker"},"visible":false},"id":"d2b2dfcd-5c7c-4d4f-abc3-0fd0b1ad42c7","type":"LinearAxis"},{"attributes":{"callback":{"id":"4b9f2bf6-be27-4c85-8d90-ac20e6e26d53","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"1393ed30-fd8f-41f9-b887-1c064a36e787","type":"Slider"},{"attributes":{},"id":"02d0af5a-02cf-4b8a-921e-c642a9d13d31","type":"BasicTicker"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"86933ca0-7e6e-4f2f-b30c-1feb603692da","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"a892b2bf-4561-4818-b533-8f6cfd3e9302","type":"ImageURL"}],"root_ids":["f065e9f7-2651-4f23-b86f-642acd3516d3"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"b636fa09-c19e-4a80-adbd-c0f29953cb9f","roots":{"f065e9f7-2651-4f23-b86f-642acd3516d3":"0fe0a90e-a47b-4ed6-bb98-a6007ab01ddc"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();