(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("b2a92a8f-8e00-4364-ab3f-7882c1ab68ef");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'b2a92a8f-8e00-4364-ab3f-7882c1ab68ef' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"d02f4068-397f-4ab8-b1d1-0cea99d8b8a9":{"roots":{"references":[{"attributes":{"callback":null},"id":"5db62f05-ea04-404c-a934-bedf3eb0b203","type":"Range1d"},{"attributes":{},"id":"e330926c-26a2-43bc-ba34-0826c5066972","type":"BasicTicker"},{"attributes":{},"id":"336f1129-825a-4b81-aca2-7350716feb8e","type":"LinearScale"},{"attributes":{"formatter":{"id":"73547a92-799d-47c9-b1fe-a5646101c0f5","type":"BasicTickFormatter"},"plot":{"id":"63e0f9e2-d5ea-42df-9469-fa6ee3664a46","subtype":"Figure","type":"Plot"},"ticker":{"id":"e330926c-26a2-43bc-ba34-0826c5066972","type":"BasicTicker"},"visible":false},"id":"515e42cd-191c-495a-bafb-33a6f611a241","type":"LinearAxis"},{"attributes":{},"id":"262a8caf-b077-47cf-b5ce-c8daf58551bf","type":"Selection"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"da14afce-c39b-4d76-9719-803b9faf3f4a","type":"ImageURL"},{"attributes":{"formatter":{"id":"b4aba6a8-6cde-46e6-86ba-108b0185248a","type":"BasicTickFormatter"},"plot":{"id":"63e0f9e2-d5ea-42df-9469-fa6ee3664a46","subtype":"Figure","type":"Plot"},"ticker":{"id":"379e1e43-21b3-446f-b9db-a53b6601739e","type":"BasicTicker"},"visible":false},"id":"5d6063f2-3cbd-4000-adf5-8f5debd798f6","type":"LinearAxis"},{"attributes":{"callback":null},"id":"cbfa210f-0ade-4b10-a189-874d6be4f842","type":"Range1d"},{"attributes":{"plot":{"id":"63e0f9e2-d5ea-42df-9469-fa6ee3664a46","subtype":"Figure","type":"Plot"},"ticker":{"id":"379e1e43-21b3-446f-b9db-a53b6601739e","type":"BasicTicker"},"visible":false},"id":"5fee994c-2bae-4972-b637-664f9e443c64","type":"Grid"},{"attributes":{},"id":"b4aba6a8-6cde-46e6-86ba-108b0185248a","type":"BasicTickFormatter"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"39f06e98-e548-4695-b6d3-3814484af2a7","type":"Toolbar"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"1eb15479-cfbf-4011-aadc-6f6ffeabef7e","type":"ImageURL"},{"attributes":{},"id":"64f2aee9-2391-48cb-af98-5839b9769f2a","type":"UnionRenderers"},{"attributes":{},"id":"379e1e43-21b3-446f-b9db-a53b6601739e","type":"BasicTicker"},{"attributes":{},"id":"73547a92-799d-47c9-b1fe-a5646101c0f5","type":"BasicTickFormatter"},{"attributes":{"plot":null,"text":""},"id":"4fb982dd-45f0-46ba-a5c6-2d00246268cb","type":"Title"},{"attributes":{"callback":{"id":"258c9450-4317-4e10-a762-56382f72e9d7","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"fba87851-109a-4694-ac0e-806b1af0f460","type":"Slider"},{"attributes":{"source":{"id":"a7ea65b4-0ec1-4e6e-b6cb-5d550bc95ae9","type":"ColumnDataSource"}},"id":"7dbeb8ff-9212-43c8-9236-c44dff48fbf4","type":"CDSView"},{"attributes":{"below":[{"id":"5d6063f2-3cbd-4000-adf5-8f5debd798f6","type":"LinearAxis"}],"left":[{"id":"515e42cd-191c-495a-bafb-33a6f611a241","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"5d6063f2-3cbd-4000-adf5-8f5debd798f6","type":"LinearAxis"},{"id":"5fee994c-2bae-4972-b637-664f9e443c64","type":"Grid"},{"id":"515e42cd-191c-495a-bafb-33a6f611a241","type":"LinearAxis"},{"id":"1f2e1f1a-6e7c-40e5-abed-a2987bbd4b2f","type":"Grid"},{"id":"2b16dbc4-d492-404c-b607-ad93bf819a7f","type":"GlyphRenderer"}],"title":{"id":"4fb982dd-45f0-46ba-a5c6-2d00246268cb","type":"Title"},"toolbar":{"id":"39f06e98-e548-4695-b6d3-3814484af2a7","type":"Toolbar"},"x_range":{"id":"cbfa210f-0ade-4b10-a189-874d6be4f842","type":"Range1d"},"x_scale":{"id":"336f1129-825a-4b81-aca2-7350716feb8e","type":"LinearScale"},"y_range":{"id":"5db62f05-ea04-404c-a934-bedf3eb0b203","type":"Range1d"},"y_scale":{"id":"e2638762-ae0c-4633-920e-3bcc889a5795","type":"LinearScale"}},"id":"63e0f9e2-d5ea-42df-9469-fa6ee3664a46","subtype":"Figure","type":"Plot"},{"attributes":{"args":{"source":{"id":"a7ea65b4-0ec1-4e6e-b6cb-5d550bc95ae9","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"258c9450-4317-4e10-a762-56382f72e9d7","type":"CustomJS"},{"attributes":{"dimension":1,"plot":{"id":"63e0f9e2-d5ea-42df-9469-fa6ee3664a46","subtype":"Figure","type":"Plot"},"ticker":{"id":"e330926c-26a2-43bc-ba34-0826c5066972","type":"BasicTicker"},"visible":false},"id":"1f2e1f1a-6e7c-40e5-abed-a2987bbd4b2f","type":"Grid"},{"attributes":{"data_source":{"id":"a7ea65b4-0ec1-4e6e-b6cb-5d550bc95ae9","type":"ColumnDataSource"},"glyph":{"id":"da14afce-c39b-4d76-9719-803b9faf3f4a","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1eb15479-cfbf-4011-aadc-6f6ffeabef7e","type":"ImageURL"},"selection_glyph":null,"view":{"id":"7dbeb8ff-9212-43c8-9236-c44dff48fbf4","type":"CDSView"}},"id":"2b16dbc4-d492-404c-b607-ad93bf819a7f","type":"GlyphRenderer"},{"attributes":{"children":[{"id":"fba87851-109a-4694-ac0e-806b1af0f460","type":"Slider"}]},"id":"f55b4132-3bac-4db6-b045-d6d8ede0ecb3","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI10O20_000.png"],"url_num":["000"]},"selected":{"id":"262a8caf-b077-47cf-b5ce-c8daf58551bf","type":"Selection"},"selection_policy":{"id":"64f2aee9-2391-48cb-af98-5839b9769f2a","type":"UnionRenderers"}},"id":"a7ea65b4-0ec1-4e6e-b6cb-5d550bc95ae9","type":"ColumnDataSource"},{"attributes":{"children":[{"id":"f55b4132-3bac-4db6-b045-d6d8ede0ecb3","type":"WidgetBox"},{"id":"63e0f9e2-d5ea-42df-9469-fa6ee3664a46","subtype":"Figure","type":"Plot"}]},"id":"b0c2fef5-9013-4652-99ad-dbf64bda381d","type":"Column"},{"attributes":{},"id":"e2638762-ae0c-4633-920e-3bcc889a5795","type":"LinearScale"}],"root_ids":["b0c2fef5-9013-4652-99ad-dbf64bda381d"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"d02f4068-397f-4ab8-b1d1-0cea99d8b8a9","roots":{"b0c2fef5-9013-4652-99ad-dbf64bda381d":"b2a92a8f-8e00-4364-ab3f-7882c1ab68ef"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();