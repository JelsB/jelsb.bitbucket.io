(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("42569e74-99dc-4e14-926b-76b821929836");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '42569e74-99dc-4e14-926b-76b821929836' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"425e8d97-a2ff-47ad-be2f-204857b9fb2f":{"roots":{"references":[{"attributes":{},"id":"646c4111-add7-41e3-9c4d-2fa6fb7abbe9","type":"BasicTickFormatter"},{"attributes":{},"id":"9c31777c-a188-49ed-af94-95c452299854","type":"LinearScale"},{"attributes":{"callback":{"id":"78a90272-034b-446e-9136-dc12503da71d","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"00ca779e-552d-474a-beab-287b72b7a3c6","type":"Slider"},{"attributes":{"below":[{"id":"2a847a69-b2f7-4684-b33e-4b6fdecdc029","type":"LinearAxis"}],"left":[{"id":"e7cad5f5-c94b-4f36-93f7-672220288ac6","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"2a847a69-b2f7-4684-b33e-4b6fdecdc029","type":"LinearAxis"},{"id":"04a8574b-9d4b-4e85-8e9f-09882ee01218","type":"Grid"},{"id":"e7cad5f5-c94b-4f36-93f7-672220288ac6","type":"LinearAxis"},{"id":"27804f2b-fa97-451c-b36b-a34c3b0a0d08","type":"Grid"},{"id":"55a0549f-dbe4-4568-8f6e-e6423e43bba8","type":"GlyphRenderer"}],"title":{"id":"f93bdd29-d837-4560-8f65-6d392f59abd4","type":"Title"},"toolbar":{"id":"854b2868-cb46-45f4-a4e8-85ea64a029a4","type":"Toolbar"},"x_range":{"id":"ebcb2fe5-6348-4f18-904a-e6303d4822c1","type":"Range1d"},"x_scale":{"id":"9c31777c-a188-49ed-af94-95c452299854","type":"LinearScale"},"y_range":{"id":"7140b51d-d08e-4d3b-9f41-64dcab1a76c0","type":"Range1d"},"y_scale":{"id":"8bab722b-dd36-46f8-824b-f469f6682b30","type":"LinearScale"}},"id":"b8384415-9858-47f5-b94d-6782f743e129","subtype":"Figure","type":"Plot"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"fe783d9a-722c-4d5d-a16b-c425aeeae1c2","type":"ImageURL"},{"attributes":{"plot":{"id":"b8384415-9858-47f5-b94d-6782f743e129","subtype":"Figure","type":"Plot"},"ticker":{"id":"f8d3b506-bf65-442f-9786-19d2b0f5bed6","type":"BasicTicker"},"visible":false},"id":"04a8574b-9d4b-4e85-8e9f-09882ee01218","type":"Grid"},{"attributes":{},"id":"8bab722b-dd36-46f8-824b-f469f6682b30","type":"LinearScale"},{"attributes":{"data_source":{"id":"2e7ef8cd-ded9-4390-9832-7d8569edb934","type":"ColumnDataSource"},"glyph":{"id":"70a704f3-8300-445c-b042-2bd29c9d311a","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"fe783d9a-722c-4d5d-a16b-c425aeeae1c2","type":"ImageURL"},"selection_glyph":null,"view":{"id":"84c62010-2913-41fc-b79c-50bd52517c51","type":"CDSView"}},"id":"55a0549f-dbe4-4568-8f6e-e6423e43bba8","type":"GlyphRenderer"},{"attributes":{"source":{"id":"2e7ef8cd-ded9-4390-9832-7d8569edb934","type":"ColumnDataSource"}},"id":"84c62010-2913-41fc-b79c-50bd52517c51","type":"CDSView"},{"attributes":{"formatter":{"id":"6ee141ab-4f85-4e07-a0ee-9ff5862da576","type":"BasicTickFormatter"},"plot":{"id":"b8384415-9858-47f5-b94d-6782f743e129","subtype":"Figure","type":"Plot"},"ticker":{"id":"f8d3b506-bf65-442f-9786-19d2b0f5bed6","type":"BasicTicker"},"visible":false},"id":"2a847a69-b2f7-4684-b33e-4b6fdecdc029","type":"LinearAxis"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"854b2868-cb46-45f4-a4e8-85ea64a029a4","type":"Toolbar"},{"attributes":{},"id":"7fb64a60-8920-4a02-9eb5-5a2f5d60cc15","type":"UnionRenderers"},{"attributes":{"callback":null},"id":"7140b51d-d08e-4d3b-9f41-64dcab1a76c0","type":"Range1d"},{"attributes":{},"id":"c51b6cac-ddf3-4e41-b549-d894d7425ba4","type":"Selection"},{"attributes":{"args":{"source":{"id":"2e7ef8cd-ded9-4390-9832-7d8569edb934","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"78a90272-034b-446e-9136-dc12503da71d","type":"CustomJS"},{"attributes":{},"id":"f8d3b506-bf65-442f-9786-19d2b0f5bed6","type":"BasicTicker"},{"attributes":{"formatter":{"id":"646c4111-add7-41e3-9c4d-2fa6fb7abbe9","type":"BasicTickFormatter"},"plot":{"id":"b8384415-9858-47f5-b94d-6782f743e129","subtype":"Figure","type":"Plot"},"ticker":{"id":"3ea52ee6-321c-4d8d-b2ec-d6bb61f4d90a","type":"BasicTicker"},"visible":false},"id":"e7cad5f5-c94b-4f36-93f7-672220288ac6","type":"LinearAxis"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI5O5_000.png"],"url_num":["000"]},"selected":{"id":"c51b6cac-ddf3-4e41-b549-d894d7425ba4","type":"Selection"},"selection_policy":{"id":"7fb64a60-8920-4a02-9eb5-5a2f5d60cc15","type":"UnionRenderers"}},"id":"2e7ef8cd-ded9-4390-9832-7d8569edb934","type":"ColumnDataSource"},{"attributes":{},"id":"3ea52ee6-321c-4d8d-b2ec-d6bb61f4d90a","type":"BasicTicker"},{"attributes":{"children":[{"id":"00ca779e-552d-474a-beab-287b72b7a3c6","type":"Slider"}]},"id":"b74ae4e6-7d14-4558-955c-466ac8a18833","type":"WidgetBox"},{"attributes":{"callback":null},"id":"ebcb2fe5-6348-4f18-904a-e6303d4822c1","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"b8384415-9858-47f5-b94d-6782f743e129","subtype":"Figure","type":"Plot"},"ticker":{"id":"3ea52ee6-321c-4d8d-b2ec-d6bb61f4d90a","type":"BasicTicker"},"visible":false},"id":"27804f2b-fa97-451c-b36b-a34c3b0a0d08","type":"Grid"},{"attributes":{"children":[{"id":"b74ae4e6-7d14-4558-955c-466ac8a18833","type":"WidgetBox"},{"id":"b8384415-9858-47f5-b94d-6782f743e129","subtype":"Figure","type":"Plot"}]},"id":"10aa2888-2972-4a0f-af46-dc0d68b05238","type":"Column"},{"attributes":{},"id":"6ee141ab-4f85-4e07-a0ee-9ff5862da576","type":"BasicTickFormatter"},{"attributes":{"plot":null,"text":""},"id":"f93bdd29-d837-4560-8f65-6d392f59abd4","type":"Title"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"70a704f3-8300-445c-b042-2bd29c9d311a","type":"ImageURL"}],"root_ids":["10aa2888-2972-4a0f-af46-dc0d68b05238"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"425e8d97-a2ff-47ad-be2f-204857b9fb2f","roots":{"10aa2888-2972-4a0f-af46-dc0d68b05238":"42569e74-99dc-4e14-926b-76b821929836"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();