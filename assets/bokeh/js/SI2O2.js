(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("55c8a055-5efc-48a1-86ea-98beaa270993");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '55c8a055-5efc-48a1-86ea-98beaa270993' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"6e1f5972-89dc-48d9-b27a-67365e77f7cb":{"roots":{"references":[{"attributes":{},"id":"37b8ac2e-1e8e-40e9-8dab-2abdd8db5690","type":"BasicTicker"},{"attributes":{},"id":"a1c41c89-16af-4023-a68d-0174c76548bc","type":"Selection"},{"attributes":{"plot":{"id":"1b7c7ffa-f041-4704-b478-a09e744f0ae0","subtype":"Figure","type":"Plot"},"ticker":{"id":"0fd65fa3-9b1b-42f8-b320-9ab4e4fce762","type":"BasicTicker"},"visible":false},"id":"7e26064a-9da0-4d8b-90c0-580fba28216d","type":"Grid"},{"attributes":{},"id":"2586b4bc-299a-466b-94ce-4351ea472ee8","type":"BasicTickFormatter"},{"attributes":{},"id":"0fd65fa3-9b1b-42f8-b320-9ab4e4fce762","type":"BasicTicker"},{"attributes":{"args":{"source":{"id":"50084ce9-ff6a-4724-b4ba-0656052cf949","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"94fb78a7-53ef-4eb7-ad06-bfd6c4512d16","type":"CustomJS"},{"attributes":{"formatter":{"id":"2586b4bc-299a-466b-94ce-4351ea472ee8","type":"BasicTickFormatter"},"plot":{"id":"1b7c7ffa-f041-4704-b478-a09e744f0ae0","subtype":"Figure","type":"Plot"},"ticker":{"id":"0fd65fa3-9b1b-42f8-b320-9ab4e4fce762","type":"BasicTicker"},"visible":false},"id":"abf66f33-9ce4-4796-8258-f3c7372bc81e","type":"LinearAxis"},{"attributes":{"source":{"id":"50084ce9-ff6a-4724-b4ba-0656052cf949","type":"ColumnDataSource"}},"id":"b393abec-5aee-4fbc-a059-faa1ce4d7c2a","type":"CDSView"},{"attributes":{"formatter":{"id":"bf4795e4-0a4e-4835-95bb-1d766496afde","type":"BasicTickFormatter"},"plot":{"id":"1b7c7ffa-f041-4704-b478-a09e744f0ae0","subtype":"Figure","type":"Plot"},"ticker":{"id":"37b8ac2e-1e8e-40e9-8dab-2abdd8db5690","type":"BasicTicker"},"visible":false},"id":"0329adf7-d5b7-44e1-950b-45c4889a65bd","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"a5b714ce-6be6-4b9a-a1dd-f403fec3ee6d","type":"Title"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"a9c0b412-b388-4e6c-bc03-0d00190650a1","type":"ImageURL"},{"attributes":{"dimension":1,"plot":{"id":"1b7c7ffa-f041-4704-b478-a09e744f0ae0","subtype":"Figure","type":"Plot"},"ticker":{"id":"37b8ac2e-1e8e-40e9-8dab-2abdd8db5690","type":"BasicTicker"},"visible":false},"id":"22db3b29-6efc-44e8-9096-a3632ea81cc1","type":"Grid"},{"attributes":{"callback":null},"id":"e1f04164-b13f-46ff-9f1c-b8fff4bb8cbd","type":"Range1d"},{"attributes":{},"id":"28ffc6d7-328e-46e3-873d-99aa223313a4","type":"LinearScale"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI2O2_000.png"],"url_num":["000"]},"selected":{"id":"a1c41c89-16af-4023-a68d-0174c76548bc","type":"Selection"},"selection_policy":{"id":"68cc1800-e029-49e4-9ae6-dee70af492f5","type":"UnionRenderers"}},"id":"50084ce9-ff6a-4724-b4ba-0656052cf949","type":"ColumnDataSource"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"c2954ced-97f9-4104-ac67-185a65b11d41","type":"Toolbar"},{"attributes":{"children":[{"id":"e9b0eef3-eded-46da-af98-3d5b8b8e5ed3","type":"WidgetBox"},{"id":"1b7c7ffa-f041-4704-b478-a09e744f0ae0","subtype":"Figure","type":"Plot"}]},"id":"cdc54fab-e68e-4fe6-951d-f2404b3b7f45","type":"Column"},{"attributes":{"callback":null},"id":"c1c5d1a1-c09e-4b1a-a541-2289825c1a4d","type":"Range1d"},{"attributes":{},"id":"83cf1e04-a717-457c-ab82-dd6cd8834a94","type":"LinearScale"},{"attributes":{},"id":"bf4795e4-0a4e-4835-95bb-1d766496afde","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"793d6df0-d36e-47cc-87ec-9fdbf3a1d9dd","type":"Slider"}]},"id":"e9b0eef3-eded-46da-af98-3d5b8b8e5ed3","type":"WidgetBox"},{"attributes":{},"id":"68cc1800-e029-49e4-9ae6-dee70af492f5","type":"UnionRenderers"},{"attributes":{"data_source":{"id":"50084ce9-ff6a-4724-b4ba-0656052cf949","type":"ColumnDataSource"},"glyph":{"id":"a9c0b412-b388-4e6c-bc03-0d00190650a1","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"6da1cb1a-521a-407e-9317-e6819c019342","type":"ImageURL"},"selection_glyph":null,"view":{"id":"b393abec-5aee-4fbc-a059-faa1ce4d7c2a","type":"CDSView"}},"id":"e99f850f-c6fd-4543-8872-6bb3c7b26f7d","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"6da1cb1a-521a-407e-9317-e6819c019342","type":"ImageURL"},{"attributes":{"callback":{"id":"94fb78a7-53ef-4eb7-ad06-bfd6c4512d16","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"793d6df0-d36e-47cc-87ec-9fdbf3a1d9dd","type":"Slider"},{"attributes":{"below":[{"id":"abf66f33-9ce4-4796-8258-f3c7372bc81e","type":"LinearAxis"}],"left":[{"id":"0329adf7-d5b7-44e1-950b-45c4889a65bd","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"abf66f33-9ce4-4796-8258-f3c7372bc81e","type":"LinearAxis"},{"id":"7e26064a-9da0-4d8b-90c0-580fba28216d","type":"Grid"},{"id":"0329adf7-d5b7-44e1-950b-45c4889a65bd","type":"LinearAxis"},{"id":"22db3b29-6efc-44e8-9096-a3632ea81cc1","type":"Grid"},{"id":"e99f850f-c6fd-4543-8872-6bb3c7b26f7d","type":"GlyphRenderer"}],"title":{"id":"a5b714ce-6be6-4b9a-a1dd-f403fec3ee6d","type":"Title"},"toolbar":{"id":"c2954ced-97f9-4104-ac67-185a65b11d41","type":"Toolbar"},"x_range":{"id":"c1c5d1a1-c09e-4b1a-a541-2289825c1a4d","type":"Range1d"},"x_scale":{"id":"83cf1e04-a717-457c-ab82-dd6cd8834a94","type":"LinearScale"},"y_range":{"id":"e1f04164-b13f-46ff-9f1c-b8fff4bb8cbd","type":"Range1d"},"y_scale":{"id":"28ffc6d7-328e-46e3-873d-99aa223313a4","type":"LinearScale"}},"id":"1b7c7ffa-f041-4704-b478-a09e744f0ae0","subtype":"Figure","type":"Plot"}],"root_ids":["cdc54fab-e68e-4fe6-951d-f2404b3b7f45"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"6e1f5972-89dc-48d9-b27a-67365e77f7cb","roots":{"cdc54fab-e68e-4fe6-951d-f2404b3b7f45":"55c8a055-5efc-48a1-86ea-98beaa270993"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();