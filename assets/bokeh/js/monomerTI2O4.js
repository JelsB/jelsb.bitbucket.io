(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("208e8a48-cfbd-4f8d-9b6b-904eeb4ff7bd");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '208e8a48-cfbd-4f8d-9b6b-904eeb4ff7bd' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"0e877249-915c-4b03-8ae0-178d253025ad":{"roots":{"references":[{"attributes":{"children":[{"id":"e25840f6-82d0-4a70-ae3c-bb6a250a33af","type":"Slider"}]},"id":"cf7a9d46-9317-4053-9ed0-44a6fb9140f1","type":"WidgetBox"},{"attributes":{},"id":"79798d0b-6c4f-42ef-9dc6-6c77648144e0","type":"Selection"},{"attributes":{"source":{"id":"5abb86eb-ef49-47b0-99ab-0b2eb92f64f2","type":"ColumnDataSource"}},"id":"f9b4ff92-5214-46d0-bf37-8118b272add8","type":"CDSView"},{"attributes":{},"id":"932a213d-cccb-4edd-8c53-0763ed63a609","type":"UnionRenderers"},{"attributes":{"data_source":{"id":"5abb86eb-ef49-47b0-99ab-0b2eb92f64f2","type":"ColumnDataSource"},"glyph":{"id":"2cf13848-bff3-4581-ba3f-0b2e9cb8971c","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"16ca2a92-e26f-44e1-a12d-5c51c907a802","type":"ImageURL"},"selection_glyph":null,"view":{"id":"f9b4ff92-5214-46d0-bf37-8118b272add8","type":"CDSView"}},"id":"e9864bb3-09c8-4ff2-95d7-4600259da6c1","type":"GlyphRenderer"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI2O4_000.png"],"url_num":["000"]},"selected":{"id":"79798d0b-6c4f-42ef-9dc6-6c77648144e0","type":"Selection"},"selection_policy":{"id":"932a213d-cccb-4edd-8c53-0763ed63a609","type":"UnionRenderers"}},"id":"5abb86eb-ef49-47b0-99ab-0b2eb92f64f2","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"16ca2a92-e26f-44e1-a12d-5c51c907a802","type":"ImageURL"},{"attributes":{"formatter":{"id":"a1b00e26-66d8-4c76-868d-69b553f43927","type":"BasicTickFormatter"},"plot":{"id":"dda80d2b-069f-48ad-aa8b-1f243019d5db","subtype":"Figure","type":"Plot"},"ticker":{"id":"acde92ba-73f4-4a54-81d1-3c0372fbdb76","type":"BasicTicker"},"visible":false},"id":"4748c0c1-0709-4101-bf5f-f76a710a0711","type":"LinearAxis"},{"attributes":{"children":[{"id":"cf7a9d46-9317-4053-9ed0-44a6fb9140f1","type":"WidgetBox"},{"id":"dda80d2b-069f-48ad-aa8b-1f243019d5db","subtype":"Figure","type":"Plot"}]},"id":"430c2bdc-4ecb-47fa-8eac-4129d082baeb","type":"Column"},{"attributes":{},"id":"acde92ba-73f4-4a54-81d1-3c0372fbdb76","type":"BasicTicker"},{"attributes":{"callback":null},"id":"420fbf4c-e4f2-47d5-9bb9-46fcff39563a","type":"Range1d"},{"attributes":{},"id":"663b51db-381b-4a1b-bc95-9f2c44d0d8bb","type":"BasicTickFormatter"},{"attributes":{},"id":"21e730f7-882a-403c-b396-068a65de9f67","type":"LinearScale"},{"attributes":{},"id":"a1b00e26-66d8-4c76-868d-69b553f43927","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"663b51db-381b-4a1b-bc95-9f2c44d0d8bb","type":"BasicTickFormatter"},"plot":{"id":"dda80d2b-069f-48ad-aa8b-1f243019d5db","subtype":"Figure","type":"Plot"},"ticker":{"id":"28bd6080-3e49-4ff0-a2ff-e84d9f4c1f07","type":"BasicTicker"},"visible":false},"id":"73d99ad2-9f02-4767-8c13-bdf41e033631","type":"LinearAxis"},{"attributes":{"plot":null,"text":""},"id":"fb81cb47-43ea-4d98-b297-cd395bdd24d7","type":"Title"},{"attributes":{"dimension":1,"plot":{"id":"dda80d2b-069f-48ad-aa8b-1f243019d5db","subtype":"Figure","type":"Plot"},"ticker":{"id":"28bd6080-3e49-4ff0-a2ff-e84d9f4c1f07","type":"BasicTicker"},"visible":false},"id":"1abd7f5c-b559-4388-835f-bd5b4b84656e","type":"Grid"},{"attributes":{},"id":"2d329614-c8e7-45af-b2ff-ba6d810f9e21","type":"LinearScale"},{"attributes":{"below":[{"id":"4748c0c1-0709-4101-bf5f-f76a710a0711","type":"LinearAxis"}],"left":[{"id":"73d99ad2-9f02-4767-8c13-bdf41e033631","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"4748c0c1-0709-4101-bf5f-f76a710a0711","type":"LinearAxis"},{"id":"a922a3d0-cb67-409b-9250-29db5a2709e2","type":"Grid"},{"id":"73d99ad2-9f02-4767-8c13-bdf41e033631","type":"LinearAxis"},{"id":"1abd7f5c-b559-4388-835f-bd5b4b84656e","type":"Grid"},{"id":"e9864bb3-09c8-4ff2-95d7-4600259da6c1","type":"GlyphRenderer"}],"title":{"id":"fb81cb47-43ea-4d98-b297-cd395bdd24d7","type":"Title"},"toolbar":{"id":"780c58ae-50bb-47d5-90e7-f1e60f79266c","type":"Toolbar"},"x_range":{"id":"420fbf4c-e4f2-47d5-9bb9-46fcff39563a","type":"Range1d"},"x_scale":{"id":"21e730f7-882a-403c-b396-068a65de9f67","type":"LinearScale"},"y_range":{"id":"2a7e3bff-35d1-42db-ac61-6c9eedd2edf9","type":"Range1d"},"y_scale":{"id":"2d329614-c8e7-45af-b2ff-ba6d810f9e21","type":"LinearScale"}},"id":"dda80d2b-069f-48ad-aa8b-1f243019d5db","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"2a7e3bff-35d1-42db-ac61-6c9eedd2edf9","type":"Range1d"},{"attributes":{"plot":{"id":"dda80d2b-069f-48ad-aa8b-1f243019d5db","subtype":"Figure","type":"Plot"},"ticker":{"id":"acde92ba-73f4-4a54-81d1-3c0372fbdb76","type":"BasicTicker"},"visible":false},"id":"a922a3d0-cb67-409b-9250-29db5a2709e2","type":"Grid"},{"attributes":{"args":{"source":{"id":"5abb86eb-ef49-47b0-99ab-0b2eb92f64f2","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"dedac480-1efd-494c-afc3-cea99a9517ca","type":"CustomJS"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"780c58ae-50bb-47d5-90e7-f1e60f79266c","type":"Toolbar"},{"attributes":{"callback":{"id":"dedac480-1efd-494c-afc3-cea99a9517ca","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"e25840f6-82d0-4a70-ae3c-bb6a250a33af","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"2cf13848-bff3-4581-ba3f-0b2e9cb8971c","type":"ImageURL"},{"attributes":{},"id":"28bd6080-3e49-4ff0-a2ff-e84d9f4c1f07","type":"BasicTicker"}],"root_ids":["430c2bdc-4ecb-47fa-8eac-4129d082baeb"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"0e877249-915c-4b03-8ae0-178d253025ad","roots":{"430c2bdc-4ecb-47fa-8eac-4129d082baeb":"208e8a48-cfbd-4f8d-9b6b-904eeb4ff7bd"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();