(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("ede19314-0087-4204-b2ce-bdc7b16c608b");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'ede19314-0087-4204-b2ce-bdc7b16c608b' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"fc323825-a388-4f5d-b3f4-6329bd83ba39":{"roots":{"references":[{"attributes":{"data_source":{"id":"6960d01a-5122-4d6d-9b85-57d4f49acbee","type":"ColumnDataSource"},"glyph":{"id":"656e3639-0d4d-4095-98e3-9970e3135cf1","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"80ab684c-87b5-49f4-95f2-c10bfa7a0612","type":"ImageURL"},"selection_glyph":null,"view":{"id":"a8639223-7c09-48c2-ae17-cea22ce055d5","type":"CDSView"}},"id":"98ee2694-3499-4e47-a42c-70a76088c4cd","type":"GlyphRenderer"},{"attributes":{"children":[{"id":"c98a70f4-dd84-4953-b938-6e8758843ca7","type":"Slider"}]},"id":"2fe094b0-145a-49f7-9417-b2a17256f80a","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSIO_000.png"],"url_num":["000"]},"selected":{"id":"0626a627-5a63-4d7e-a1bf-8bb922c322e3","type":"Selection"},"selection_policy":{"id":"c0871b66-ad21-42c9-bdcf-03f3f5ee8f85","type":"UnionRenderers"}},"id":"6960d01a-5122-4d6d-9b85-57d4f49acbee","type":"ColumnDataSource"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"80ab684c-87b5-49f4-95f2-c10bfa7a0612","type":"ImageURL"},{"attributes":{"dimension":1,"plot":{"id":"67c8d9dc-1bff-4858-b73e-ff9a98d85bfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"7873b83c-0d9a-4ad3-84af-267b54e4994a","type":"BasicTicker"},"visible":false},"id":"9eeb9f3c-3276-45af-810f-6e6fab8d171c","type":"Grid"},{"attributes":{},"id":"17e495f6-b617-4079-bd45-447f560e7dff","type":"BasicTicker"},{"attributes":{"args":{"source":{"id":"6960d01a-5122-4d6d-9b85-57d4f49acbee","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"a631f477-72e1-4fc3-822e-aaeb51be9009","type":"CustomJS"},{"attributes":{"callback":null},"id":"d3bf7820-172b-4da0-b796-adb2f2054622","type":"Range1d"},{"attributes":{"formatter":{"id":"3a5a50c5-538e-4402-a045-92ac5bbf0035","type":"BasicTickFormatter"},"plot":{"id":"67c8d9dc-1bff-4858-b73e-ff9a98d85bfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"17e495f6-b617-4079-bd45-447f560e7dff","type":"BasicTicker"},"visible":false},"id":"53fe571e-b607-4492-a184-7a6d990ce3c6","type":"LinearAxis"},{"attributes":{},"id":"c0871b66-ad21-42c9-bdcf-03f3f5ee8f85","type":"UnionRenderers"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"56117b10-b4cd-49fd-863f-075e6f704069","type":"Toolbar"},{"attributes":{},"id":"c87ac693-8b8b-40b6-8362-ba8d2af3c3f7","type":"LinearScale"},{"attributes":{},"id":"0996ee58-e6ec-4623-bd52-2133376baa5a","type":"LinearScale"},{"attributes":{"callback":{"id":"a631f477-72e1-4fc3-822e-aaeb51be9009","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c98a70f4-dd84-4953-b938-6e8758843ca7","type":"Slider"},{"attributes":{"formatter":{"id":"7d20a49f-a4d7-4eed-ba22-df2a50f3abc7","type":"BasicTickFormatter"},"plot":{"id":"67c8d9dc-1bff-4858-b73e-ff9a98d85bfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"7873b83c-0d9a-4ad3-84af-267b54e4994a","type":"BasicTicker"},"visible":false},"id":"24f01a62-8f2d-4756-b97f-89d8675edc9e","type":"LinearAxis"},{"attributes":{"callback":null},"id":"f311326a-2084-4437-bf3f-a54414469b4a","type":"Range1d"},{"attributes":{"plot":{"id":"67c8d9dc-1bff-4858-b73e-ff9a98d85bfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"17e495f6-b617-4079-bd45-447f560e7dff","type":"BasicTicker"},"visible":false},"id":"74d55cce-47f5-44dd-8574-ca06f9c2333e","type":"Grid"},{"attributes":{},"id":"7d20a49f-a4d7-4eed-ba22-df2a50f3abc7","type":"BasicTickFormatter"},{"attributes":{"plot":null,"text":""},"id":"f6929bf9-9b17-4370-a04f-2e697f6afd81","type":"Title"},{"attributes":{"children":[{"id":"2fe094b0-145a-49f7-9417-b2a17256f80a","type":"WidgetBox"},{"id":"67c8d9dc-1bff-4858-b73e-ff9a98d85bfd","subtype":"Figure","type":"Plot"}]},"id":"b0146ae8-e957-428b-b916-1b9a64110466","type":"Column"},{"attributes":{"source":{"id":"6960d01a-5122-4d6d-9b85-57d4f49acbee","type":"ColumnDataSource"}},"id":"a8639223-7c09-48c2-ae17-cea22ce055d5","type":"CDSView"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"656e3639-0d4d-4095-98e3-9970e3135cf1","type":"ImageURL"},{"attributes":{"below":[{"id":"53fe571e-b607-4492-a184-7a6d990ce3c6","type":"LinearAxis"}],"left":[{"id":"24f01a62-8f2d-4756-b97f-89d8675edc9e","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"53fe571e-b607-4492-a184-7a6d990ce3c6","type":"LinearAxis"},{"id":"74d55cce-47f5-44dd-8574-ca06f9c2333e","type":"Grid"},{"id":"24f01a62-8f2d-4756-b97f-89d8675edc9e","type":"LinearAxis"},{"id":"9eeb9f3c-3276-45af-810f-6e6fab8d171c","type":"Grid"},{"id":"98ee2694-3499-4e47-a42c-70a76088c4cd","type":"GlyphRenderer"}],"title":{"id":"f6929bf9-9b17-4370-a04f-2e697f6afd81","type":"Title"},"toolbar":{"id":"56117b10-b4cd-49fd-863f-075e6f704069","type":"Toolbar"},"x_range":{"id":"d3bf7820-172b-4da0-b796-adb2f2054622","type":"Range1d"},"x_scale":{"id":"c87ac693-8b8b-40b6-8362-ba8d2af3c3f7","type":"LinearScale"},"y_range":{"id":"f311326a-2084-4437-bf3f-a54414469b4a","type":"Range1d"},"y_scale":{"id":"0996ee58-e6ec-4623-bd52-2133376baa5a","type":"LinearScale"}},"id":"67c8d9dc-1bff-4858-b73e-ff9a98d85bfd","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"3a5a50c5-538e-4402-a045-92ac5bbf0035","type":"BasicTickFormatter"},{"attributes":{},"id":"7873b83c-0d9a-4ad3-84af-267b54e4994a","type":"BasicTicker"},{"attributes":{},"id":"0626a627-5a63-4d7e-a1bf-8bb922c322e3","type":"Selection"}],"root_ids":["b0146ae8-e957-428b-b916-1b9a64110466"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"fc323825-a388-4f5d-b3f4-6329bd83ba39","roots":{"b0146ae8-e957-428b-b916-1b9a64110466":"ede19314-0087-4204-b2ce-bdc7b16c608b"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();