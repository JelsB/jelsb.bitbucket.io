(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("304dba7b-9f2b-4ec2-9bb2-261a4e6bb9f0");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '304dba7b-9f2b-4ec2-9bb2-261a4e6bb9f0' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"cfda0bd6-6ccb-4332-8bd3-4056aac6a245":{"roots":{"references":[{"attributes":{"formatter":{"id":"c1e08364-51b1-483a-a626-59555a7c1734","type":"BasicTickFormatter"},"plot":{"id":"a7405555-956c-48ee-af5f-0f5b9550e61d","subtype":"Figure","type":"Plot"},"ticker":{"id":"e6391c04-2fe6-4d3f-9ac0-0425321256f4","type":"BasicTicker"},"visible":false},"id":"40842333-5c16-4b61-98b4-dd9098556f36","type":"LinearAxis"},{"attributes":{},"id":"2c596584-0cad-4b74-8343-dbc81990a7e7","type":"LinearScale"},{"attributes":{},"id":"5e5aa4d4-0f34-485c-b1bf-5844b4e62edf","type":"UnionRenderers"},{"attributes":{},"id":"0a00fb47-a627-4d93-9f55-dbb9b8480e0b","type":"Selection"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"8c1ef86d-7f61-40da-b1c1-da8bbd08042a","type":"Toolbar"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI7O7_000.png"],"url_num":["000"]},"selected":{"id":"0a00fb47-a627-4d93-9f55-dbb9b8480e0b","type":"Selection"},"selection_policy":{"id":"5e5aa4d4-0f34-485c-b1bf-5844b4e62edf","type":"UnionRenderers"}},"id":"0a6cd649-a088-4f90-8b9d-664641041f4e","type":"ColumnDataSource"},{"attributes":{},"id":"a66ab4ef-705a-4abc-b796-dafca2f4d2d7","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"005d5fde-3325-4852-b685-b5e0674edf77","type":"Slider"}]},"id":"0180c05f-bffd-4e10-ab21-f3d050853e07","type":"WidgetBox"},{"attributes":{"below":[{"id":"f7f6f455-f892-40de-9cd9-a97fa6847449","type":"LinearAxis"}],"left":[{"id":"40842333-5c16-4b61-98b4-dd9098556f36","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"f7f6f455-f892-40de-9cd9-a97fa6847449","type":"LinearAxis"},{"id":"7f40caaa-cca6-4942-b6a2-506b3d5109f4","type":"Grid"},{"id":"40842333-5c16-4b61-98b4-dd9098556f36","type":"LinearAxis"},{"id":"497d3530-8aa3-4927-8dea-484b7ba2afb7","type":"Grid"},{"id":"44692037-575c-4164-957a-ea3ac4f480a7","type":"GlyphRenderer"}],"title":{"id":"d3c511a0-f7d0-4d7b-81bc-99f7b1ff7866","type":"Title"},"toolbar":{"id":"8c1ef86d-7f61-40da-b1c1-da8bbd08042a","type":"Toolbar"},"x_range":{"id":"79a574d2-8aee-42f0-8f3b-174ce8026f1d","type":"Range1d"},"x_scale":{"id":"9059d5e7-6716-4439-94b3-addac574d951","type":"LinearScale"},"y_range":{"id":"5db71b73-fe0d-49b9-8e23-e17652cdb002","type":"Range1d"},"y_scale":{"id":"2c596584-0cad-4b74-8343-dbc81990a7e7","type":"LinearScale"}},"id":"a7405555-956c-48ee-af5f-0f5b9550e61d","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"9b023aef-ed87-4673-80ef-d02e58d11708","type":"BasicTicker"},{"attributes":{"formatter":{"id":"a66ab4ef-705a-4abc-b796-dafca2f4d2d7","type":"BasicTickFormatter"},"plot":{"id":"a7405555-956c-48ee-af5f-0f5b9550e61d","subtype":"Figure","type":"Plot"},"ticker":{"id":"9b023aef-ed87-4673-80ef-d02e58d11708","type":"BasicTicker"},"visible":false},"id":"f7f6f455-f892-40de-9cd9-a97fa6847449","type":"LinearAxis"},{"attributes":{},"id":"9059d5e7-6716-4439-94b3-addac574d951","type":"LinearScale"},{"attributes":{"callback":{"id":"367ad544-1a1f-4ea9-8956-8fc5b43ab5f0","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"005d5fde-3325-4852-b685-b5e0674edf77","type":"Slider"},{"attributes":{"children":[{"id":"0180c05f-bffd-4e10-ab21-f3d050853e07","type":"WidgetBox"},{"id":"a7405555-956c-48ee-af5f-0f5b9550e61d","subtype":"Figure","type":"Plot"}]},"id":"db9afd65-8964-44f0-a92d-cbcd1f4feace","type":"Column"},{"attributes":{"plot":null,"text":""},"id":"d3c511a0-f7d0-4d7b-81bc-99f7b1ff7866","type":"Title"},{"attributes":{"source":{"id":"0a6cd649-a088-4f90-8b9d-664641041f4e","type":"ColumnDataSource"}},"id":"24b401b9-5a20-44d4-bb7d-cabc13d8c68b","type":"CDSView"},{"attributes":{},"id":"e6391c04-2fe6-4d3f-9ac0-0425321256f4","type":"BasicTicker"},{"attributes":{"callback":null},"id":"79a574d2-8aee-42f0-8f3b-174ce8026f1d","type":"Range1d"},{"attributes":{"data_source":{"id":"0a6cd649-a088-4f90-8b9d-664641041f4e","type":"ColumnDataSource"},"glyph":{"id":"3cac6b81-4d41-4a4b-9ca7-ad130f793422","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"8214ac86-3806-4cdf-b558-660415b4cece","type":"ImageURL"},"selection_glyph":null,"view":{"id":"24b401b9-5a20-44d4-bb7d-cabc13d8c68b","type":"CDSView"}},"id":"44692037-575c-4164-957a-ea3ac4f480a7","type":"GlyphRenderer"},{"attributes":{"args":{"source":{"id":"0a6cd649-a088-4f90-8b9d-664641041f4e","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"367ad544-1a1f-4ea9-8956-8fc5b43ab5f0","type":"CustomJS"},{"attributes":{"dimension":1,"plot":{"id":"a7405555-956c-48ee-af5f-0f5b9550e61d","subtype":"Figure","type":"Plot"},"ticker":{"id":"e6391c04-2fe6-4d3f-9ac0-0425321256f4","type":"BasicTicker"},"visible":false},"id":"497d3530-8aa3-4927-8dea-484b7ba2afb7","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"3cac6b81-4d41-4a4b-9ca7-ad130f793422","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8214ac86-3806-4cdf-b558-660415b4cece","type":"ImageURL"},{"attributes":{"callback":null},"id":"5db71b73-fe0d-49b9-8e23-e17652cdb002","type":"Range1d"},{"attributes":{},"id":"c1e08364-51b1-483a-a626-59555a7c1734","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"a7405555-956c-48ee-af5f-0f5b9550e61d","subtype":"Figure","type":"Plot"},"ticker":{"id":"9b023aef-ed87-4673-80ef-d02e58d11708","type":"BasicTicker"},"visible":false},"id":"7f40caaa-cca6-4942-b6a2-506b3d5109f4","type":"Grid"}],"root_ids":["db9afd65-8964-44f0-a92d-cbcd1f4feace"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"cfda0bd6-6ccb-4332-8bd3-4056aac6a245","roots":{"db9afd65-8964-44f0-a92d-cbcd1f4feace":"304dba7b-9f2b-4ec2-9bb2-261a4e6bb9f0"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();