(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("7481d534-8ad4-491c-82b7-28ceb9c6a8ab");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '7481d534-8ad4-491c-82b7-28ceb9c6a8ab' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"777984a0-7a94-4d43-9dba-81656cfb7829":{"roots":{"references":[{"attributes":{"plot":null,"text":""},"id":"df72b1a0-3add-43d1-b9a0-56ded7a6d30c","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"03502463-3027-48c7-a584-6c99fa0d03f9","type":"Toolbar"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"2f7012d1-9028-4d0c-bbdf-b1d8ec878f0c","type":"ImageURL"},{"attributes":{"plot":{"id":"010102e9-95fa-464f-81d7-7684fdeb39dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"081b6460-fe1e-47c6-8bf8-9f88e24c6931","type":"BasicTicker"},"visible":false},"id":"37faa0c3-6c14-480f-84fc-c05c8f43fb9b","type":"Grid"},{"attributes":{},"id":"792acabb-cf15-422f-ba52-e4777db4ce6d","type":"BasicTicker"},{"attributes":{},"id":"8b04ebfb-16e8-4da9-8a6c-fe28c671cae5","type":"BasicTickFormatter"},{"attributes":{"args":{"source":{"id":"1b3011d9-1e0c-4a26-9262-4109fc954a58","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"3ba8205b-0d99-4166-9a6f-7b7e6d7dc895","type":"CustomJS"},{"attributes":{"source":{"id":"1b3011d9-1e0c-4a26-9262-4109fc954a58","type":"ColumnDataSource"}},"id":"6fb13c63-b325-41fc-bec4-8c3e6cd5cf59","type":"CDSView"},{"attributes":{},"id":"b50d8c68-6b02-4b52-a9fb-ff8f532e45cf","type":"LinearScale"},{"attributes":{},"id":"d46ff660-09f0-4063-98ad-0bfacfb708cf","type":"Selection"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI9O9_000.png"],"url_num":["000"]},"selected":{"id":"d46ff660-09f0-4063-98ad-0bfacfb708cf","type":"Selection"},"selection_policy":{"id":"3a2eeb97-3f46-40e7-b87f-21cf40353976","type":"UnionRenderers"}},"id":"1b3011d9-1e0c-4a26-9262-4109fc954a58","type":"ColumnDataSource"},{"attributes":{},"id":"ff39740d-e030-49a2-a4ea-5ba6a5930c58","type":"BasicTickFormatter"},{"attributes":{},"id":"c2ff05c6-6417-4dce-a9ec-f0e83605ebed","type":"LinearScale"},{"attributes":{"formatter":{"id":"8b04ebfb-16e8-4da9-8a6c-fe28c671cae5","type":"BasicTickFormatter"},"plot":{"id":"010102e9-95fa-464f-81d7-7684fdeb39dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"081b6460-fe1e-47c6-8bf8-9f88e24c6931","type":"BasicTicker"},"visible":false},"id":"9921a4a9-3f02-4b8d-8dcb-0da2f898727f","type":"LinearAxis"},{"attributes":{"children":[{"id":"c9e92ce5-19e7-4ade-a97f-15d68d8ede7d","type":"Slider"}]},"id":"a08ef602-2510-4b28-8d12-0f34d0765881","type":"WidgetBox"},{"attributes":{},"id":"081b6460-fe1e-47c6-8bf8-9f88e24c6931","type":"BasicTicker"},{"attributes":{"callback":null},"id":"d4d13e5f-0f4f-4e95-b096-4bb86596d298","type":"Range1d"},{"attributes":{"callback":null},"id":"4805e8b2-6e3c-410c-86a7-c035bf795a3e","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"010102e9-95fa-464f-81d7-7684fdeb39dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"792acabb-cf15-422f-ba52-e4777db4ce6d","type":"BasicTicker"},"visible":false},"id":"d620b16b-f5dd-4ed2-898b-69e1086a71b4","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"01208f39-0320-45c7-a1a9-b1ca628e2f08","type":"ImageURL"},{"attributes":{"below":[{"id":"9921a4a9-3f02-4b8d-8dcb-0da2f898727f","type":"LinearAxis"}],"left":[{"id":"3cf5a3ae-0f86-42a0-bb44-c89d9f5f60ec","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"9921a4a9-3f02-4b8d-8dcb-0da2f898727f","type":"LinearAxis"},{"id":"37faa0c3-6c14-480f-84fc-c05c8f43fb9b","type":"Grid"},{"id":"3cf5a3ae-0f86-42a0-bb44-c89d9f5f60ec","type":"LinearAxis"},{"id":"d620b16b-f5dd-4ed2-898b-69e1086a71b4","type":"Grid"},{"id":"fcc1ce34-8b2e-4300-bd9c-9d4714f19447","type":"GlyphRenderer"}],"title":{"id":"df72b1a0-3add-43d1-b9a0-56ded7a6d30c","type":"Title"},"toolbar":{"id":"03502463-3027-48c7-a584-6c99fa0d03f9","type":"Toolbar"},"x_range":{"id":"4805e8b2-6e3c-410c-86a7-c035bf795a3e","type":"Range1d"},"x_scale":{"id":"b50d8c68-6b02-4b52-a9fb-ff8f532e45cf","type":"LinearScale"},"y_range":{"id":"d4d13e5f-0f4f-4e95-b096-4bb86596d298","type":"Range1d"},"y_scale":{"id":"c2ff05c6-6417-4dce-a9ec-f0e83605ebed","type":"LinearScale"}},"id":"010102e9-95fa-464f-81d7-7684fdeb39dc","subtype":"Figure","type":"Plot"},{"attributes":{"formatter":{"id":"ff39740d-e030-49a2-a4ea-5ba6a5930c58","type":"BasicTickFormatter"},"plot":{"id":"010102e9-95fa-464f-81d7-7684fdeb39dc","subtype":"Figure","type":"Plot"},"ticker":{"id":"792acabb-cf15-422f-ba52-e4777db4ce6d","type":"BasicTicker"},"visible":false},"id":"3cf5a3ae-0f86-42a0-bb44-c89d9f5f60ec","type":"LinearAxis"},{"attributes":{},"id":"3a2eeb97-3f46-40e7-b87f-21cf40353976","type":"UnionRenderers"},{"attributes":{"data_source":{"id":"1b3011d9-1e0c-4a26-9262-4109fc954a58","type":"ColumnDataSource"},"glyph":{"id":"01208f39-0320-45c7-a1a9-b1ca628e2f08","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"2f7012d1-9028-4d0c-bbdf-b1d8ec878f0c","type":"ImageURL"},"selection_glyph":null,"view":{"id":"6fb13c63-b325-41fc-bec4-8c3e6cd5cf59","type":"CDSView"}},"id":"fcc1ce34-8b2e-4300-bd9c-9d4714f19447","type":"GlyphRenderer"},{"attributes":{"callback":{"id":"3ba8205b-0d99-4166-9a6f-7b7e6d7dc895","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c9e92ce5-19e7-4ade-a97f-15d68d8ede7d","type":"Slider"},{"attributes":{"children":[{"id":"a08ef602-2510-4b28-8d12-0f34d0765881","type":"WidgetBox"},{"id":"010102e9-95fa-464f-81d7-7684fdeb39dc","subtype":"Figure","type":"Plot"}]},"id":"d3f6c4c0-5b38-4750-8311-53ef224eb7b7","type":"Column"}],"root_ids":["d3f6c4c0-5b38-4750-8311-53ef224eb7b7"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"777984a0-7a94-4d43-9dba-81656cfb7829","roots":{"d3f6c4c0-5b38-4750-8311-53ef224eb7b7":"7481d534-8ad4-491c-82b7-28ceb9c6a8ab"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();