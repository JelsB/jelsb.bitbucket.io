(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("db9f563b-a856-4071-8079-b2b47b3079b7");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'db9f563b-a856-4071-8079-b2b47b3079b7' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"919e7bfe-22c0-4aa7-8ca9-34ddb6085852":{"roots":{"references":[{"attributes":{},"id":"0c287873-3f18-4b14-afb3-d59a15b9f8c4","type":"Selection"},{"attributes":{"callback":null},"id":"5b23fe5b-f70b-4b05-8cd0-bb12c38ff5f6","type":"Range1d"},{"attributes":{"args":{"source":{"id":"1823c6a6-ebff-4526-895a-13d9e3f71780","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"16ceaaac-058f-4e13-b1d4-a09bbd5479e3","type":"CustomJS"},{"attributes":{},"id":"02558edb-5e21-40ec-a24b-c72ce6acf2e6","type":"BasicTickFormatter"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"afbc7df5-c943-4aa5-80b0-0bffbf56f695","type":"Toolbar"},{"attributes":{"formatter":{"id":"02558edb-5e21-40ec-a24b-c72ce6acf2e6","type":"BasicTickFormatter"},"plot":{"id":"8359ee40-f475-4a04-986e-801d8f4e115e","subtype":"Figure","type":"Plot"},"ticker":{"id":"938952ee-983f-4c6c-ac26-df2c465f30b9","type":"BasicTicker"},"visible":false},"id":"63528337-cb35-4810-852d-d4f264c8bde6","type":"LinearAxis"},{"attributes":{},"id":"42897a1f-7b2c-47bc-994f-02ac17535040","type":"BasicTicker"},{"attributes":{"data_source":{"id":"1823c6a6-ebff-4526-895a-13d9e3f71780","type":"ColumnDataSource"},"glyph":{"id":"5a83e93f-4dcd-4797-981b-706a68656f24","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"e1ac0391-8335-4410-b50a-7631a4273021","type":"ImageURL"},"selection_glyph":null,"view":{"id":"5ece3ded-4027-479c-a35e-3b0af3272325","type":"CDSView"}},"id":"c0b3eb2d-d89d-486d-8455-71cba62af574","type":"GlyphRenderer"},{"attributes":{},"id":"91549227-acf4-4e64-8c9b-b74a6328a76b","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"e1ac0391-8335-4410-b50a-7631a4273021","type":"ImageURL"},{"attributes":{"children":[{"id":"0bff0fdb-4d0e-433f-8e94-2dbac8fdc391","type":"WidgetBox"},{"id":"8359ee40-f475-4a04-986e-801d8f4e115e","subtype":"Figure","type":"Plot"}]},"id":"0d94e372-e4be-4185-bd03-9ce9633ea246","type":"Column"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG3O3_000.png"],"url_num":["000"]},"selected":{"id":"0c287873-3f18-4b14-afb3-d59a15b9f8c4","type":"Selection"},"selection_policy":{"id":"d587fa23-88f9-4e6d-ae8e-57673ba66999","type":"UnionRenderers"}},"id":"1823c6a6-ebff-4526-895a-13d9e3f71780","type":"ColumnDataSource"},{"attributes":{"callback":null},"id":"19068aad-9d69-4149-83f2-79a995a233b2","type":"Range1d"},{"attributes":{"callback":{"id":"16ceaaac-058f-4e13-b1d4-a09bbd5479e3","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"809738d9-01ae-45d7-9c43-05cd1bc82893","type":"Slider"},{"attributes":{},"id":"eae73b68-7988-4435-b0df-2e2ed7a7631c","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"5a83e93f-4dcd-4797-981b-706a68656f24","type":"ImageURL"},{"attributes":{"formatter":{"id":"eae73b68-7988-4435-b0df-2e2ed7a7631c","type":"BasicTickFormatter"},"plot":{"id":"8359ee40-f475-4a04-986e-801d8f4e115e","subtype":"Figure","type":"Plot"},"ticker":{"id":"42897a1f-7b2c-47bc-994f-02ac17535040","type":"BasicTicker"},"visible":false},"id":"963d7877-c802-4696-8f80-af23ecf070bf","type":"LinearAxis"},{"attributes":{"below":[{"id":"963d7877-c802-4696-8f80-af23ecf070bf","type":"LinearAxis"}],"left":[{"id":"63528337-cb35-4810-852d-d4f264c8bde6","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"963d7877-c802-4696-8f80-af23ecf070bf","type":"LinearAxis"},{"id":"83983607-ea27-475c-bf56-2d5fc15601a5","type":"Grid"},{"id":"63528337-cb35-4810-852d-d4f264c8bde6","type":"LinearAxis"},{"id":"3a2f682e-b561-4b4b-bdb8-118d821a6c36","type":"Grid"},{"id":"c0b3eb2d-d89d-486d-8455-71cba62af574","type":"GlyphRenderer"}],"title":{"id":"de41ffed-a3e8-41af-acb0-20463387d853","type":"Title"},"toolbar":{"id":"afbc7df5-c943-4aa5-80b0-0bffbf56f695","type":"Toolbar"},"x_range":{"id":"5b23fe5b-f70b-4b05-8cd0-bb12c38ff5f6","type":"Range1d"},"x_scale":{"id":"6bb1d810-68c0-4c1c-9f57-fba3465afb15","type":"LinearScale"},"y_range":{"id":"19068aad-9d69-4149-83f2-79a995a233b2","type":"Range1d"},"y_scale":{"id":"91549227-acf4-4e64-8c9b-b74a6328a76b","type":"LinearScale"}},"id":"8359ee40-f475-4a04-986e-801d8f4e115e","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"6bb1d810-68c0-4c1c-9f57-fba3465afb15","type":"LinearScale"},{"attributes":{"plot":{"id":"8359ee40-f475-4a04-986e-801d8f4e115e","subtype":"Figure","type":"Plot"},"ticker":{"id":"42897a1f-7b2c-47bc-994f-02ac17535040","type":"BasicTicker"},"visible":false},"id":"83983607-ea27-475c-bf56-2d5fc15601a5","type":"Grid"},{"attributes":{"source":{"id":"1823c6a6-ebff-4526-895a-13d9e3f71780","type":"ColumnDataSource"}},"id":"5ece3ded-4027-479c-a35e-3b0af3272325","type":"CDSView"},{"attributes":{"dimension":1,"plot":{"id":"8359ee40-f475-4a04-986e-801d8f4e115e","subtype":"Figure","type":"Plot"},"ticker":{"id":"938952ee-983f-4c6c-ac26-df2c465f30b9","type":"BasicTicker"},"visible":false},"id":"3a2f682e-b561-4b4b-bdb8-118d821a6c36","type":"Grid"},{"attributes":{"plot":null,"text":""},"id":"de41ffed-a3e8-41af-acb0-20463387d853","type":"Title"},{"attributes":{"children":[{"id":"809738d9-01ae-45d7-9c43-05cd1bc82893","type":"Slider"}]},"id":"0bff0fdb-4d0e-433f-8e94-2dbac8fdc391","type":"WidgetBox"},{"attributes":{},"id":"938952ee-983f-4c6c-ac26-df2c465f30b9","type":"BasicTicker"},{"attributes":{},"id":"d587fa23-88f9-4e6d-ae8e-57673ba66999","type":"UnionRenderers"}],"root_ids":["0d94e372-e4be-4185-bd03-9ce9633ea246"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"919e7bfe-22c0-4aa7-8ca9-34ddb6085852","roots":{"0d94e372-e4be-4185-bd03-9ce9633ea246":"db9f563b-a856-4071-8079-b2b47b3079b7"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();