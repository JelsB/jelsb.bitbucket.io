(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("8346d5fa-455d-484d-a5b9-9d51f3fe51b8");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '8346d5fa-455d-484d-a5b9-9d51f3fe51b8' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"95bac897-c33f-4f48-8453-2bc8bb296d7a":{"roots":{"references":[{"attributes":{},"id":"229f536a-8c9d-4dda-a5a0-23993fe2fcf1","type":"BasicTickFormatter"},{"attributes":{"formatter":{"id":"5cb471e6-2da2-42e7-9d23-a7c5768d0e8b","type":"BasicTickFormatter"},"plot":{"id":"e239e5b6-0897-41d5-93b5-f11eb47a4486","subtype":"Figure","type":"Plot"},"ticker":{"id":"d7dcb03f-181b-4394-a434-ad5f8073c142","type":"BasicTicker"},"visible":false},"id":"ec9ca180-dc18-4a8b-b085-ef87a343ca9e","type":"LinearAxis"},{"attributes":{"source":{"id":"c07b7c69-aa28-4845-832a-933b423e328a","type":"ColumnDataSource"}},"id":"ec1ad23c-0bbd-4656-9da9-44f9db6c1bd9","type":"CDSView"},{"attributes":{"plot":{"id":"e239e5b6-0897-41d5-93b5-f11eb47a4486","subtype":"Figure","type":"Plot"},"ticker":{"id":"d7dcb03f-181b-4394-a434-ad5f8073c142","type":"BasicTicker"},"visible":false},"id":"38c24da9-ab9f-420d-86e4-b85bacf8f17c","type":"Grid"},{"attributes":{"callback":null},"id":"122dd675-9a23-42a2-88c3-4c77289d37fb","type":"Range1d"},{"attributes":{},"id":"d7dcb03f-181b-4394-a434-ad5f8073c142","type":"BasicTicker"},{"attributes":{},"id":"838e517a-34cc-4ff2-a651-3a423c83437a","type":"LinearScale"},{"attributes":{},"id":"8b39902f-ca6d-4735-9924-d60e64f46d12","type":"UnionRenderers"},{"attributes":{},"id":"5cb471e6-2da2-42e7-9d23-a7c5768d0e8b","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"9b83b7ae-40cc-408b-ae80-aaff6f64101e","type":"WidgetBox"},{"id":"e239e5b6-0897-41d5-93b5-f11eb47a4486","subtype":"Figure","type":"Plot"}]},"id":"5cd8f180-104a-4ef5-9a8a-8a4acf986943","type":"Column"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"1f9e77b7-3b04-4096-b06e-c46da160629d","type":"ImageURL"},{"attributes":{"children":[{"id":"af32f458-8e77-4454-8909-cfe767b03442","type":"Slider"}]},"id":"9b83b7ae-40cc-408b-ae80-aaff6f64101e","type":"WidgetBox"},{"attributes":{},"id":"92addb67-6ec7-4af9-bcee-bb090642b904","type":"BasicTicker"},{"attributes":{},"id":"a78685c5-2f5b-474b-a5a4-955a38df5772","type":"Selection"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"c908606d-0e98-4ee0-8289-72dc134be4d7","type":"Toolbar"},{"attributes":{"dimension":1,"plot":{"id":"e239e5b6-0897-41d5-93b5-f11eb47a4486","subtype":"Figure","type":"Plot"},"ticker":{"id":"92addb67-6ec7-4af9-bcee-bb090642b904","type":"BasicTicker"},"visible":false},"id":"f53d3204-e63a-4bf9-b6a2-dd1b0ed370e3","type":"Grid"},{"attributes":{"callback":{"id":"668dbcf4-5f06-4709-95e5-af78a917e6c3","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"af32f458-8e77-4454-8909-cfe767b03442","type":"Slider"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"181a9d41-7403-4f52-8109-d8a3b636aeba","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"c07b7c69-aa28-4845-832a-933b423e328a","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"668dbcf4-5f06-4709-95e5-af78a917e6c3","type":"CustomJS"},{"attributes":{"plot":null,"text":""},"id":"3bfa8aff-3088-4b53-ac28-c47b00b0ab9e","type":"Title"},{"attributes":{"callback":null},"id":"6e239173-b52d-4e05-a648-4eeea4599745","type":"Range1d"},{"attributes":{"data_source":{"id":"c07b7c69-aa28-4845-832a-933b423e328a","type":"ColumnDataSource"},"glyph":{"id":"181a9d41-7403-4f52-8109-d8a3b636aeba","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"1f9e77b7-3b04-4096-b06e-c46da160629d","type":"ImageURL"},"selection_glyph":null,"view":{"id":"ec1ad23c-0bbd-4656-9da9-44f9db6c1bd9","type":"CDSView"}},"id":"2468ff4c-724c-4907-ae13-2a5ac5706b96","type":"GlyphRenderer"},{"attributes":{},"id":"21ee4481-9c0c-4858-b3c3-e424a36b9d12","type":"LinearScale"},{"attributes":{"formatter":{"id":"229f536a-8c9d-4dda-a5a0-23993fe2fcf1","type":"BasicTickFormatter"},"plot":{"id":"e239e5b6-0897-41d5-93b5-f11eb47a4486","subtype":"Figure","type":"Plot"},"ticker":{"id":"92addb67-6ec7-4af9-bcee-bb090642b904","type":"BasicTicker"},"visible":false},"id":"d083b1a4-3e9d-4e15-98c1-2bfeb17c6b63","type":"LinearAxis"},{"attributes":{"below":[{"id":"ec9ca180-dc18-4a8b-b085-ef87a343ca9e","type":"LinearAxis"}],"left":[{"id":"d083b1a4-3e9d-4e15-98c1-2bfeb17c6b63","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"ec9ca180-dc18-4a8b-b085-ef87a343ca9e","type":"LinearAxis"},{"id":"38c24da9-ab9f-420d-86e4-b85bacf8f17c","type":"Grid"},{"id":"d083b1a4-3e9d-4e15-98c1-2bfeb17c6b63","type":"LinearAxis"},{"id":"f53d3204-e63a-4bf9-b6a2-dd1b0ed370e3","type":"Grid"},{"id":"2468ff4c-724c-4907-ae13-2a5ac5706b96","type":"GlyphRenderer"}],"title":{"id":"3bfa8aff-3088-4b53-ac28-c47b00b0ab9e","type":"Title"},"toolbar":{"id":"c908606d-0e98-4ee0-8289-72dc134be4d7","type":"Toolbar"},"x_range":{"id":"6e239173-b52d-4e05-a648-4eeea4599745","type":"Range1d"},"x_scale":{"id":"21ee4481-9c0c-4858-b3c3-e424a36b9d12","type":"LinearScale"},"y_range":{"id":"122dd675-9a23-42a2-88c3-4c77289d37fb","type":"Range1d"},"y_scale":{"id":"838e517a-34cc-4ff2-a651-3a423c83437a","type":"LinearScale"}},"id":"e239e5b6-0897-41d5-93b5-f11eb47a4486","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI6O6_000.png"],"url_num":["000"]},"selected":{"id":"a78685c5-2f5b-474b-a5a4-955a38df5772","type":"Selection"},"selection_policy":{"id":"8b39902f-ca6d-4735-9924-d60e64f46d12","type":"UnionRenderers"}},"id":"c07b7c69-aa28-4845-832a-933b423e328a","type":"ColumnDataSource"}],"root_ids":["5cd8f180-104a-4ef5-9a8a-8a4acf986943"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"95bac897-c33f-4f48-8453-2bc8bb296d7a","roots":{"5cd8f180-104a-4ef5-9a8a-8a4acf986943":"8346d5fa-455d-484d-a5b9-9d51f3fe51b8"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();