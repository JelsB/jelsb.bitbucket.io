(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("f455782c-1960-43e0-b8e6-4c7315010345");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'f455782c-1960-43e0-b8e6-4c7315010345' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"9b67ddfc-dbb0-451b-91db-f3471bb4e40f":{"roots":{"references":[{"attributes":{},"id":"d124916e-2e7b-4082-abe1-d8d7cd87158b","type":"LinearScale"},{"attributes":{"plot":null,"text":""},"id":"6e38c88c-3682-41d3-b120-78a95e15e93e","type":"Title"},{"attributes":{"data_source":{"id":"421cdb17-3aee-4ac0-bd61-9e0f49b0c794","type":"ColumnDataSource"},"glyph":{"id":"db7662a2-6268-4707-836b-67b7ae2f59b6","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"21bc40fc-c220-46a3-8369-2aac17f2a33e","type":"ImageURL"},"selection_glyph":null,"view":{"id":"a018b848-d207-433b-8cad-97e2c56429da","type":"CDSView"}},"id":"d3009300-68e9-4778-b0f7-e31266655e29","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"21bc40fc-c220-46a3-8369-2aac17f2a33e","type":"ImageURL"},{"attributes":{},"id":"a501b645-9e2a-45e4-af0f-7dbd71b50478","type":"Selection"},{"attributes":{"children":[{"id":"adb3f85b-ca79-4593-8927-828fa2306b45","type":"Slider"}]},"id":"2adac7f3-8124-40f8-8ac9-5136ba9ec5ad","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerTI9O18_000.png"],"url_num":["000"]},"selected":{"id":"a501b645-9e2a-45e4-af0f-7dbd71b50478","type":"Selection"},"selection_policy":{"id":"8f936d5f-a681-4461-a201-b2ba08c151a1","type":"UnionRenderers"}},"id":"421cdb17-3aee-4ac0-bd61-9e0f49b0c794","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"eccda25f-3050-4366-b078-092c4cf470f9","type":"LinearAxis"}],"left":[{"id":"7d9a29d4-dd06-4ee5-b97b-1e942cbeceb4","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"eccda25f-3050-4366-b078-092c4cf470f9","type":"LinearAxis"},{"id":"8fdb19cb-0a2a-4967-9195-d1d265e441ce","type":"Grid"},{"id":"7d9a29d4-dd06-4ee5-b97b-1e942cbeceb4","type":"LinearAxis"},{"id":"5d483b14-2f46-424e-b3f0-f812c5bcc107","type":"Grid"},{"id":"d3009300-68e9-4778-b0f7-e31266655e29","type":"GlyphRenderer"}],"title":{"id":"6e38c88c-3682-41d3-b120-78a95e15e93e","type":"Title"},"toolbar":{"id":"bf3544ce-cb54-4e18-be37-9b5352c0e983","type":"Toolbar"},"x_range":{"id":"9fe2baba-61eb-4b72-99b2-848bff392ea2","type":"Range1d"},"x_scale":{"id":"b0de6b56-2c41-471a-90af-9700cd60b6dd","type":"LinearScale"},"y_range":{"id":"655ae345-d731-4544-8962-22916bf4d369","type":"Range1d"},"y_scale":{"id":"d124916e-2e7b-4082-abe1-d8d7cd87158b","type":"LinearScale"}},"id":"2f423c9b-591a-4fcc-9eb5-5730bbc7322d","subtype":"Figure","type":"Plot"},{"attributes":{"children":[{"id":"2adac7f3-8124-40f8-8ac9-5136ba9ec5ad","type":"WidgetBox"},{"id":"2f423c9b-591a-4fcc-9eb5-5730bbc7322d","subtype":"Figure","type":"Plot"}]},"id":"6df0d193-e051-4ce9-9c78-ff939317abc2","type":"Column"},{"attributes":{"source":{"id":"421cdb17-3aee-4ac0-bd61-9e0f49b0c794","type":"ColumnDataSource"}},"id":"a018b848-d207-433b-8cad-97e2c56429da","type":"CDSView"},{"attributes":{"args":{"source":{"id":"421cdb17-3aee-4ac0-bd61-9e0f49b0c794","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"61c8a15d-e736-4b93-9087-417f528d422b","type":"CustomJS"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"db7662a2-6268-4707-836b-67b7ae2f59b6","type":"ImageURL"},{"attributes":{"callback":null},"id":"655ae345-d731-4544-8962-22916bf4d369","type":"Range1d"},{"attributes":{"callback":{"id":"61c8a15d-e736-4b93-9087-417f528d422b","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"adb3f85b-ca79-4593-8927-828fa2306b45","type":"Slider"},{"attributes":{"formatter":{"id":"7a2ca3f5-6a49-4aef-92fe-72884549b9a5","type":"BasicTickFormatter"},"plot":{"id":"2f423c9b-591a-4fcc-9eb5-5730bbc7322d","subtype":"Figure","type":"Plot"},"ticker":{"id":"97e984cf-6df4-4371-846c-8d74d4a73d73","type":"BasicTicker"},"visible":false},"id":"7d9a29d4-dd06-4ee5-b97b-1e942cbeceb4","type":"LinearAxis"},{"attributes":{"formatter":{"id":"99033679-0106-4317-a329-b4f40b0f6635","type":"BasicTickFormatter"},"plot":{"id":"2f423c9b-591a-4fcc-9eb5-5730bbc7322d","subtype":"Figure","type":"Plot"},"ticker":{"id":"4600e7de-1a29-4371-a6e4-6f55412b0fbb","type":"BasicTicker"},"visible":false},"id":"eccda25f-3050-4366-b078-092c4cf470f9","type":"LinearAxis"},{"attributes":{"plot":{"id":"2f423c9b-591a-4fcc-9eb5-5730bbc7322d","subtype":"Figure","type":"Plot"},"ticker":{"id":"4600e7de-1a29-4371-a6e4-6f55412b0fbb","type":"BasicTicker"},"visible":false},"id":"8fdb19cb-0a2a-4967-9195-d1d265e441ce","type":"Grid"},{"attributes":{},"id":"97e984cf-6df4-4371-846c-8d74d4a73d73","type":"BasicTicker"},{"attributes":{},"id":"b0de6b56-2c41-471a-90af-9700cd60b6dd","type":"LinearScale"},{"attributes":{},"id":"8f936d5f-a681-4461-a201-b2ba08c151a1","type":"UnionRenderers"},{"attributes":{"dimension":1,"plot":{"id":"2f423c9b-591a-4fcc-9eb5-5730bbc7322d","subtype":"Figure","type":"Plot"},"ticker":{"id":"97e984cf-6df4-4371-846c-8d74d4a73d73","type":"BasicTicker"},"visible":false},"id":"5d483b14-2f46-424e-b3f0-f812c5bcc107","type":"Grid"},{"attributes":{},"id":"99033679-0106-4317-a329-b4f40b0f6635","type":"BasicTickFormatter"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"bf3544ce-cb54-4e18-be37-9b5352c0e983","type":"Toolbar"},{"attributes":{},"id":"7a2ca3f5-6a49-4aef-92fe-72884549b9a5","type":"BasicTickFormatter"},{"attributes":{"callback":null},"id":"9fe2baba-61eb-4b72-99b2-848bff392ea2","type":"Range1d"},{"attributes":{},"id":"4600e7de-1a29-4371-a6e4-6f55412b0fbb","type":"BasicTicker"}],"root_ids":["6df0d193-e051-4ce9-9c78-ff939317abc2"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"9b67ddfc-dbb0-451b-91db-f3471bb4e40f","roots":{"6df0d193-e051-4ce9-9c78-ff939317abc2":"f455782c-1960-43e0-b8e6-4c7315010345"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();