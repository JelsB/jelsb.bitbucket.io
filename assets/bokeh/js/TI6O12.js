(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("bbd3b5f6-1654-425b-969f-f8a904f454e2");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'bbd3b5f6-1654-425b-969f-f8a904f454e2' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"09cd8e02-6b46-474b-b0c5-ddff70c9784e":{"roots":{"references":[{"attributes":{},"id":"554c1bae-2e02-495e-bcac-be7275c679c7","type":"LinearScale"},{"attributes":{"callback":{"id":"74131156-22db-47c2-8ec4-37623c53bbd3","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c7d77d25-848f-4159-901d-e20e9a26fe24","type":"Slider"},{"attributes":{"source":{"id":"6c52be74-238e-4758-8502-f2342eacb12d","type":"ColumnDataSource"}},"id":"9b0d8611-5b6a-4d7a-ab62-9e8b8140b527","type":"CDSView"},{"attributes":{"args":{"source":{"id":"6c52be74-238e-4758-8502-f2342eacb12d","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"74131156-22db-47c2-8ec4-37623c53bbd3","type":"CustomJS"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"93e394aa-d9d8-4340-8551-920ee4c930ac","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"c7f80e51-b6f8-4bdc-885c-75debd52a23d","type":"ImageURL"},{"attributes":{},"id":"ac41b1d0-8e15-405e-8b90-ec9c71e68b4f","type":"BasicTicker"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"827e2209-a15a-44b8-a11d-c402e6b4c761","type":"Toolbar"},{"attributes":{},"id":"d7b18268-6d5c-4ed3-9701-40bb7ce90a56","type":"Selection"},{"attributes":{"callback":null},"id":"9d1c2d09-9888-4056-92f1-bebcf24c936d","type":"Range1d"},{"attributes":{"plot":null,"text":""},"id":"b78b8eb4-57db-454f-bbeb-614750ef2fd8","type":"Title"},{"attributes":{"formatter":{"id":"799766d4-93ea-4529-a926-b45002b22149","type":"BasicTickFormatter"},"plot":{"id":"c55177ac-d6bf-4974-a7a3-4a9d6e9688d6","subtype":"Figure","type":"Plot"},"ticker":{"id":"c9cb8714-b210-4bde-b591-d87cb44b1677","type":"BasicTicker"},"visible":false},"id":"7bd19a53-6b76-4d71-a1a3-81fd9a665ed5","type":"LinearAxis"},{"attributes":{"children":[{"id":"45d9355c-451b-4c17-8964-709d5cf46d3d","type":"WidgetBox"},{"id":"c55177ac-d6bf-4974-a7a3-4a9d6e9688d6","subtype":"Figure","type":"Plot"}]},"id":"78498b6c-302f-458c-b8c9-ec8bdce955a5","type":"Column"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI6O12_000.png"],"url_num":["000"]},"selected":{"id":"d7b18268-6d5c-4ed3-9701-40bb7ce90a56","type":"Selection"},"selection_policy":{"id":"7f09f977-d355-4108-86c9-9b274778a561","type":"UnionRenderers"}},"id":"6c52be74-238e-4758-8502-f2342eacb12d","type":"ColumnDataSource"},{"attributes":{"below":[{"id":"488fefb4-5c29-41ae-a714-24b545775f5e","type":"LinearAxis"}],"left":[{"id":"7bd19a53-6b76-4d71-a1a3-81fd9a665ed5","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"488fefb4-5c29-41ae-a714-24b545775f5e","type":"LinearAxis"},{"id":"f9f28858-7370-44b5-8f15-43fb645edb67","type":"Grid"},{"id":"7bd19a53-6b76-4d71-a1a3-81fd9a665ed5","type":"LinearAxis"},{"id":"1d6f4572-3ee2-4d49-a2a5-bca08daef259","type":"Grid"},{"id":"5985bf00-ad63-4794-a0cc-57fa32ea0c9b","type":"GlyphRenderer"}],"title":{"id":"b78b8eb4-57db-454f-bbeb-614750ef2fd8","type":"Title"},"toolbar":{"id":"827e2209-a15a-44b8-a11d-c402e6b4c761","type":"Toolbar"},"x_range":{"id":"9788874c-24d8-4c91-97f0-ae39bb9d1559","type":"Range1d"},"x_scale":{"id":"554c1bae-2e02-495e-bcac-be7275c679c7","type":"LinearScale"},"y_range":{"id":"9d1c2d09-9888-4056-92f1-bebcf24c936d","type":"Range1d"},"y_scale":{"id":"2a52ba16-d269-411f-b8d5-7de2630f0363","type":"LinearScale"}},"id":"c55177ac-d6bf-4974-a7a3-4a9d6e9688d6","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"799766d4-93ea-4529-a926-b45002b22149","type":"BasicTickFormatter"},{"attributes":{},"id":"2a52ba16-d269-411f-b8d5-7de2630f0363","type":"LinearScale"},{"attributes":{},"id":"9f3f543e-787a-4c27-a336-adf2aee140da","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"c55177ac-d6bf-4974-a7a3-4a9d6e9688d6","subtype":"Figure","type":"Plot"},"ticker":{"id":"ac41b1d0-8e15-405e-8b90-ec9c71e68b4f","type":"BasicTicker"},"visible":false},"id":"f9f28858-7370-44b5-8f15-43fb645edb67","type":"Grid"},{"attributes":{"formatter":{"id":"9f3f543e-787a-4c27-a336-adf2aee140da","type":"BasicTickFormatter"},"plot":{"id":"c55177ac-d6bf-4974-a7a3-4a9d6e9688d6","subtype":"Figure","type":"Plot"},"ticker":{"id":"ac41b1d0-8e15-405e-8b90-ec9c71e68b4f","type":"BasicTicker"},"visible":false},"id":"488fefb4-5c29-41ae-a714-24b545775f5e","type":"LinearAxis"},{"attributes":{},"id":"c9cb8714-b210-4bde-b591-d87cb44b1677","type":"BasicTicker"},{"attributes":{"data_source":{"id":"6c52be74-238e-4758-8502-f2342eacb12d","type":"ColumnDataSource"},"glyph":{"id":"93e394aa-d9d8-4340-8551-920ee4c930ac","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"c7f80e51-b6f8-4bdc-885c-75debd52a23d","type":"ImageURL"},"selection_glyph":null,"view":{"id":"9b0d8611-5b6a-4d7a-ab62-9e8b8140b527","type":"CDSView"}},"id":"5985bf00-ad63-4794-a0cc-57fa32ea0c9b","type":"GlyphRenderer"},{"attributes":{},"id":"7f09f977-d355-4108-86c9-9b274778a561","type":"UnionRenderers"},{"attributes":{"dimension":1,"plot":{"id":"c55177ac-d6bf-4974-a7a3-4a9d6e9688d6","subtype":"Figure","type":"Plot"},"ticker":{"id":"c9cb8714-b210-4bde-b591-d87cb44b1677","type":"BasicTicker"},"visible":false},"id":"1d6f4572-3ee2-4d49-a2a5-bca08daef259","type":"Grid"},{"attributes":{"callback":null},"id":"9788874c-24d8-4c91-97f0-ae39bb9d1559","type":"Range1d"},{"attributes":{"children":[{"id":"c7d77d25-848f-4159-901d-e20e9a26fe24","type":"Slider"}]},"id":"45d9355c-451b-4c17-8964-709d5cf46d3d","type":"WidgetBox"}],"root_ids":["78498b6c-302f-458c-b8c9-ec8bdce955a5"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"09cd8e02-6b46-474b-b0c5-ddff70c9784e","roots":{"78498b6c-302f-458c-b8c9-ec8bdce955a5":"bbd3b5f6-1654-425b-969f-f8a904f454e2"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();