(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("f152157d-a6d7-4b04-8a72-657bf7845a8d");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'f152157d-a6d7-4b04-8a72-657bf7845a8d' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"2eaed911-4c2f-4dd0-8d2d-04b73172c798":{"roots":{"references":[{"attributes":{"data_source":{"id":"fd51cefe-9ee3-4270-975f-0404f1817ace","type":"ColumnDataSource"},"glyph":{"id":"85ffa051-6a13-494f-af59-3afaafb33a0f","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"f0b10ea0-03d8-49ec-8fff-79f5fd544bfb","type":"ImageURL"},"selection_glyph":null,"view":{"id":"f8b6f290-ba16-4821-90c4-43a8a4ac8315","type":"CDSView"}},"id":"a9e945a7-9a31-48a0-b56b-bbf1a9deb5f6","type":"GlyphRenderer"},{"attributes":{},"id":"796adf7f-7d53-4ff0-abd1-77d158299261","type":"LinearScale"},{"attributes":{},"id":"c9ac95d0-fe98-4d47-9f17-63cfe1051b9e","type":"BasicTickFormatter"},{"attributes":{},"id":"1708c858-c9d1-439e-b3ea-03108e75d492","type":"BasicTicker"},{"attributes":{"callback":null},"id":"64cb8c0d-c8ee-476d-b0fd-266db12945d4","type":"Range1d"},{"attributes":{"source":{"id":"fd51cefe-9ee3-4270-975f-0404f1817ace","type":"ColumnDataSource"}},"id":"f8b6f290-ba16-4821-90c4-43a8a4ac8315","type":"CDSView"},{"attributes":{"args":{"source":{"id":"fd51cefe-9ee3-4270-975f-0404f1817ace","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"c103a47f-26d6-4e53-a7a4-464715341690","type":"CustomJS"},{"attributes":{"callback":{"id":"c103a47f-26d6-4e53-a7a4-464715341690","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"cb19b309-d781-44b7-afba-41f6a2416280","type":"Slider"},{"attributes":{"children":[{"id":"6893bf94-380d-4616-924e-1201d2713516","type":"WidgetBox"},{"id":"6f246bf1-50fd-4924-bbe3-128bb4661072","subtype":"Figure","type":"Plot"}]},"id":"d465df1a-770d-488e-9633-39f723fa35c4","type":"Column"},{"attributes":{},"id":"673e2aa9-8691-4781-80d8-61b51ce994ad","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"85ffa051-6a13-494f-af59-3afaafb33a0f","type":"ImageURL"},{"attributes":{"children":[{"id":"cb19b309-d781-44b7-afba-41f6a2416280","type":"Slider"}]},"id":"6893bf94-380d-4616-924e-1201d2713516","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/MG6O6_000.png"],"url_num":["000"]},"selected":{"id":"13b343ba-d7bb-456c-984f-71068af1a6ce","type":"Selection"},"selection_policy":{"id":"b282743f-b733-4271-a6fc-b6066afd4eb8","type":"UnionRenderers"}},"id":"fd51cefe-9ee3-4270-975f-0404f1817ace","type":"ColumnDataSource"},{"attributes":{},"id":"b282743f-b733-4271-a6fc-b6066afd4eb8","type":"UnionRenderers"},{"attributes":{},"id":"13b343ba-d7bb-456c-984f-71068af1a6ce","type":"Selection"},{"attributes":{"below":[{"id":"64f5fa80-e7ad-4e81-8084-25a8fae64dd7","type":"LinearAxis"}],"left":[{"id":"b4c1c5f6-418b-4ade-96d8-19dff8abdc19","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"64f5fa80-e7ad-4e81-8084-25a8fae64dd7","type":"LinearAxis"},{"id":"bc601ff4-ddf7-4b7b-8993-a39458416564","type":"Grid"},{"id":"b4c1c5f6-418b-4ade-96d8-19dff8abdc19","type":"LinearAxis"},{"id":"134cd539-1ff3-480f-9c22-6cf60b9d810b","type":"Grid"},{"id":"a9e945a7-9a31-48a0-b56b-bbf1a9deb5f6","type":"GlyphRenderer"}],"title":{"id":"1edd852a-e115-4564-9c68-8c10538405e3","type":"Title"},"toolbar":{"id":"1e0a6797-afad-4513-9d03-b7b3383552a8","type":"Toolbar"},"x_range":{"id":"64cb8c0d-c8ee-476d-b0fd-266db12945d4","type":"Range1d"},"x_scale":{"id":"6659afc8-ef92-4535-8228-ab3f2607a457","type":"LinearScale"},"y_range":{"id":"050a451d-8f6c-45d1-92fe-dbe2a02137f8","type":"Range1d"},"y_scale":{"id":"796adf7f-7d53-4ff0-abd1-77d158299261","type":"LinearScale"}},"id":"6f246bf1-50fd-4924-bbe3-128bb4661072","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"050a451d-8f6c-45d1-92fe-dbe2a02137f8","type":"Range1d"},{"attributes":{"formatter":{"id":"673e2aa9-8691-4781-80d8-61b51ce994ad","type":"BasicTickFormatter"},"plot":{"id":"6f246bf1-50fd-4924-bbe3-128bb4661072","subtype":"Figure","type":"Plot"},"ticker":{"id":"8df6bde7-f990-495a-8f75-1d0d7fe1935e","type":"BasicTicker"},"visible":false},"id":"64f5fa80-e7ad-4e81-8084-25a8fae64dd7","type":"LinearAxis"},{"attributes":{"dimension":1,"plot":{"id":"6f246bf1-50fd-4924-bbe3-128bb4661072","subtype":"Figure","type":"Plot"},"ticker":{"id":"1708c858-c9d1-439e-b3ea-03108e75d492","type":"BasicTicker"},"visible":false},"id":"134cd539-1ff3-480f-9c22-6cf60b9d810b","type":"Grid"},{"attributes":{},"id":"6659afc8-ef92-4535-8228-ab3f2607a457","type":"LinearScale"},{"attributes":{"plot":{"id":"6f246bf1-50fd-4924-bbe3-128bb4661072","subtype":"Figure","type":"Plot"},"ticker":{"id":"8df6bde7-f990-495a-8f75-1d0d7fe1935e","type":"BasicTicker"},"visible":false},"id":"bc601ff4-ddf7-4b7b-8993-a39458416564","type":"Grid"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"f0b10ea0-03d8-49ec-8fff-79f5fd544bfb","type":"ImageURL"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"1e0a6797-afad-4513-9d03-b7b3383552a8","type":"Toolbar"},{"attributes":{"formatter":{"id":"c9ac95d0-fe98-4d47-9f17-63cfe1051b9e","type":"BasicTickFormatter"},"plot":{"id":"6f246bf1-50fd-4924-bbe3-128bb4661072","subtype":"Figure","type":"Plot"},"ticker":{"id":"1708c858-c9d1-439e-b3ea-03108e75d492","type":"BasicTicker"},"visible":false},"id":"b4c1c5f6-418b-4ade-96d8-19dff8abdc19","type":"LinearAxis"},{"attributes":{},"id":"8df6bde7-f990-495a-8f75-1d0d7fe1935e","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"1edd852a-e115-4564-9c68-8c10538405e3","type":"Title"}],"root_ids":["d465df1a-770d-488e-9633-39f723fa35c4"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"2eaed911-4c2f-4dd0-8d2d-04b73172c798","roots":{"d465df1a-770d-488e-9633-39f723fa35c4":"f152157d-a6d7-4b04-8a72-657bf7845a8d"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();