(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("e5c8636a-4199-426c-90df-2e78d1be61cc");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'e5c8636a-4199-426c-90df-2e78d1be61cc' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"922cb150-ecce-4d92-8a7e-0912af08e948":{"roots":{"references":[{"attributes":{"callback":{"id":"9ec65c91-f346-479e-b687-a3dbfca7eae0","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"c97aebb7-893c-4853-aa33-f76c67dfc922","type":"Slider"},{"attributes":{"plot":null,"text":""},"id":"b23b8054-44a3-4552-8c7a-72bc7f5198c9","type":"Title"},{"attributes":{"formatter":{"id":"bcd1f9ee-1996-4135-9e93-fb3406e047a5","type":"BasicTickFormatter"},"plot":{"id":"c6f9b65b-68dd-447f-a327-f37ad48d6522","subtype":"Figure","type":"Plot"},"ticker":{"id":"60917a81-49da-426e-a635-b5c60953f6f7","type":"BasicTicker"},"visible":false},"id":"a3b16144-84f9-4f5f-86ec-a759349e4e3e","type":"LinearAxis"},{"attributes":{},"id":"faf05587-3a24-44d1-847e-4bc3e2baac16","type":"UnionRenderers"},{"attributes":{},"id":"ba47b622-6262-4788-b59e-02549d3473e2","type":"LinearScale"},{"attributes":{"source":{"id":"df01c59a-0a51-4c99-adff-a64a0194b939","type":"ColumnDataSource"}},"id":"33cdbae0-e067-43e0-8292-ea76f14235c2","type":"CDSView"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"a976e795-8172-46b9-a40f-ef9352bf297a","type":"ImageURL"},{"attributes":{},"id":"bcd1f9ee-1996-4135-9e93-fb3406e047a5","type":"BasicTickFormatter"},{"attributes":{"data_source":{"id":"df01c59a-0a51-4c99-adff-a64a0194b939","type":"ColumnDataSource"},"glyph":{"id":"be906c14-928c-42ff-97f1-97413c94a1e6","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"a976e795-8172-46b9-a40f-ef9352bf297a","type":"ImageURL"},"selection_glyph":null,"view":{"id":"33cdbae0-e067-43e0-8292-ea76f14235c2","type":"CDSView"}},"id":"54d456ab-1fdd-44c4-830d-49e24b588fb0","type":"GlyphRenderer"},{"attributes":{},"id":"00ad88ac-5016-4f9a-9632-b746f44b7439","type":"BasicTicker"},{"attributes":{"callback":null},"id":"5f7f35c8-ac61-4921-8427-b8cc178fa55b","type":"Range1d"},{"attributes":{"callback":null},"id":"b9800453-f8c0-4023-bb25-54f805cbf9c4","type":"Range1d"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI4O8_000.png"],"url_num":["000"]},"selected":{"id":"2d738ce5-8b12-44cb-b79b-9478dc464956","type":"Selection"},"selection_policy":{"id":"faf05587-3a24-44d1-847e-4bc3e2baac16","type":"UnionRenderers"}},"id":"df01c59a-0a51-4c99-adff-a64a0194b939","type":"ColumnDataSource"},{"attributes":{"formatter":{"id":"1ca3f673-2f04-4d16-9376-8f89a575c47c","type":"BasicTickFormatter"},"plot":{"id":"c6f9b65b-68dd-447f-a327-f37ad48d6522","subtype":"Figure","type":"Plot"},"ticker":{"id":"00ad88ac-5016-4f9a-9632-b746f44b7439","type":"BasicTicker"},"visible":false},"id":"5644c87c-82bf-451c-b64c-25bba2d0278b","type":"LinearAxis"},{"attributes":{"children":[{"id":"c97aebb7-893c-4853-aa33-f76c67dfc922","type":"Slider"}]},"id":"bef6c94d-cb60-457e-8099-de253623ebd8","type":"WidgetBox"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"be906c14-928c-42ff-97f1-97413c94a1e6","type":"ImageURL"},{"attributes":{"children":[{"id":"bef6c94d-cb60-457e-8099-de253623ebd8","type":"WidgetBox"},{"id":"c6f9b65b-68dd-447f-a327-f37ad48d6522","subtype":"Figure","type":"Plot"}]},"id":"5d299b4e-1b9a-4529-9c12-44392dc93949","type":"Column"},{"attributes":{"args":{"source":{"id":"df01c59a-0a51-4c99-adff-a64a0194b939","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"9ec65c91-f346-479e-b687-a3dbfca7eae0","type":"CustomJS"},{"attributes":{},"id":"60917a81-49da-426e-a635-b5c60953f6f7","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"c6f9b65b-68dd-447f-a327-f37ad48d6522","subtype":"Figure","type":"Plot"},"ticker":{"id":"00ad88ac-5016-4f9a-9632-b746f44b7439","type":"BasicTicker"},"visible":false},"id":"0ee59f59-955b-4646-aa4d-6ef09d894188","type":"Grid"},{"attributes":{},"id":"9f9970da-981f-495b-8a55-2ecb65f73c74","type":"LinearScale"},{"attributes":{"below":[{"id":"a3b16144-84f9-4f5f-86ec-a759349e4e3e","type":"LinearAxis"}],"left":[{"id":"5644c87c-82bf-451c-b64c-25bba2d0278b","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"a3b16144-84f9-4f5f-86ec-a759349e4e3e","type":"LinearAxis"},{"id":"9d18ab65-0b98-4364-8819-1612eb8700c1","type":"Grid"},{"id":"5644c87c-82bf-451c-b64c-25bba2d0278b","type":"LinearAxis"},{"id":"0ee59f59-955b-4646-aa4d-6ef09d894188","type":"Grid"},{"id":"54d456ab-1fdd-44c4-830d-49e24b588fb0","type":"GlyphRenderer"}],"title":{"id":"b23b8054-44a3-4552-8c7a-72bc7f5198c9","type":"Title"},"toolbar":{"id":"5fb1a222-15b7-49b3-9393-02036bb756d5","type":"Toolbar"},"x_range":{"id":"5f7f35c8-ac61-4921-8427-b8cc178fa55b","type":"Range1d"},"x_scale":{"id":"9f9970da-981f-495b-8a55-2ecb65f73c74","type":"LinearScale"},"y_range":{"id":"b9800453-f8c0-4023-bb25-54f805cbf9c4","type":"Range1d"},"y_scale":{"id":"ba47b622-6262-4788-b59e-02549d3473e2","type":"LinearScale"}},"id":"c6f9b65b-68dd-447f-a327-f37ad48d6522","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"2d738ce5-8b12-44cb-b79b-9478dc464956","type":"Selection"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"5fb1a222-15b7-49b3-9393-02036bb756d5","type":"Toolbar"},{"attributes":{},"id":"1ca3f673-2f04-4d16-9376-8f89a575c47c","type":"BasicTickFormatter"},{"attributes":{"plot":{"id":"c6f9b65b-68dd-447f-a327-f37ad48d6522","subtype":"Figure","type":"Plot"},"ticker":{"id":"60917a81-49da-426e-a635-b5c60953f6f7","type":"BasicTicker"},"visible":false},"id":"9d18ab65-0b98-4364-8819-1612eb8700c1","type":"Grid"}],"root_ids":["5d299b4e-1b9a-4529-9c12-44392dc93949"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"922cb150-ecce-4d92-8a7e-0912af08e948","roots":{"5d299b4e-1b9a-4529-9c12-44392dc93949":"e5c8636a-4199-426c-90df-2e78d1be61cc"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();