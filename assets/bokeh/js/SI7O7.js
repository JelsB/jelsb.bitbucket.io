(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("cdfa862b-87ec-4f3c-be24-3115a6251fcc");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'cdfa862b-87ec-4f3c-be24-3115a6251fcc' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"f1145d59-ff7c-433a-834d-e4596d17ad8e":{"roots":{"references":[{"attributes":{},"id":"cc2ff09f-8e43-401a-a82c-1af402aac53b","type":"LinearScale"},{"attributes":{"args":{"source":{"id":"4d79165c-1e31-47fe-b8b8-0f9bc1adc1f4","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"d079ff67-a19d-442d-a6ae-7fe2ce0f9507","type":"CustomJS"},{"attributes":{"formatter":{"id":"edbaa355-bc8a-4fe6-bd27-8065ca8e58ed","type":"BasicTickFormatter"},"plot":{"id":"9ed10e59-ded6-4fd1-8a99-f205152e52c0","subtype":"Figure","type":"Plot"},"ticker":{"id":"086fed18-909e-49b9-9e70-2def771ff1ab","type":"BasicTicker"},"visible":false},"id":"a8616cc1-7f3c-46b7-b0ed-2ddeba8961e0","type":"LinearAxis"},{"attributes":{"data_source":{"id":"4d79165c-1e31-47fe-b8b8-0f9bc1adc1f4","type":"ColumnDataSource"},"glyph":{"id":"30aa12ee-242e-4830-a2d3-f60080aa9f50","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"d426f302-ae09-4c10-baf3-9b4772e1eb46","type":"ImageURL"},"selection_glyph":null,"view":{"id":"469f0552-ff4f-4b1a-afb0-740f2df62b4b","type":"CDSView"}},"id":"ac892285-f42b-4f0a-ba5d-32e5a8b5142f","type":"GlyphRenderer"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"d426f302-ae09-4c10-baf3-9b4772e1eb46","type":"ImageURL"},{"attributes":{"source":{"id":"4d79165c-1e31-47fe-b8b8-0f9bc1adc1f4","type":"ColumnDataSource"}},"id":"469f0552-ff4f-4b1a-afb0-740f2df62b4b","type":"CDSView"},{"attributes":{},"id":"086fed18-909e-49b9-9e70-2def771ff1ab","type":"BasicTicker"},{"attributes":{"callback":null},"id":"23fe2f34-b967-433a-951b-c1289fc548ab","type":"Range1d"},{"attributes":{},"id":"c9aa5afa-57c7-4bb1-92ea-7cc6e8f9125c","type":"UnionRenderers"},{"attributes":{"dimension":1,"plot":{"id":"9ed10e59-ded6-4fd1-8a99-f205152e52c0","subtype":"Figure","type":"Plot"},"ticker":{"id":"086fed18-909e-49b9-9e70-2def771ff1ab","type":"BasicTicker"},"visible":false},"id":"f2fe3a7f-8b0c-4f76-b238-877be325073a","type":"Grid"},{"attributes":{"children":[{"id":"99954270-90b3-464b-9706-5fb8e04ceea2","type":"Slider"}]},"id":"fe3cd3a7-58ff-409b-9d91-1b137e47fece","type":"WidgetBox"},{"attributes":{},"id":"f06cfe28-9584-42d0-9d68-68fec8939e93","type":"BasicTickFormatter"},{"attributes":{"children":[{"id":"fe3cd3a7-58ff-409b-9d91-1b137e47fece","type":"WidgetBox"},{"id":"9ed10e59-ded6-4fd1-8a99-f205152e52c0","subtype":"Figure","type":"Plot"}]},"id":"4725b9c5-afbe-433e-9afe-f06ee1c0cdb5","type":"Column"},{"attributes":{"below":[{"id":"2ab77787-ef23-4bf3-9c1e-6d0e0aa93ace","type":"LinearAxis"}],"left":[{"id":"a8616cc1-7f3c-46b7-b0ed-2ddeba8961e0","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"2ab77787-ef23-4bf3-9c1e-6d0e0aa93ace","type":"LinearAxis"},{"id":"9539d695-b940-447b-b011-f25f9ac1bd8d","type":"Grid"},{"id":"a8616cc1-7f3c-46b7-b0ed-2ddeba8961e0","type":"LinearAxis"},{"id":"f2fe3a7f-8b0c-4f76-b238-877be325073a","type":"Grid"},{"id":"ac892285-f42b-4f0a-ba5d-32e5a8b5142f","type":"GlyphRenderer"}],"title":{"id":"e62d02f0-f6df-4171-ad4d-d907a80b26de","type":"Title"},"toolbar":{"id":"22778fb2-ecf9-439e-a7d6-510ae9abd94b","type":"Toolbar"},"x_range":{"id":"2985f3b8-5656-4aab-85d9-2ce0fbc80c9f","type":"Range1d"},"x_scale":{"id":"cc2ff09f-8e43-401a-a82c-1af402aac53b","type":"LinearScale"},"y_range":{"id":"23fe2f34-b967-433a-951b-c1289fc548ab","type":"Range1d"},"y_scale":{"id":"e5ee17aa-e142-4204-9725-35e64fce9ff6","type":"LinearScale"}},"id":"9ed10e59-ded6-4fd1-8a99-f205152e52c0","subtype":"Figure","type":"Plot"},{"attributes":{"plot":{"id":"9ed10e59-ded6-4fd1-8a99-f205152e52c0","subtype":"Figure","type":"Plot"},"ticker":{"id":"c022acb5-a355-4c0e-9e12-eddc3bf3cb37","type":"BasicTicker"},"visible":false},"id":"9539d695-b940-447b-b011-f25f9ac1bd8d","type":"Grid"},{"attributes":{},"id":"c022acb5-a355-4c0e-9e12-eddc3bf3cb37","type":"BasicTicker"},{"attributes":{"callback":null},"id":"2985f3b8-5656-4aab-85d9-2ce0fbc80c9f","type":"Range1d"},{"attributes":{"formatter":{"id":"f06cfe28-9584-42d0-9d68-68fec8939e93","type":"BasicTickFormatter"},"plot":{"id":"9ed10e59-ded6-4fd1-8a99-f205152e52c0","subtype":"Figure","type":"Plot"},"ticker":{"id":"c022acb5-a355-4c0e-9e12-eddc3bf3cb37","type":"BasicTicker"},"visible":false},"id":"2ab77787-ef23-4bf3-9c1e-6d0e0aa93ace","type":"LinearAxis"},{"attributes":{"callback":{"id":"d079ff67-a19d-442d-a6ae-7fe2ce0f9507","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"99954270-90b3-464b-9706-5fb8e04ceea2","type":"Slider"},{"attributes":{},"id":"edbaa355-bc8a-4fe6-bd27-8065ca8e58ed","type":"BasicTickFormatter"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/SI7O7_000.png"],"url_num":["000"]},"selected":{"id":"00b50a5d-6d89-46a4-8949-b1d46cbb0c18","type":"Selection"},"selection_policy":{"id":"c9aa5afa-57c7-4bb1-92ea-7cc6e8f9125c","type":"UnionRenderers"}},"id":"4d79165c-1e31-47fe-b8b8-0f9bc1adc1f4","type":"ColumnDataSource"},{"attributes":{"plot":null,"text":""},"id":"e62d02f0-f6df-4171-ad4d-d907a80b26de","type":"Title"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"22778fb2-ecf9-439e-a7d6-510ae9abd94b","type":"Toolbar"},{"attributes":{},"id":"e5ee17aa-e142-4204-9725-35e64fce9ff6","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"30aa12ee-242e-4830-a2d3-f60080aa9f50","type":"ImageURL"},{"attributes":{},"id":"00b50a5d-6d89-46a4-8949-b1d46cbb0c18","type":"Selection"}],"root_ids":["4725b9c5-afbe-433e-9afe-f06ee1c0cdb5"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"f1145d59-ff7c-433a-834d-e4596d17ad8e","roots":{"4725b9c5-afbe-433e-9afe-f06ee1c0cdb5":"cdfa862b-87ec-4f3c-be24-3115a6251fcc"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();