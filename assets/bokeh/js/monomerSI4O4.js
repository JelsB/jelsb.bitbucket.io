(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("1de0175f-cb7c-46ce-a6f4-da24993d4959");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '1de0175f-cb7c-46ce-a6f4-da24993d4959' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"e16a8bbb-d1eb-4a55-a296-4aaf29caeae0":{"roots":{"references":[{"attributes":{"data_source":{"id":"91fac768-fb9c-45eb-98be-d7ae747764b4","type":"ColumnDataSource"},"glyph":{"id":"67ad6558-47ef-4da0-87a6-3d1dfc2b740b","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"31151b15-734b-4f43-8cb0-4b0636700912","type":"ImageURL"},"selection_glyph":null,"view":{"id":"584adb11-bcb4-443c-be76-ad8c93005d41","type":"CDSView"}},"id":"aea95af8-ff48-47e1-9aec-1be3ba4f9e6d","type":"GlyphRenderer"},{"attributes":{"formatter":{"id":"883c51c2-6863-4da5-b72c-68340dc70703","type":"BasicTickFormatter"},"plot":{"id":"d4ab81fa-bc1f-4fd1-a055-ce9e2cff5dfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"a58a168d-8e44-4648-b06e-9bb87b7e67ce","type":"BasicTicker"},"visible":false},"id":"6e998d94-34fc-4424-aa3c-1fd58d209356","type":"LinearAxis"},{"attributes":{"callback":null},"id":"c99c0f3d-4647-4023-9b51-66cce6a159d0","type":"Range1d"},{"attributes":{},"id":"a24e205a-dee1-49df-aac3-7b7594dce9f2","type":"BasicTicker"},{"attributes":{"plot":null,"text":""},"id":"07393a67-fcfb-47c1-8035-3f18b955a9dc","type":"Title"},{"attributes":{},"id":"246b0f9d-37c4-44c8-bbd9-c1588230a872","type":"BasicTickFormatter"},{"attributes":{},"id":"883c51c2-6863-4da5-b72c-68340dc70703","type":"BasicTickFormatter"},{"attributes":{"source":{"id":"91fac768-fb9c-45eb-98be-d7ae747764b4","type":"ColumnDataSource"}},"id":"584adb11-bcb4-443c-be76-ad8c93005d41","type":"CDSView"},{"attributes":{"plot":{"id":"d4ab81fa-bc1f-4fd1-a055-ce9e2cff5dfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"a24e205a-dee1-49df-aac3-7b7594dce9f2","type":"BasicTicker"},"visible":false},"id":"1bb6e840-02c7-43cf-9396-9922096eb17f","type":"Grid"},{"attributes":{},"id":"6dc61278-0dee-4b69-b8df-9613bdd2d2a8","type":"LinearScale"},{"attributes":{"children":[{"id":"9b1e5d3b-58f6-4b42-aeaa-875926ed6f6f","type":"Slider"}]},"id":"cddbf7b6-c711-4064-8109-6cc5e3d470cd","type":"WidgetBox"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerSI4O4_000.png"],"url_num":["000"]},"selected":{"id":"c21fa6d7-1f9b-4fbe-9ad3-b87b2c12608a","type":"Selection"},"selection_policy":{"id":"7a189f19-b42a-4d4d-9944-273fa136821a","type":"UnionRenderers"}},"id":"91fac768-fb9c-45eb-98be-d7ae747764b4","type":"ColumnDataSource"},{"attributes":{},"id":"a58a168d-8e44-4648-b06e-9bb87b7e67ce","type":"BasicTicker"},{"attributes":{"formatter":{"id":"246b0f9d-37c4-44c8-bbd9-c1588230a872","type":"BasicTickFormatter"},"plot":{"id":"d4ab81fa-bc1f-4fd1-a055-ce9e2cff5dfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"a24e205a-dee1-49df-aac3-7b7594dce9f2","type":"BasicTicker"},"visible":false},"id":"5a539aa6-e59a-44be-a311-d13d16906f24","type":"LinearAxis"},{"attributes":{},"id":"c21fa6d7-1f9b-4fbe-9ad3-b87b2c12608a","type":"Selection"},{"attributes":{"dimension":1,"plot":{"id":"d4ab81fa-bc1f-4fd1-a055-ce9e2cff5dfd","subtype":"Figure","type":"Plot"},"ticker":{"id":"a58a168d-8e44-4648-b06e-9bb87b7e67ce","type":"BasicTicker"},"visible":false},"id":"61d80ffe-dc43-4d7f-b6cd-a6c130d6dc69","type":"Grid"},{"attributes":{"callback":null},"id":"eadc8908-9817-4f15-81ef-17db7c8f4078","type":"Range1d"},{"attributes":{},"id":"7a189f19-b42a-4d4d-9944-273fa136821a","type":"UnionRenderers"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"31151b15-734b-4f43-8cb0-4b0636700912","type":"ImageURL"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"67ad6558-47ef-4da0-87a6-3d1dfc2b740b","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"91fac768-fb9c-45eb-98be-d7ae747764b4","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"efd2b87c-72af-4e59-856c-e5340b523065","type":"CustomJS"},{"attributes":{},"id":"90dbdc4c-f41e-4b3b-8841-3f9444e5786e","type":"LinearScale"},{"attributes":{"callback":{"id":"efd2b87c-72af-4e59-856c-e5340b523065","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"9b1e5d3b-58f6-4b42-aeaa-875926ed6f6f","type":"Slider"},{"attributes":{"children":[{"id":"cddbf7b6-c711-4064-8109-6cc5e3d470cd","type":"WidgetBox"},{"id":"d4ab81fa-bc1f-4fd1-a055-ce9e2cff5dfd","subtype":"Figure","type":"Plot"}]},"id":"a018b1d7-a466-4a03-bc33-b5ec96b6e8a8","type":"Column"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"27ec45b1-3674-49cb-b0c1-f3bae2bbfa7d","type":"Toolbar"},{"attributes":{"below":[{"id":"5a539aa6-e59a-44be-a311-d13d16906f24","type":"LinearAxis"}],"left":[{"id":"6e998d94-34fc-4424-aa3c-1fd58d209356","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"5a539aa6-e59a-44be-a311-d13d16906f24","type":"LinearAxis"},{"id":"1bb6e840-02c7-43cf-9396-9922096eb17f","type":"Grid"},{"id":"6e998d94-34fc-4424-aa3c-1fd58d209356","type":"LinearAxis"},{"id":"61d80ffe-dc43-4d7f-b6cd-a6c130d6dc69","type":"Grid"},{"id":"aea95af8-ff48-47e1-9aec-1be3ba4f9e6d","type":"GlyphRenderer"}],"title":{"id":"07393a67-fcfb-47c1-8035-3f18b955a9dc","type":"Title"},"toolbar":{"id":"27ec45b1-3674-49cb-b0c1-f3bae2bbfa7d","type":"Toolbar"},"x_range":{"id":"eadc8908-9817-4f15-81ef-17db7c8f4078","type":"Range1d"},"x_scale":{"id":"6dc61278-0dee-4b69-b8df-9613bdd2d2a8","type":"LinearScale"},"y_range":{"id":"c99c0f3d-4647-4023-9b51-66cce6a159d0","type":"Range1d"},"y_scale":{"id":"90dbdc4c-f41e-4b3b-8841-3f9444e5786e","type":"LinearScale"}},"id":"d4ab81fa-bc1f-4fd1-a055-ce9e2cff5dfd","subtype":"Figure","type":"Plot"}],"root_ids":["a018b1d7-a466-4a03-bc33-b5ec96b6e8a8"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"e16a8bbb-d1eb-4a55-a296-4aaf29caeae0","roots":{"a018b1d7-a466-4a03-bc33-b5ec96b6e8a8":"1de0175f-cb7c-46ce-a6f4-da24993d4959"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();