(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("c2862c33-17c6-4955-8161-f28b6e13bad8");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'c2862c33-17c6-4955-8161-f28b6e13bad8' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"3dff1d86-30c2-449b-80d4-42acdcde5f3f":{"roots":{"references":[{"attributes":{"callback":null},"id":"c3e1b71c-b705-4657-9517-279c438c73e1","type":"Range1d"},{"attributes":{},"id":"c6285eae-7b09-498b-93a4-4d947012b26a","type":"BasicTicker"},{"attributes":{},"id":"6fe4292a-6f97-4719-a444-f53d69ffa0af","type":"LinearScale"},{"attributes":{},"id":"639102bb-3d5f-40fe-98d8-d931ccd32146","type":"BasicTicker"},{"attributes":{"data_source":{"id":"6d843447-8f75-4bde-aa86-64c9eef40fd1","type":"ColumnDataSource"},"glyph":{"id":"8f1cb6db-96d7-4b25-8841-7456576f4ffd","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"695fb225-e9a9-461c-802d-e45acba2a912","type":"ImageURL"},"selection_glyph":null,"view":{"id":"014b939a-36e9-4c9a-b6b6-06018228134e","type":"CDSView"}},"id":"44a80b21-7718-4dd6-82e0-9e34f352e279","type":"GlyphRenderer"},{"attributes":{},"id":"2c3f141b-e4dc-49b4-b2e9-d923eef0b1f2","type":"UnionRenderers"},{"attributes":{"below":[{"id":"9650bcee-1d90-4c2c-8a9f-7f47bdd1f92c","type":"LinearAxis"}],"left":[{"id":"526293b9-db85-4a02-a297-f23c40379e30","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"9650bcee-1d90-4c2c-8a9f-7f47bdd1f92c","type":"LinearAxis"},{"id":"99aee351-63b6-454c-8926-1df1afeb764b","type":"Grid"},{"id":"526293b9-db85-4a02-a297-f23c40379e30","type":"LinearAxis"},{"id":"d12318bf-2760-4825-92a4-3e251e55314b","type":"Grid"},{"id":"44a80b21-7718-4dd6-82e0-9e34f352e279","type":"GlyphRenderer"}],"title":{"id":"ae277558-d6dc-45bd-957f-e182e5fc74fb","type":"Title"},"toolbar":{"id":"d1900017-81c8-4d1f-99a3-aa76262e33cd","type":"Toolbar"},"x_range":{"id":"7f2658b0-52fe-46ff-b873-430d4c7982eb","type":"Range1d"},"x_scale":{"id":"4e508741-64b6-4f8e-ac37-ac6d46ebc621","type":"LinearScale"},"y_range":{"id":"c3e1b71c-b705-4657-9517-279c438c73e1","type":"Range1d"},"y_scale":{"id":"6fe4292a-6f97-4719-a444-f53d69ffa0af","type":"LinearScale"}},"id":"4a432af7-0658-40d7-8744-79a0f3eb8f08","subtype":"Figure","type":"Plot"},{"attributes":{"args":{"source":{"id":"6d843447-8f75-4bde-aa86-64c9eef40fd1","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"49800145-57c9-480f-b5ca-b894d4ab1fc5","type":"CustomJS"},{"attributes":{},"id":"87d28193-b497-4519-a4f6-a853845ec40f","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"695fb225-e9a9-461c-802d-e45acba2a912","type":"ImageURL"},{"attributes":{"plot":null,"text":""},"id":"ae277558-d6dc-45bd-957f-e182e5fc74fb","type":"Title"},{"attributes":{"children":[{"id":"48b66c9d-6cf0-4cb5-a2e3-7537de18d29e","type":"Slider"}]},"id":"dc2b2022-51da-455f-b133-3df5bc18dc05","type":"WidgetBox"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"d1900017-81c8-4d1f-99a3-aa76262e33cd","type":"Toolbar"},{"attributes":{"source":{"id":"6d843447-8f75-4bde-aa86-64c9eef40fd1","type":"ColumnDataSource"}},"id":"014b939a-36e9-4c9a-b6b6-06018228134e","type":"CDSView"},{"attributes":{"children":[{"id":"dc2b2022-51da-455f-b133-3df5bc18dc05","type":"WidgetBox"},{"id":"4a432af7-0658-40d7-8744-79a0f3eb8f08","subtype":"Figure","type":"Plot"}]},"id":"85aef57f-bd2a-439c-9053-04533da46776","type":"Column"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8f1cb6db-96d7-4b25-8841-7456576f4ffd","type":"ImageURL"},{"attributes":{},"id":"56b64081-d351-4fef-9425-a9aa4bf6c8b7","type":"Selection"},{"attributes":{"plot":{"id":"4a432af7-0658-40d7-8744-79a0f3eb8f08","subtype":"Figure","type":"Plot"},"ticker":{"id":"639102bb-3d5f-40fe-98d8-d931ccd32146","type":"BasicTicker"},"visible":false},"id":"99aee351-63b6-454c-8926-1df1afeb764b","type":"Grid"},{"attributes":{"formatter":{"id":"1de082a5-2fc2-436e-bd85-9ee0dc37b635","type":"BasicTickFormatter"},"plot":{"id":"4a432af7-0658-40d7-8744-79a0f3eb8f08","subtype":"Figure","type":"Plot"},"ticker":{"id":"639102bb-3d5f-40fe-98d8-d931ccd32146","type":"BasicTicker"},"visible":false},"id":"9650bcee-1d90-4c2c-8a9f-7f47bdd1f92c","type":"LinearAxis"},{"attributes":{},"id":"4e508741-64b6-4f8e-ac37-ac6d46ebc621","type":"LinearScale"},{"attributes":{},"id":"1de082a5-2fc2-436e-bd85-9ee0dc37b635","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"49800145-57c9-480f-b5ca-b894d4ab1fc5","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"48b66c9d-6cf0-4cb5-a2e3-7537de18d29e","type":"Slider"},{"attributes":{"callback":null},"id":"7f2658b0-52fe-46ff-b873-430d4c7982eb","type":"Range1d"},{"attributes":{"dimension":1,"plot":{"id":"4a432af7-0658-40d7-8744-79a0f3eb8f08","subtype":"Figure","type":"Plot"},"ticker":{"id":"c6285eae-7b09-498b-93a4-4d947012b26a","type":"BasicTicker"},"visible":false},"id":"d12318bf-2760-4825-92a4-3e251e55314b","type":"Grid"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerAL2O3_000.png"],"url_num":["000"]},"selected":{"id":"56b64081-d351-4fef-9425-a9aa4bf6c8b7","type":"Selection"},"selection_policy":{"id":"2c3f141b-e4dc-49b4-b2e9-d923eef0b1f2","type":"UnionRenderers"}},"id":"6d843447-8f75-4bde-aa86-64c9eef40fd1","type":"ColumnDataSource"},{"attributes":{"formatter":{"id":"87d28193-b497-4519-a4f6-a853845ec40f","type":"BasicTickFormatter"},"plot":{"id":"4a432af7-0658-40d7-8744-79a0f3eb8f08","subtype":"Figure","type":"Plot"},"ticker":{"id":"c6285eae-7b09-498b-93a4-4d947012b26a","type":"BasicTicker"},"visible":false},"id":"526293b9-db85-4a02-a297-f23c40379e30","type":"LinearAxis"}],"root_ids":["85aef57f-bd2a-439c-9053-04533da46776"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"3dff1d86-30c2-449b-80d4-42acdcde5f3f","roots":{"85aef57f-bd2a-439c-9053-04533da46776":"c2862c33-17c6-4955-8161-f28b6e13bad8"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();