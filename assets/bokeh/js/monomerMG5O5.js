(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("91e24162-d38c-473c-9491-05f49edf73e9");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid '91e24162-d38c-473c-9491-05f49edf73e9' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"4ef5c038-b1b7-4c3e-9e23-fd5fce79b0f2":{"roots":{"references":[{"attributes":{"children":[{"id":"51a7fc93-2930-4e6c-9f1f-f41827adf75d","type":"WidgetBox"},{"id":"56ac6b38-4507-4207-83f9-f339058b16c4","subtype":"Figure","type":"Plot"}]},"id":"34bdb375-ab10-4939-9060-9f3142050a23","type":"Column"},{"attributes":{},"id":"8be436a7-73a1-4c54-b03c-66a826aaec58","type":"BasicTicker"},{"attributes":{"dimension":1,"plot":{"id":"56ac6b38-4507-4207-83f9-f339058b16c4","subtype":"Figure","type":"Plot"},"ticker":{"id":"e5882fde-9aa5-4a48-831e-43a5ee559507","type":"BasicTicker"},"visible":false},"id":"123284f1-663b-443f-95e4-9f89e5736895","type":"Grid"},{"attributes":{"formatter":{"id":"3de0c76d-64fb-42bc-9a59-d9bd8b7341f6","type":"BasicTickFormatter"},"plot":{"id":"56ac6b38-4507-4207-83f9-f339058b16c4","subtype":"Figure","type":"Plot"},"ticker":{"id":"8be436a7-73a1-4c54-b03c-66a826aaec58","type":"BasicTicker"},"visible":false},"id":"f52ffab4-e19a-4d45-beae-120b2ef4838c","type":"LinearAxis"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"9fe1681a-0821-4b0e-886b-b651a30cc3fe","type":"ImageURL"},{"attributes":{"plot":{"id":"56ac6b38-4507-4207-83f9-f339058b16c4","subtype":"Figure","type":"Plot"},"ticker":{"id":"8be436a7-73a1-4c54-b03c-66a826aaec58","type":"BasicTicker"},"visible":false},"id":"9e02e153-6d7e-436d-aad0-ccbc65a5acfd","type":"Grid"},{"attributes":{"plot":null,"text":""},"id":"1709cc7b-002d-42e2-9362-700373f0a766","type":"Title"},{"attributes":{"below":[{"id":"f52ffab4-e19a-4d45-beae-120b2ef4838c","type":"LinearAxis"}],"left":[{"id":"d67a8620-88b7-49e5-8972-a52f51d4830d","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"f52ffab4-e19a-4d45-beae-120b2ef4838c","type":"LinearAxis"},{"id":"9e02e153-6d7e-436d-aad0-ccbc65a5acfd","type":"Grid"},{"id":"d67a8620-88b7-49e5-8972-a52f51d4830d","type":"LinearAxis"},{"id":"123284f1-663b-443f-95e4-9f89e5736895","type":"Grid"},{"id":"277beb4d-b4e5-4b3a-8b81-b57ebf72781a","type":"GlyphRenderer"}],"title":{"id":"1709cc7b-002d-42e2-9362-700373f0a766","type":"Title"},"toolbar":{"id":"d2f6767c-c224-4252-9ecd-03805f5dc9d8","type":"Toolbar"},"x_range":{"id":"df54018d-2c8d-4079-9b62-abab5eddf20b","type":"Range1d"},"x_scale":{"id":"9cb28b13-4085-4e60-b1f1-5c5c85a016c1","type":"LinearScale"},"y_range":{"id":"55d97b99-c282-41be-99ad-81da69d141c1","type":"Range1d"},"y_scale":{"id":"2b2c8850-9d98-4281-8bdd-50c8b9c1a2ca","type":"LinearScale"}},"id":"56ac6b38-4507-4207-83f9-f339058b16c4","subtype":"Figure","type":"Plot"},{"attributes":{"callback":null},"id":"df54018d-2c8d-4079-9b62-abab5eddf20b","type":"Range1d"},{"attributes":{},"id":"e5882fde-9aa5-4a48-831e-43a5ee559507","type":"BasicTicker"},{"attributes":{"formatter":{"id":"628747f4-8a06-423e-a0e2-9e5203c94390","type":"BasicTickFormatter"},"plot":{"id":"56ac6b38-4507-4207-83f9-f339058b16c4","subtype":"Figure","type":"Plot"},"ticker":{"id":"e5882fde-9aa5-4a48-831e-43a5ee559507","type":"BasicTicker"},"visible":false},"id":"d67a8620-88b7-49e5-8972-a52f51d4830d","type":"LinearAxis"},{"attributes":{},"id":"3de0c76d-64fb-42bc-9a59-d9bd8b7341f6","type":"BasicTickFormatter"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"02b4c977-244b-4cc8-91c4-e34f4038a586","type":"ImageURL"},{"attributes":{"args":{"source":{"id":"bc3734d2-881e-4d2a-a525-3304608f910c","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"98ee82d8-2040-4304-acef-892f6a18dc70","type":"CustomJS"},{"attributes":{},"id":"abe583c3-960d-4dc4-bd21-a6d37b682b10","type":"Selection"},{"attributes":{"data_source":{"id":"bc3734d2-881e-4d2a-a525-3304608f910c","type":"ColumnDataSource"},"glyph":{"id":"02b4c977-244b-4cc8-91c4-e34f4038a586","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"9fe1681a-0821-4b0e-886b-b651a30cc3fe","type":"ImageURL"},"selection_glyph":null,"view":{"id":"c073bf25-be15-406b-8de5-d83142aea9da","type":"CDSView"}},"id":"277beb4d-b4e5-4b3a-8b81-b57ebf72781a","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"d2f6767c-c224-4252-9ecd-03805f5dc9d8","type":"Toolbar"},{"attributes":{"callback":null},"id":"55d97b99-c282-41be-99ad-81da69d141c1","type":"Range1d"},{"attributes":{},"id":"9cb28b13-4085-4e60-b1f1-5c5c85a016c1","type":"LinearScale"},{"attributes":{"source":{"id":"bc3734d2-881e-4d2a-a525-3304608f910c","type":"ColumnDataSource"}},"id":"c073bf25-be15-406b-8de5-d83142aea9da","type":"CDSView"},{"attributes":{},"id":"787e0245-d7a4-4d81-8826-8afe674f1e84","type":"UnionRenderers"},{"attributes":{},"id":"628747f4-8a06-423e-a0e2-9e5203c94390","type":"BasicTickFormatter"},{"attributes":{"callback":{"id":"98ee82d8-2040-4304-acef-892f6a18dc70","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"7f450424-83ce-4d2f-8019-69c6da8488b5","type":"Slider"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/monomerMG5O5_000.png"],"url_num":["000"]},"selected":{"id":"abe583c3-960d-4dc4-bd21-a6d37b682b10","type":"Selection"},"selection_policy":{"id":"787e0245-d7a4-4d81-8826-8afe674f1e84","type":"UnionRenderers"}},"id":"bc3734d2-881e-4d2a-a525-3304608f910c","type":"ColumnDataSource"},{"attributes":{},"id":"2b2c8850-9d98-4281-8bdd-50c8b9c1a2ca","type":"LinearScale"},{"attributes":{"children":[{"id":"7f450424-83ce-4d2f-8019-69c6da8488b5","type":"Slider"}]},"id":"51a7fc93-2930-4e6c-9f1f-f41827adf75d","type":"WidgetBox"}],"root_ids":["34bdb375-ab10-4939-9060-9f3142050a23"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"4ef5c038-b1b7-4c3e-9e23-fd5fce79b0f2","roots":{"34bdb375-ab10-4939-9060-9f3142050a23":"91e24162-d38c-473c-9491-05f49edf73e9"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();