(function() {
  var fn = function() {
    
    (function(root) {
      function now() {
        return new Date();
      }
    
      var force = false;
    
      if (typeof (root._bokeh_onload_callbacks) === "undefined" || force === true) {
        root._bokeh_onload_callbacks = [];
        root._bokeh_is_loading = undefined;
      }
    
      
      
    
      
      
    
      function run_callbacks() {
        try {
          root._bokeh_onload_callbacks.forEach(function(callback) { callback() });
        }
        finally {
          delete root._bokeh_onload_callbacks
        }
        console.info("Bokeh: all callbacks have finished");
      }
    
      function load_libs(js_urls, callback) {
        root._bokeh_onload_callbacks.push(callback);
        if (root._bokeh_is_loading > 0) {
          console.log("Bokeh: BokehJS is being loaded, scheduling callback at", now());
          return null;
        }
        if (js_urls == null || js_urls.length === 0) {
          run_callbacks();
          return null;
        }
        console.log("Bokeh: BokehJS not loaded, scheduling load and callback at", now());
        root._bokeh_is_loading = js_urls.length;
        for (var i = 0; i < js_urls.length; i++) {
          var url = js_urls[i];
          var s = document.createElement('script');
          s.src = url;
          s.async = false;
          s.onreadystatechange = s.onload = function() {
            root._bokeh_is_loading--;
            if (root._bokeh_is_loading === 0) {
              console.log("Bokeh: all BokehJS libraries loaded");
              run_callbacks()
            }
          };
          s.onerror = function() {
            console.warn("failed to load library " + url);
          };
          console.log("Bokeh: injecting script tag for BokehJS library: ", url);
          document.getElementsByTagName("head")[0].appendChild(s);
        }
      };var element = document.getElementById("d985fd6c-17ef-41a0-b9dd-9da5bd1e2983");
      if (element == null) {
        console.log("Bokeh: ERROR: autoload.js configured with elementid 'd985fd6c-17ef-41a0-b9dd-9da5bd1e2983' but no matching script tag was found. ")
        return false;
      }
    
      var js_urls = ["https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.js", "https://cdn.pydata.org/bokeh/release/bokeh-gl-0.13.0.min.js"];
    
      var inline_js = [
        function(Bokeh) {
          Bokeh.set_log_level("info");
        },
        
        function(Bokeh) {
          
        },
        
        function(Bokeh) {
          (function() {
            var fn = function() {
              Bokeh.safely(function() {
                (function(root) {
                  function embed_document(root) {
                    
                  var docs_json = '{"eef22c72-4f68-4997-9c67-69beb3eff604":{"roots":{"references":[{"attributes":{"plot":{"id":"091aa8c8-dda7-4aa3-a347-371abcbc31e0","subtype":"Figure","type":"Plot"},"ticker":{"id":"88b85863-6195-4cd0-88ea-05c5bfae3850","type":"BasicTicker"},"visible":false},"id":"8dbfffe5-a90a-4ddd-a8e7-b8ab44b6c3dd","type":"Grid"},{"attributes":{},"id":"f615aaa7-dbc5-4227-9a15-d3d5f9e13876","type":"LinearScale"},{"attributes":{"plot":null,"text":""},"id":"3dfb9386-5c3b-4309-ba5d-be28e293b1ba","type":"Title"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"6795bcfb-6199-4d8e-afe1-e1df2eb6a304","type":"ImageURL"},{"attributes":{"formatter":{"id":"651a21c0-a928-40c2-a87d-4ef65db6cb7e","type":"BasicTickFormatter"},"plot":{"id":"091aa8c8-dda7-4aa3-a347-371abcbc31e0","subtype":"Figure","type":"Plot"},"ticker":{"id":"88b85863-6195-4cd0-88ea-05c5bfae3850","type":"BasicTicker"},"visible":false},"id":"9c964a6d-5751-4a81-aabf-8cc870574410","type":"LinearAxis"},{"attributes":{"children":[{"id":"2fc0ccf2-5240-463a-9560-7ef769a55ffb","type":"WidgetBox"},{"id":"091aa8c8-dda7-4aa3-a347-371abcbc31e0","subtype":"Figure","type":"Plot"}]},"id":"1e87d8f3-6f53-40a8-ac75-9ea1f75cbb7e","type":"Column"},{"attributes":{},"id":"dd848c23-6a37-49db-9953-1e8c13a351fb","type":"BasicTickFormatter"},{"attributes":{},"id":"a1a40e54-9ccb-46ea-bdd4-f3a1322bab04","type":"LinearScale"},{"attributes":{"h":{"units":"data","value":1},"url":{"field":"url_full"},"w":{"units":"data","value":1},"x":{"value":0},"y":{"value":1}},"id":"8a78eb44-7858-4654-83f3-0badfa863695","type":"ImageURL"},{"attributes":{"callback":null},"id":"574e65ca-6dc2-4fdd-b333-99bd4c8cb6a2","type":"Range1d"},{"attributes":{},"id":"88b85863-6195-4cd0-88ea-05c5bfae3850","type":"BasicTicker"},{"attributes":{"callback":null},"id":"f9e5038d-1a53-4b25-80e3-6ec8ccc85fae","type":"Range1d"},{"attributes":{},"id":"00cf33ef-cfc6-4c48-88a5-660a08dd25ee","type":"Selection"},{"attributes":{"formatter":{"id":"dd848c23-6a37-49db-9953-1e8c13a351fb","type":"BasicTickFormatter"},"plot":{"id":"091aa8c8-dda7-4aa3-a347-371abcbc31e0","subtype":"Figure","type":"Plot"},"ticker":{"id":"39af1514-43f8-496b-be32-fad693c8b6fe","type":"BasicTicker"},"visible":false},"id":"16ba4382-13da-4044-b6ec-4d19445a63f9","type":"LinearAxis"},{"attributes":{"args":{"source":{"id":"1e56d9d2-d479-4f60-a11d-7e2d2b70a131","type":"ColumnDataSource"}},"code":"\\n        var data = source.data;\\n        var f = cb_obj.value/5\\n        var file_name = data[&#x27;url_full&#x27;]\\n        var file_num = data[&#x27;url_num&#x27;]\\n        var f_string = f.toString().padStart(3, &#x27;0&#x27;)\\n        file_name[0] = file_name[0].replace(file_num[0], f_string)\\n        file_num[0] = f_string\\n        source.change.emit();\\n        "},"id":"51854a65-fcdb-4305-840f-86f3e1583de2","type":"CustomJS"},{"attributes":{"callback":null,"data":{"url_full":["/assets/bokeh/figs/TI10O20_000.png"],"url_num":["000"]},"selected":{"id":"00cf33ef-cfc6-4c48-88a5-660a08dd25ee","type":"Selection"},"selection_policy":{"id":"c8ffc286-8f27-4acb-a5a4-cff1910994ff","type":"UnionRenderers"}},"id":"1e56d9d2-d479-4f60-a11d-7e2d2b70a131","type":"ColumnDataSource"},{"attributes":{"dimension":1,"plot":{"id":"091aa8c8-dda7-4aa3-a347-371abcbc31e0","subtype":"Figure","type":"Plot"},"ticker":{"id":"39af1514-43f8-496b-be32-fad693c8b6fe","type":"BasicTicker"},"visible":false},"id":"67f20a50-13a5-4485-a6cd-c02c1335cb61","type":"Grid"},{"attributes":{"data_source":{"id":"1e56d9d2-d479-4f60-a11d-7e2d2b70a131","type":"ColumnDataSource"},"glyph":{"id":"6795bcfb-6199-4d8e-afe1-e1df2eb6a304","type":"ImageURL"},"hover_glyph":null,"muted_glyph":null,"nonselection_glyph":{"id":"8a78eb44-7858-4654-83f3-0badfa863695","type":"ImageURL"},"selection_glyph":null,"view":{"id":"fd4e32a6-efa7-4962-a214-0bef861125c9","type":"CDSView"}},"id":"7e796800-8307-4ad1-b252-a3e41cb70284","type":"GlyphRenderer"},{"attributes":{"active_drag":"auto","active_inspect":"auto","active_multi":null,"active_scroll":"auto","active_tap":"auto"},"id":"6e877191-f9df-4850-b391-44db4523cfdf","type":"Toolbar"},{"attributes":{"children":[{"id":"befcf058-1bb2-4c9e-ae14-7c9076b7d306","type":"Slider"}]},"id":"2fc0ccf2-5240-463a-9560-7ef769a55ffb","type":"WidgetBox"},{"attributes":{"below":[{"id":"9c964a6d-5751-4a81-aabf-8cc870574410","type":"LinearAxis"}],"left":[{"id":"16ba4382-13da-4044-b6ec-4d19445a63f9","type":"LinearAxis"}],"plot_height":500,"plot_width":700,"renderers":[{"id":"9c964a6d-5751-4a81-aabf-8cc870574410","type":"LinearAxis"},{"id":"8dbfffe5-a90a-4ddd-a8e7-b8ab44b6c3dd","type":"Grid"},{"id":"16ba4382-13da-4044-b6ec-4d19445a63f9","type":"LinearAxis"},{"id":"67f20a50-13a5-4485-a6cd-c02c1335cb61","type":"Grid"},{"id":"7e796800-8307-4ad1-b252-a3e41cb70284","type":"GlyphRenderer"}],"title":{"id":"3dfb9386-5c3b-4309-ba5d-be28e293b1ba","type":"Title"},"toolbar":{"id":"6e877191-f9df-4850-b391-44db4523cfdf","type":"Toolbar"},"x_range":{"id":"f9e5038d-1a53-4b25-80e3-6ec8ccc85fae","type":"Range1d"},"x_scale":{"id":"a1a40e54-9ccb-46ea-bdd4-f3a1322bab04","type":"LinearScale"},"y_range":{"id":"574e65ca-6dc2-4fdd-b333-99bd4c8cb6a2","type":"Range1d"},"y_scale":{"id":"f615aaa7-dbc5-4227-9a15-d3d5f9e13876","type":"LinearScale"}},"id":"091aa8c8-dda7-4aa3-a347-371abcbc31e0","subtype":"Figure","type":"Plot"},{"attributes":{},"id":"39af1514-43f8-496b-be32-fad693c8b6fe","type":"BasicTicker"},{"attributes":{"source":{"id":"1e56d9d2-d479-4f60-a11d-7e2d2b70a131","type":"ColumnDataSource"}},"id":"fd4e32a6-efa7-4962-a214-0bef861125c9","type":"CDSView"},{"attributes":{"callback":{"id":"51854a65-fcdb-4305-840f-86f3e1583de2","type":"CustomJS"},"end":365,"start":0,"step":5,"title":"days","value":0},"id":"befcf058-1bb2-4c9e-ae14-7c9076b7d306","type":"Slider"},{"attributes":{},"id":"c8ffc286-8f27-4acb-a5a4-cff1910994ff","type":"UnionRenderers"},{"attributes":{},"id":"651a21c0-a928-40c2-a87d-4ef65db6cb7e","type":"BasicTickFormatter"}],"root_ids":["1e87d8f3-6f53-40a8-ac75-9ea1f75cbb7e"]},"title":"Bokeh Application","version":"0.13.0"}}';
                  var render_items = [{"docid":"eef22c72-4f68-4997-9c67-69beb3eff604","roots":{"1e87d8f3-6f53-40a8-ac75-9ea1f75cbb7e":"d985fd6c-17ef-41a0-b9dd-9da5bd1e2983"}}];
                  root.Bokeh.embed.embed_items(docs_json, render_items);
                
                  }
                  if (root.Bokeh !== undefined) {
                    embed_document(root);
                  } else {
                    var attempts = 0;
                    var timer = setInterval(function(root) {
                      if (root.Bokeh !== undefined) {
                        embed_document(root);
                        clearInterval(timer);
                      }
                      attempts++;
                      if (attempts > 100) {
                        console.log("Bokeh: ERROR: Unable to run BokehJS code because BokehJS library is missing")
                        clearInterval(timer);
                      }
                    }, 10, root)
                  }
                })(window);
              });
            };
            if (document.readyState != "loading") fn();
            else document.addEventListener("DOMContentLoaded", fn);
          })();
        },
        function(Bokeh) {
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-widgets-0.13.0.min.css");
          console.log("Bokeh: injecting CSS: https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
          Bokeh.embed.inject_css("https://cdn.pydata.org/bokeh/release/bokeh-tables-0.13.0.min.css");
        }
      ];
    
      function run_inline_js() {
        
        for (var i = 0; i < inline_js.length; i++) {
          inline_js[i].call(root, root.Bokeh);
        }
        
      }
    
      if (root._bokeh_is_loading === 0) {
        console.log("Bokeh: BokehJS loaded, going straight to plotting");
        run_inline_js();
      } else {
        load_libs(js_urls, function() {
          console.log("Bokeh: BokehJS plotting callback run at", now());
          run_inline_js();
        });
      }
    }(window));
  };
  if (document.readyState != "loading") fn();
  else document.addEventListener("DOMContentLoaded", fn);
})();